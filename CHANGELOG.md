# CHANGELOG

## v3.1.2 [2024-04-09]

### Changed

#### Ecoli
* updated pathorules
* updated stx databases

#### Listeria: 
* more lissero reporting 

#### Database Updates
* pubmlst
* amrfinder
* bakta
* plasmid database: plsdb


### Added

#### Plasmidblaster: 
* Summed blast over all contigs
    summary_plasmidblaster_persubject_filtered = "results/summary/summary_plasmidblaster_persubject_filtered.tsv",
    summary_plasmidblaster_persubject_allbesthits = "results/summary/summary_plasmidblaster_persubject_allbesthits.tsv",
    summary_plasmidblaster_persubject_nonredundant = "results/summary/summary_plasmidblaster_persubject_nonredundant.tsv"
* in report aggregated over contigs

#### Bacillus 
* Transition to btyper3 databases, 
* NEW pesticidal proteins, 
* mesopsychro with signature from bakta

#### Staphyloccus
* sccmec class via CGE 
* sccmec class via staphopia


#### bakta summary
* new results/summary/summary_bakta.tsv


### Deprecated

* prokka deprecated
* legacy report deprecated
