# %% BakCharak: a Snakemake pipeline for Bakterial Characterization of food-borne pathogens

# Python Standard Packages
import os
from datetime import datetime
import shutil

# Other Packages
import pandas as pd

# %% Settings -------------------------------------------------------

shell.executable("bash")

# Set snakemake main workdir variable
workdir: config["workdir"]

# Collect sample names from sample list
samples_raw = pd.read_csv(config["samples"], sep = "\t", dtype = str).set_index('sample', inplace = False)  # use two steps to set first column as index_col in case samples are integers or a scientific notation
samples = samples_raw[~samples_raw.index.str.match('^#.*')]  # remove samples that are commented-out
samples_column = "fq1"

# Legacy Fallback for old config_bakcharak.yaml files  # TODO: remove legacy check after a year
if 'threads' not in config.get('smk_params', {}):
  config.setdefault('smk_params', {})
  config['smk_params']['threads'] = config['params'].get('threads', 1)


# %% Functions ------------------------------------------------------

def _get_fastq(wildcards, column_name = samples_column):
    return samples.loc[wildcards.sample, [column_name]].dropna()[0]


def block_copy(input_files, output_file):
    with open(output_file, 'wb') as outfile:
        for i, fname in enumerate(input_files):
            with open(fname, 'rb') as infile:
                if i != 0:
                    infile.readline()  # Throw away header on all but first file
                shutil.copyfileobj(infile, outfile)  # Block copy rest of file from input to output without parsing


def block_copy_first(input_files, output_file):
    # write header first
    with open(output_file, 'wb') as outfile:
        with open(input_files[0], 'rb') as headerfile:
            header = headerfile.readline()
            outfile.write(header)

    # append top hit later
    with open(output_file, 'ab') as outfile:
        for i, fname in enumerate(input_files):
            with open(fname, 'rb') as infile:
                infile.readline()  # Throw away header on all but first file
                toprow = infile.readline()  # read first line after header
                outfile.write(toprow)


# %% All and Local Rules --------------------------------------------

rule all:
    input:
        # summary: single sample
        expand("results/{sample}/report/report_{sample}.html", sample=samples.index),
        expand("results/{sample}/report/{sample}.bakcharak.yaml", sample=samples.index),
        # summary: all samples
        "results/summary/summary_allsamples.tsv",
        "results/reports/summary.html",
        # collected summary files
        "results/summary/summary_bestrefererence.tsv",
        "results/summary/summary_platon.tsv",
        "results/summary/summary_plasmidfinder_short.tsv",
        "results/summary/summary_plasmidfinder.tsv",
        "results/summary/summary_mobile_amrgenes.tsv",
        "results/summary/summary_vfdb.tsv",
        "results/summary/summary_vfdb_short.tsv",
        "results/summary/summary_mlst_short.tsv",
        "results/summary/summary_mlst.tsv",
        # if bakta
        expand("results/{sample}/bakta/{sample}.txt", sample=samples.index) if config["params"]["do_bakta"] == True else [],
        "results/summary/summary_bakta.tsv" if config["params"]["do_bakta"] == True else [],
        # plasmidblaster
        "results/summary/summary_plasmidblaster_persubject_allbesthits.tsv",
        # [conditional] Salmonella rules
        "results/summary/summary_sistr_short.tsv" if config["species"] == "Salmonella" else [],
        "results/summary/summary_SPI_short.tsv" if config["species"] == "Salmonella" else [], # TODO check
        "results/summary/summary_salmonella_subspecies.tsv" if config["species"] == "Salmonella" else [],
        # [conditional] Ecoli rules
        "results/summary/summary_OH_short.tsv" if config["species"] == "Ecoli" else [],
        "results/summary/summary_ecoli_vf_short.tsv" if config["species"] == "Ecoli" else [],
        "results/summary/summary_stx.tsv" if config["species"] == "Ecoli" else [],
        "results/summary/summary_ecoli_pathotyping.tsv" if config["species"] == "Ecoli" else [],
        # [conditional] Bacillus rules
        #"results/summary/summary_btyper_vf_short.tsv" if config["species"] == "Bacillus" else [],
        #"results/summary/summary_panC_short.tsv" if config["species"] == "Bacillus" else [],
        #"results/summary/summary_mesopsychro.tsv" if config["species"] == "Bacillus" else [],
        "results/summary/bacillus_summary.tsv" if config["species"] == "Bacillus" else [],
        # [conditional] Staphylococcus
        "results/summary/summary_vfdb_extra.tsv" if config["species"] == "Staphylococcus" else [], # set A
        "results/summary/summary_sccmec.tsv" if config["species"] == "Staphylococcus" else [],
        "results/summary/summary_saureus_toxin.tsv" if config["species"] == "Staphylococcus" else [],
        # [conditional] Listeria
        "results/summary/summary_listeria_serotyping.tsv" if config["species"] == "Listeria" else [],
        # [conditional] ANI module
        expand("results/{sample}/reffinder/fastani.tsv", sample=samples.index) if config["params"]["do_ani"] == True else [],
        "results/summary/summary_fastani.tsv" if config["params"]["do_ani"] == True else [],
        # [conditional] bakta module
        expand("results/{sample}/bakta/{sample}.txt", sample = samples.index) if config["params"]["do_bakta"] == True else [],
        "results/summary/summary_bakta.tsv" if config["params"]["do_bakta"] == True else [],
        # [conditional] MLST CC
        "results/summary/summary_mlstCC.tsv" if config["params"]["mlst"]["cc"] == True else [],
        # plasmidblaster
        "results/summary/summary_plasmidblaster_persubject_allbesthits.tsv",
        # AMRFinder
        expand("results/{sample}/amrfinder/results_plus.tsv", sample=samples.index),
        "results/summary/summary_amrfinder.tsv",
        "results/summary/summary_amrfinder_classes.tsv",
        # platon
        #expand("results/{sample}/platon/{sample}.tsv", sample=samples.index),
        "results/summary/summary_platon.tsv",
        "results/summary/summary_mobile_amrgenes.tsv",
        # versions
        "results/summary/database_versions.tsv",
        "results/summary/software_versions.tsv",
        # genebrowser
        "results/summary/genebrowser_slim.tsv",
        "results/summary/genebrowser.tsv",


rule all_missing_files:
    input:
        "results/summary/summary_plasmidfinder_short.tsv",
        "results/summary/summary_plasmidfinder.tsv",
        "results/summary/summary_mobile_amrgenes.tsv",
        "results/summary/summary_vfdb.tsv",
        "results/summary/summary_vfdb_short.tsv",
        "results/summary/summary_mlst_short.tsv",
        "results/summary/summary_mlst.tsv",


rule all_minimal:
    input:
        "results/summary/summary_allsamples.tsv",
        "results/summary/genebrowser.tsv",

rule new_report:
    input:
        "results/reports/summary.html"

rule legacy_report:
    input:
        "results/reports/summary_legacy.html"

rule all_versions:
    input:
        "results/summary/database_versions.tsv",
        "results/summary/software_versions.tsv"

rule all_platon:
    input:
        "results/summary/summary_platon.tsv"

rule all_amrfinder:
    input:
        expand("results/{sample}/amrfinder/results_plus.tsv", sample=samples.index),
        "results/summary/summary_amrfinder.tsv",
        "results/summary/summary_amrfinder_classes.tsv"

rule all_detailed:
    input:
        "results/summary/summary_resfinder_short.tsv",
        "results/summary/summary_plasmidfinder.tsv",
        "results/summary/summary_plasmidfinder_short.tsv",
        expand("results/{sample}/mlst/report.tsv", sample=samples.index),
        "results/summary/summary_mlst.tsv",
        "results/summary/summary_mlst_short.tsv",
        "results/summary/summary_vfdb.tsv",
        "results/summary/summary_vfdb_short.tsv",
        expand("results/{sample}/bakta/{sample}.txt", sample=samples.index) if config["params"]["do_bakta"] == True else [],
        "results/reports/summary_legacy.html",
        expand("results/{sample}/stats/contig_stats.tsv", sample=samples.index),
        expand("results/{sample}/reffinder/mashscreen_plasmid_top.tsv", sample=samples.index),
        expand("results/{sample}/reffinder/mashscreen_genome_best.tsv", sample=samples.index),
        expand("results/{sample}/plasmidBlaster/results_blast.tsv", sample=samples.index),
        expand("results/{sample}/plasmidBlaster/results_stats.tsv", sample=samples.index),
        expand("results/{sample}/plasmidBlaster/results_stats_info.tsv", sample=samples.index),
        expand("results/{sample}/report/report_{sample}.html", sample=samples.index)

rule all_onlybacta:
    input:
        expand("results/{sample}/bakta/{sample}.gff3", sample=samples.index)

rule all_generic:
    input:
        expand("results/{sample}/report/report_{sample}.html", sample=samples.index),
        "results/reports/summary_legacy.html",
        #expand("results/{sample}/prokka/{sample}.features.tsv", sample=samples.index) if config["params"]["do_prokka"] else [],
        expand("results/{sample}/bakta/{sample}.txt", sample=samples.index) if config["params"]["do_bakta"] == True else [],
        "results/summary/summary_sistr_short.tsv" if config["species"] == "Salmonella" else [],
        "results/summary/summary_OH_short.tsv" if config["species"] == "Ecoli" else [],
        "results/summary/summary_ecoli_vf_short.tsv" if config["species"] == "Ecoli" else [],
        #"results/{sample}/reffinder/fastani.tsv" if (config["species"] == "Bacillus") or (config["species"] == "Aeromonas") else [],
        expand("results/{sample}/reffinder/fastani.tsv", sample=samples.index) if config["params"]["do_ani"] == True else [],
        "results/summary/summary_bestrefererence.tsv"

rule all_report:
    input:
        "results/reports/summary_legacy.html",

rule all_sample_reports:
    input:
        expand("results/{sample}/report/report_{sample}.html", sample=samples.index),

rule all_json:
    input:
        expand("results/{sample}/report/{sample}.bakcharak.json", sample=samples.index)

# %% Import Default Rules -------------------------------------------

include: "rules/mlst.smk" # 7 gene mlst
include: "rules/abricate.smk" # performs resfinder, plasmidfinder, vffinder
include: "rules/reporting.smk" # creates reports
include: "rules/reffinder.smk" # closest genomes, plasmids
include: "rules/other.smk" # contig stats
include: "rules/plasmidprediction.smk" # plasmid prediction with platon
include: "rules/amrfinder.smk" # resgene screening with amrfinder
include: "rules/extensions.smk" # new features in dev

# %% Import Conditional Rules ---------------------------------------

if config["params"]["do_bakta"] == True:
    include: "rules/bakta.smk" # performs annotation
    print("Including bakta.smk")

if config["params"]["do_ani"] == True:
    include: "rules/fastani.smk"
    print("Including fastani.smk")

if config["species"] == "Ecoli":
    include: "rules/ecoli_rules.smk" # rules for ecoli only (OH antigens, ecoli virulence genes)
    print("Including ecoli_rules.smk")

if config["species"] == "Salmonella":
    include: "rules/salmonella_rules.smk" # rules for salmonella only (sistr)
    print("Including salmonella_rules.smk")

if config["species"] == "Bacillus":
    include: "rules/bacillus_rules.smk"
    print("Including bacillus_rules.smk")

if config["species"] == "Staphylococcus":
    include: "rules/staphylococcus_rules.smk"
    print("Including staphylococcus_rules.smk")

if config["species"] == "Listeria":
    include: "rules/listeria_rules.smk"
    print("Including listeria_rules.smk")

#if config["species"] == "Bacillus":
#    include: "rules/fastani.smk"
#    print("Including fastani.smk")

#if config["species"] == "Aeromonas":
#    include: "rules/fastani.smk"
#    print("Including fastani.smk")
pass

# %% Logging Information --------------------------------------------

def pipeline_status_message(status: str) -> tuple:
    RESET = "\033[0m"
    RED = "\033[31m"
    GREEN = "\033[32m"

    timestamp = datetime.now()
    script_id = os.environ.get('script_id', 'unset')
    status_file = os.path.join(config['workdir'], f"pipeline_status_{config['pipeline'].lower()}.txt")
    status_chunk = f"{timestamp.strftime('%F %H:%M')}\t{snakemake.logging.logger.logfile}\t{os.environ.get('CONDA_PREFIX', 'CONDA_PREFIX_is_unset')}\t{os.environ.get('USER', 'unknown')}:{os.uname().nodename}\tShellScriptId:[{script_id}]\n"

    if status == "running":
        status = f"{status}\t{status_chunk}"
        message_chunk = f"[START] {os.path.abspath(__file__)}"
        message = f"[{timestamp.strftime('%F %T')}] {message_chunk}" if script_id == 'unset' else f"[{timestamp.strftime('%F %T')}][{script_id}] {message_chunk}"
    elif status == "success":
        status = f"{status}\t{status_chunk}"
        message_chunk = f"{GREEN}[SUCCESS]{RESET} Pipeline status: Workflow finished successfully"
        message = f"[{timestamp.strftime('%F %T')}] {message_chunk}" if script_id == 'unset' else f"[{timestamp.strftime('%F %T')}][{script_id}] {message_chunk}"
    elif status == "error":
        status = f"{status}\t{status_chunk}"
        message_chunk = f"{RED}[ERROR]{RESET} Pipeline status: An error occurred"
        message = f"[{timestamp.strftime('%F %T')}] {message_chunk}" if script_id == 'unset' else f"[{timestamp.strftime('%F %T')}][{script_id}] {message_chunk}"

    return status_file, status, message


onstart:
    status_file, status, message = pipeline_status_message(status = 'running')
    with open(status_file, 'w') as file:
        file.write(status)
    print(message)

onsuccess:
    status_file, status, message = pipeline_status_message(status = 'success')
    with open(status_file, 'w') as file:
        file.write(status)
    print(message)

onerror:
    status_file, status, message = pipeline_status_message(status = 'error')
    with open(status_file, 'w') as file:
        file.write(status)
    print(message)
