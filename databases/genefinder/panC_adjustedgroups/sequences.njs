{
  "version": "1.2",
  "dbname": "sequences",
  "dbtype": "Nucleotide",
  "db-version": 5,
  "description": "panC",
  "number-of-letters": 57715,
  "number-of-sequences": 68,
  "last-updated": "2023-12-05T17:39:00",
  "number-of-volumes": 1,
  "bytes-total": 64725,
  "bytes-to-cache": 15389,
  "files": [
    "sequences.ndb",
    "sequences.nhd",
    "sequences.nhi",
    "sequences.nhr",
    "sequences.nin",
    "sequences.nog",
    "sequences.not",
    "sequences.nsq",
    "sequences.ntf",
    "sequences.nto"
  ]
}
