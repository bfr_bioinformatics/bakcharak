{
  "version": "1.2",
  "dbname": "sequences",
  "dbtype": "Nucleotide",
  "db-version": 5,
  "description": "btyper_vf",
  "number-of-letters": 56118,
  "number-of-sequences": 35,
  "last-updated": "2023-12-05T17:49:00",
  "number-of-volumes": 1,
  "bytes-total": 59881,
  "bytes-to-cache": 14573,
  "files": [
    "sequences.ndb",
    "sequences.nhd",
    "sequences.nhi",
    "sequences.nhr",
    "sequences.nin",
    "sequences.nog",
    "sequences.not",
    "sequences.nsq",
    "sequences.ntf",
    "sequences.nto"
  ]
}
