cat btyper3_virulence_sequences.ffn | sed -E 's/^>[A-Za-z0-9\-]+ />btyper_vf~~~/' | sed -E 's/[|]+/~~~/' | sed 's/ Bacillus/~~~vf_class Bacillus/' | sed '/^$/d' > sequences
makeblastdb -in sequences -title btyper_vf -dbtype nucl -hash_index
/cephfs/abteilung4/deneke/repositories/abricate_databases/scripts/versionize_database.sh /cephfs/abteilung4/deneke/Projects/bakcharak_dev/Bacillus/btyper3/btyper3_db/btyper_bacillus_vf
