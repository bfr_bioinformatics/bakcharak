# prepare ani db

baseoutdir="/home/DenekeC/Snakefiles/bakcharak/databases/ani"

name="Yersinia"

dbdir="$baseoutdir/$name"

mkdir -p $dbdir

version_file=$dbdir/version.yaml

download_dir="/cephfs/abteilung4/deneke/Projects/bakcharak_dev/$name/ani_db/ncbi_refs"


mkdir $download_dir
# download 
echo "~/scripts_BfR/RefDownloader.sh $name $download_dir --rep"
 ~/scripts_BfR/RefDownloader.sh $name $download_dir --rep

#download_dir="/cephfs/abteilung4/deneke/Projects/bakcharak_dev/Vibrio/ncbi_refs/Vibrio"

 

 
# unzip
cd "$download_dir/$name"
gunzip -k *.fna.gz

# new names
#mkdir renamed

# scheme
#genus_species_strain_accession
#1,8,9

#accessiontable="/cephfs/abteilung4/deneke/Projects/bakcharak_dev/Vibrio/assembly_summary_refseq_Vibrio_representative.txt"
accessiontable="/cephfs/abteilung4/deneke/Projects/bakcharak_dev/Vibrio/ncbi_refs/Vibrio/assembly_summary.txt"
accessiontable="$download_dir/$name/assembly_summary.txt"


#file="/cephfs/abteilung4/deneke/Projects/bakcharak_dev/Vibrio/ncbi_refs/Vibrio/GCF_001547935.1_ASM154793v1_genomic.fna"


for file in $download_dir/$name/*.fna; do
    echo $file


    accession=`basename $file | cut -f 1-2 -d '_'`


    header=`grep '>' $file | head -n 1`

    echo "header: $header"


    count_check=`awk -F'\t' -v accession=$accession '{if($1 == accession) print $8,$9,$1}' $accessiontable | tr ' ' '_' | sed 's/strain=//' | wc -l`




    if [[ $count_check != 1 ]]; then
        echo "ERROR: count_check is $count_check. MUST be 1"
        break
    fi

    #newname=`awk -F'\t' -v accession=$accession '{if($1 == accession) print $8,$9,$1}' $accessiontable | tr ' ' '_' | sed 's/strain=//'`
    newname=`awk -F'\t' -v accession=$accession '{if($1 == accession) print $8"~"$9"~"$1}' $accessiontable | tr -d ':' | tr ' ' '_' | sed 's/strain=//' | sed 's/~~/~unnamed_strain~/'`
    echo "newname: $newname"

    echo "cp $file $dbdir/$newname.fna"
    cp $file $dbdir/$newname.fna

done

# add version info

checksum=`cat $dbdir/*.fna | md5sum | cut -f 1 -d ' '`

ls $dbdir/*.fna > $dbdir/referenceTypeStrains.txt


reference_file="$dbdir/referenceTypeStrains.txt"
count_references=`wc -l $reference_file | cut -f 1 -d ' '`

echo "name: $name" > $version_file
echo "type: ANI" >> $version_file
echo "creation_date: `date -I`" >> $version_file
echo "version: `date -I`" >> $version_file
echo "references: $count_references" >> $version_file
echo "reference_file: $reference_file" >> $version_file
echo "checksum: $checksum" >> $version_file



# bundle as zip
