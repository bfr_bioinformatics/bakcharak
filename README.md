# BakCharak: Bakterial Characterization of food-borne pathogens

[TOC]

## 📖 Description

BakCharak is a pipeline for the characterization of food-borne pathogens. It works on assembled draft and complete bacterial genomes.

* It consists of different modules, some of them applicable to all species and others that are species or genus specific
* A list of assembled genomes needs to be provided and bakcharak performs all analyses on all samples including an interactive summary report. 
* Once more samples are added to the sample list, only the new samples are analyzed. 
* If a newer genefinder database is supplied, all samples are updated with the results from the new gene search.

## Notes

The latest changes can be found in the [changelogs](CHANGELOG.md). Some new tools and databases were added in version v 3.1.2 that require a re-installation of conda and databases.

Some issues are related to differences in __NCBI amrfinder__ versions 3 and 4. This affects the parsing of the results and potentially also the bakta analysis. You may want to fix amrfinder to version 3 or 4. Therefore you can follow the instructions using __conda explicit__
, see [section using-conda-explicit](#using-conda-explicit) below.

## Modules

* Antimicrobial resistance gene finder [Tool: AMRfinder; Database: NCBI resistance gene database]
* Plasmidfinder [Tool: Abricate; Database: CGE plasmidfinder]
* Virulence factor finder [Tool: Abricate; Database: VFDB]
* Reference finder [Tool: mash; Database: NCBI refseq]
* Plasmid reference finder [Tool: mash; Database: NCBI plasmid database]
* PlasmidBlaster [Tool: Blastn; Database: NCBI plasmid database]
* Plasmid prediction [Tool: platon, Database: platon database]

These elements run regardless of the chosen bacterial taxa. A number of modules is available for the species-specific charakererization:

### Salmonella

* Serotyping [Tool: sistr; Database: sistr]
* cgMLST [Tool: sistr; Database: sistr 330 locus scheme]
* Salmonella pathogenicity islands (SPI) [Tool: abricate]
* Salmonella subspecies typing [Tool: mash]

### E. coli

* Search of O and H antigens [Tool: abricate; Database: CGE OH]
* Virulence factor finder [Tool: abricate; Database: CGE virulencefinder]
* Extraction of amrfinder stx subunits [Tool: AMRfinder; Database: NCBI resistance gene database]
* Full gene stx genes [Tool: abricate, Database: Custom stx database (provided)]

### Bacillus

* Average nucleotide identity estiation [Tool: fastANI; Database: BTyper Database]
* Btyper3 Databases: panC, virulence, species typing
* Extraction of meso/psychrotolerance [Tool: bakta]
* Identification of pesticidal proteins [Tool: bakta]

### Staphyloccus

* ssmec genes [Tool: abricate, Database: CGE]
* sccmec class via CGE  [Tool: CGE]
* sccmec class via staphopia [Tool: staphopia-sccmec]

### Listeria

* lissero serotyping [Tool: lissero]

### ANI databases for species typing

* Campylobacter, including hybrid species
* Bacillus
* Vibrio
* Aeromonas

### Graphical summary of workflow

![Alt text](https://gitlab.com/bfr_bioinformatics/bakcharak/raw/master/dag.svg)

## Set-Up

0. Create directory for this repository

```sh
REPO_PATH="path/2/parent" # select a path of your choice; bakcharak will be available below this path
mkdir -p $REPO_PATH # make sure that path exists
```

1. Clone this repository

```sh
cd $REPO_PATH
git clone https://gitlab.com/bfr_bioinformatics/bakcharak
```

2. Installation

An installation script is available at  `scripts/bakcharak_setup.sh`, which should be the only thing you need to run.

See `bash ./scripts/bakcharak_setup.sh --help` for options:

```
Basic usage:
bash ./scripts/bakcharak_setup.sh [OPTIONS]

Description:
This script completes the installation of the BakCharak pipeline. The openssl library is required for hashing the downloaded files.
For BakCharak installations from Gitlab use the option set [--mamba --databases].
For more information, please visit https://gitlab.com/bfr_bioinformatics/bakcharak

Options:
  -s, --status               Show installation status
  -e, --env_name             Override conda environment name derived from envs/bakcharak.yaml (default: bakcharak)
  -m, --mamba                Install the latest version of mamba to the conda base environment and
                             create the BakCharak environment from the git repository recipe
  -a, --amrfinder_update     Download the latest AMRfinder database
  -d, --databases            Download databases to <BakCharak>/download and extract them in <BakCharak>/databases
  -b, --bakta {light,full}   Download the BAKTA light (2G) or full (33G) database to <BakCharak>/download and extract it in <BakCharak>/databases/bakta
  -t, --test_data            Download test data to <BakCharak>/download and extract them in <BakCharak>/test_data
  -f, --force                Force overwrite for downloads in <BakCharak>/download
  -k, --keep_downloads       Do not remove downloads after extraction
  -v, --verbose              Print script debug info
  -h, --help                 Show this help
```

A simple installation including creation of an appropriate conda environment (by default, named `bakcharak`) and the required databases can be achieved by running:

```sh
bash ./scripts/bakcharak_setup.sh -m -d
```

If you want to give a different name to the conda environment, use the `-e` option:

```sh
bash ./scripts/bakcharak_setup.sh -m -d -e my_own_conda_env_name
```

**NOTE:** Please be aware that if you want to install or update additional databases (e.g., using the `-b light`, `-a` options), you will need to add the **same** `-e my_own_conda_env_name` to the call of the setup script.


#### Alternative installation options

##### Using mamba directly

The script bakcharak_setup.sh uses the following command for installing all software dependencies:

```sh
mamba env create -n bakcharak -f $repo_path/envs/bakcharak.yaml
```

You can replace mamba with conda, micromamba etc. and add additional commands from mamba and also choose a different yaml file and different environment name.

##### Using conda explicit

Resolving all software dependencies can be difficult and also time consuming. Therefore it may be convenient to install the software dependencies exactly with the same versions as previously installed. For this aim, we provide `*.explicit` files in the `env` subfolder.

You can install bakcharak with, e.g.

```sh
conda create --name bakcharak --file envs/bakcharak_latest.explicit
```

* Change the name if desired or if it already exists
* The path after `--file` depends on your current location and/or the path of your bakcharak repository
* Choose file `envs/bakcharak_amrfinderv3.explicit` if you want to fix the version if amrfinder to 3.12.8


Note that when installing bakcharak with the previous commands, you still need to install the amrfinder database using `amrfinder --update` - after activating the conda environment.


#### Logging 

The setup script provides quite comprehensive logging, both on the command line and in the `/tmp/${USER}_logs` folder (where `${USER}` is your user name on your machine). Should you run into issues during installation, please [open an issue](https://gitlab.com/bfr_bioinformatics/bakcharak/-/issues) and, if you can, provide us with the latest log files from this folder and your command line output. 

3. Set up databases

The setup script will attempt to download all required databases. Note that some of the databases are large (> 1GB).

In case there are problems with setting up the databases, please also check your proxy settings. 

* GeneFinder databases (resistance genes, PlasmidFinder) are provided with the conda environment. To find out the location activate the conda environment and type `abricate --help`

Furthermore, during setup of conda environment the databases are alternatively provided via `abricate`.

Additionally, the following database are also provided in this repo:

```
$REPO_PATH/bakcharak/databases/genefinder/ecoh
$REPO_PATH/bakcharak/databases/genefinder/ecoli_vf
$REPO_PATH/bakcharak/databases/genefinder/ncbi
$REPO_PATH/bakcharak/databases/genefinder/plasmidfinder
$REPO_PATH/bakcharak/databases/genefinder/vfdb
$REPO_PATH/bakcharak/databases/genefinder/btyper_bacillus_vf
$REPO_PATH/bakcharak/databases/genefinder/ecoh
$REPO_PATH/bakcharak/databases/genefinder/ecoli_vf
$REPO_PATH/bakcharak/databases/genefinder/mesopsychro
$REPO_PATH/bakcharak/databases/genefinder/panC
$REPO_PATH/bakcharak/databases/genefinder/saureus_toxin
$REPO_PATH/bakcharak/databases/genefinder/sccmec
$REPO_PATH/bakcharak/databases/genefinder/SPI
$REPO_PATH/bakcharak/databases/genefinder/stx
$REPO_PATH/bakcharak/databases/genefinder/vfdb_aeromonas_setA
$REPO_PATH/bakcharak/databases/genefinder/vfdb_saureus_setB
```

* ANI Database

You need a set of reference genome fasta files to be used for the ANI search. A file with the full path to all fasta files is required for an ANI search. The filename must contain the desired type strain naming.

4. Prepare data

All data must be supplied in a sample sheet (`samples.tsv`). It requires the following entries:

```
sample    assembly
sample_1    path/2/assembly_1.fasta
sample_2    path/2/assembly_1.fasta
```

The sample sheet can be updated and analysis will be run only on new samples.
The sample sheets can be created for all fasta files in a directory with the script create_sample `$REPO_PATH/bakcharak/scripts/create_sampleSheet.sh --mode assembly`

## Usage

The execution needs the path to the config file and to the Snakefile of this repository (`$REPO_PATH/bakcharak/Snakefile`). Furthermore you can specify the number of threads.

### Step 1: Activate conda

```
source activate bakcharak
```

### Step 2: Test workflow

Test your configuration by performing a dry-run via

```
$REPO_PATH/bakcharak/bakcharak.py --sample_list path/2/samples.tsv --working_directory path/2/output --species MYSPECIES --dryrun
```

### Step 3: Execute workflow

Execute the workflow via

```
$REPO_PATH/bakcharak/bakcharak.py --sample_list path/2/samples.tsv --working_directory  --species MYSPECIES path/2/output
```

### Important optional parameters

* `--species`: Species (or genus) name; `Salmonella`, `Ecoli`, `Bacillus`, `Aeromonas` add extra results (see above)
* `--config`: Use pre-existing configfile (config_bakcharak.yaml); overrides all other parameters
* `--bakta`: Also perform bakta annotation (requires most CPU time compare to other actions)
* `--ani`: Perform average nucleotide identity analysis for species identification. Requires `--ani_reference`, a text file with full paths to a set of reference fasta files
* `--dryrun`: Perform a dry-run without execution (creates a config file)
* `--threads`: Number of threads to use

### Further options

Specify `$REPO_PATH/bakcharak/bakcharak.py --help` for a list of all available options.

<details><summary>help file</summary>
<p>

```bash
$ python bakcharak.py --help
You are using bakcharak version 3.1.2
usage: bakcharak.py [-h] [-l SAMPLE_LIST] [-d WORKING_DIRECTORY] [-c CONFIG] [--species SPECIES] [-s SNAKEFILE] [--bakta] [--runname_position RUNNAME_POSITION] [--cc]
                    [--mlst_scheme MLST_SCHEME] [--mlst_db MLST_DB] [--bakta_genus BAKTA_GENUS] [--bakta_db BAKTA_DB] [--bakta_proteins BAKTA_PROTEINS]
                    [--bakta_extra BAKTA_EXTRA] [--minid MINID] [--mincov MINCOV] [--abricate_db ABRICATE_DB]
                    [--abricate_duplicationhandling ABRICATE_DUPLICATIONHANDLING] [--vfdb VFDB] [--amrfinder_db AMRFINDER_DB] [--phenomatch_db PHENOMATCH_DB]
                    [--match_phenotype] [--amrfinder_minid AMRFINDER_MINID] [--amrfinder_mincov AMRFINDER_MINCOV]
                    [--amrfinder_duplicationhandling AMRFINDER_DUPLICATIONHANDLING] [--mash_genome_db MASH_GENOME_DB] [--mash_genome_info MASH_GENOME_INFO]
                    [--mash_plasmid_db MASH_PLASMID_DB] [--show_bestref] [--plasmidblast_db PLASMIDBLAST_DB] [--platon_db PLATON_DB] [--platon_mode PLATON_MODE] [--ani]
                    [--ani_reference ANI_REFERENCE] [-t THREADS] [-n] [--threads_sample THREADS_SAMPLE] [--forceall] [--force FORCE] [--unlock] [--logdir LOGDIR]

optional arguments:
  -h, --help            show this help message and exit
  -l SAMPLE_LIST, --sample_list SAMPLE_LIST
                        List of samples to analyze, as a two column tsv file with columns sample and assembly. Can be generated with provided script create_sampleSheet,sh
  -d WORKING_DIRECTORY, --working_directory WORKING_DIRECTORY
                        Working directory where results are saved
  -c CONFIG, --config CONFIG
                        Path to pre-defined config file (Other parameters will be ignored)
  --species SPECIES     Species or genus name (default: "Unspecified Bacteria"). Special analyses are performed when choosing Salmonella, Ecoli, Bacillus, Staphylococcus.
                        see also for available amrfinder species with amrfinder -l
  -s SNAKEFILE, --snakefile SNAKEFILE
                        Path to Snakefile of bakcharak pipeline, default is path to Snakefile in same directory
  --bakta               Also run bakta
  --runname_position RUNNAME_POSITION
                        Position of runname in fasta path. Specify 0 to not export runname. default=0
  --cc                  Infer MLST CC from ST
  --mlst_scheme MLST_SCHEME
                        fix mlst scheme instead of autodetecting scheme (default: "").
  --mlst_db MLST_DB     Path to custom mlst database (default is conda db: /home/DenekeC/miniconda3/envs/bakcharak3/db).
  --bakta_genus BAKTA_GENUS
                        Bakta genus option. See bakta --help for details. Default = "Genus"
  --bakta_db BAKTA_DB   Path to location of bakta database for annotation; default: /cephfs/abteilung4/deneke/Snakefiles/bakcharak/databases/bakta
  --bakta_proteins BAKTA_PROTEINS
                        Path to extra protein fasta file for use in bakta
  --bakta_extra BAKTA_EXTRA
                        Extra, non-standard commands for bakta, e.g. --complete or --compliant. See bakta --help for options. use with care
  --minid MINID         Minimum identity in abricate, default=80
  --mincov MINCOV       Minimum coverage in abricate, default=50
  --abricate_db ABRICATE_DB
                        Path to abricate and prokka databases, default is database of conda environment
  --abricate_duplicationhandling ABRICATE_DUPLICATIONHANDLING
                        How to treat genes that occur more than once. #sum=sum values for each hit, max=value of best hit, first=value of first gene, all= add all values
                        seperated by ; (default=max)
  --vfdb VFDB           Path to non-standard vfdb for use in abricate; path must contain file "sequences" in abricate format
  --amrfinder_db AMRFINDER_DB
                        Path to amrfinder, default is database of conda environment ("path/2/condaenv/share/amrfinderplus/data/latest")
  --phenomatch_db PHENOMATCH_DB
                        Path to phenomatch_db, default is path/2/repo/databases/phenotypes/phenotype_amrfinder_matchtable_wPhenotypes.tsv
  --match_phenotype     Match resfinder phenotype to amrfinder results. Requires a proper phenomatch_db, default=False
  --amrfinder_minid AMRFINDER_MINID
                        Minimum identity in amrfinder, -1 means use a curated threshold if it exists and 0.9 otherwise (default=-1)
  --amrfinder_mincov AMRFINDER_MINCOV
                        Minimum coverage in amrfinder (default=0.5)
  --amrfinder_duplicationhandling AMRFINDER_DUPLICATIONHANDLING
                        How to treat genes that occur more than once. #sum=sum values for each hit, max=value of best hit, first=value of first gene, all= add all values
                        seperated by ; (default=max)
  --mash_genome_db MASH_GENOME_DB
                        Path to mash db (default: path/2/repo/databases/mash/genomes/refseq.genomes.k21s1000.msh)
  --mash_genome_info MASH_GENOME_INFO
                        Path to mash db (default: path/2/repo/databases/mash/genomes/refseq.genomes.k21s1000.info)
  --mash_plasmid_db MASH_PLASMID_DB
                        Path to mash db (default: path/2/repo/databases/mash/plasmids/plsdb.msh)
  --show_bestref        Display best mash reference genome in report
  --plasmidblast_db PLASMIDBLAST_DB
                        Path to location of plasmid blast database: plasmid_db (default: path/2/repo/databases/plasmidblast)
  --platon_db PLATON_DB
                        Path to location of platon database for plasmid prediction: plasmid_db; default: /cephfs/abteilung4/deneke/Snakefiles/bakcharak/databases/platon
  --platon_mode PLATON_MODE
                        applied filter mode in platon: sensitivity: RDS only (>= 95 % sensitivity); specificity: RDS only (>=99.9 % specificity); accuracy: RDS
                        characterization heuristics (highest accuracy) (default: "accuracy")
  --ani                 Perform species identification with average nucleotide identity. Requieres specification of ani_reference
  --ani_reference ANI_REFERENCE
                        Path to ANI reference file. Each line in the file must contain the full path to a reference fasta file
  -t THREADS, --threads THREADS
                        Number of Threads to use, default = 10
  -n, --dryrun          Snakemake dryrun. Only calculate graph without executing anything
  --threads_sample THREADS_SAMPLE
                        Number of Threads to use per sample, default = 1
  --forceall            Snakemake force. Force recalculation of all steps
  --force FORCE         Force a specific rule or file. Must be followed by rule or file, e.g. --force results/summary/summary_allsamples.tsv (default "". i.e. no force)
  --unlock              unlock snakemake
  --logdir LOGDIR       Path to directory where .snakemake output is to be saved



```

</p>
</details>

#### Call bakcharak from existing configfile

#### Call snakemake explicitly

```
snakemake -p --cores 20 --configfile path/2/config_bakcharak.yaml --snakefile $REPO_PATH/bakcharak/Snakefile --keep-going
```

See the [Snakemake documentation](https://snakemake.readthedocs.io) for further details.

## Output

All output is in the sub-directory `results`, all log files are in `log`.
An example outout is given in $REPO_PATH/test_results.

The sub-directory `reports` contains the interactive html report.
`/path/to/testdata/bakcharak/results/reports/summary.html`

The sub-directory `summary` contains aggegrated information of the individual methods for all samples analyzed:

* database_versions.tsv
* genebrowser_slim.tsv
* genebrowser.tsv
* software_versions.tsv
* summary_allsamples.tsv
* summary_amrfinder_classes.tsv
* summary_amrfinder_genecoverage.tsv
* summary_amrfinder_geneidentity.tsv
* summary_amrfinder.tsv
* summary_bakta.tsv
* summary_bestrefererence.tsv
* summary_mobile_amrgenes.tsv
* summary_plasmidblaster_persubject_allbesthits.tsv
* summary_plasmidblaster_persubject_filtered.tsv
* summary_plasmidblaster_persubject_nonredundant.tsv
* summary_platon.tsv
* summary_salmonella_subspecies.tsv
* summary_sistr_short.tsv
* summary_sistr.tsv
* summary_SPI_short.tsv
* summary_SPI.tsv

Some summary files differ by the choice of the species.
Each analyzed samples has its set of results file in a separate sub-directory of _results_. The results of  different analyses performed are are provided in individual sub-directories:

* mlst
* plasmidBlaster
* plasmidfinder
* reffinder
* amrfinder
* platon
* sistr
* stats
* vfdb

The interactive, per-sample html-report is located under `results`.

Results for each sample are saved in a machine-readable _json_ file and in a condensed _flat_ file.

## Test data

* Download and extract the test data

```sh
testDIR=path/to/testdata
cd $testDIR
wget -O assemblies.tar.gz https://gitlab.bfr.berlin/bfr_bioinformatics/bakcharak_resources/-/raw/main/testdata/assemblies.tar.gz
tar -xzvf assemblies.tar.gz
```

* Create sample sheet. A file samples.tsv is returned.

```
cd $testDIR/assemblies
$REPO_PATH/bakcharak/scripts/create_sampleSheet.sh --mode assembly
```

* Execute workflow

```
cp $REPO_PATH/bakcharak/config/config_Salmonella.yaml $testDIR
$REPO_PATH/bakcharak/bakcharak.py --sample_list $testDIR/assemblies/samples.tsv --working_directory $testDIR --species Salmonella
```

### Manual setup of databases (deprecated)

Please try first using the provided script `bakcharak_setup.sh`

* Download blast databases  (link to release)

```sh
wget -O $REPO_PATH/bakcharak/databases/plasmidblast.tar.gz https://gitlab.bfr.berlin/bfr_bioinformatics/bakcharak_resources/-/raw/main/databases/plasmidblast.tar.gz
tar -xzvf $REPO_PATH/bakcharak/databases/plasmidblast.tar.gz
```

* Download mash databases (link to release)

```sh
wget -O $REPO_PATH/bakcharak/databases/mash.tar.gz https://gitlab.bfr.berlin/bfr_bioinformatics/bakcharak_resources/-/raw/main/databases/mash.tar.gz
tar -xzvf $REPO_PATH/bakcharak/databases/mash.tar.gz
```

If you save these databases in a different location, please specify these using the `--mash_genome_db`, `--mash_genome_info`, `--mash_plasmid_db`, `--plasmidblast_db` flags.

* Platon database for plasmid prediction

The default location of the database is `path/2/repo/databases/platon`

Get the database from zenodo:

```
cd path/2/repo/databases
$ wget https://zenodo.org/record/4066768/files/db.tar.gz?download=1
$ tar -xzf db.tar.gz
$ rm db.tar.gz
$ mv db platon
```

See [here](https://github.com/oschwengers/platon) for details.

* amr finder database

With the conda environment avtivated

```
amrfinder -u
```

## ⚖️ License

BakCharak is distributed under the terms of the BSD 3-Clause License.

## ✉️ Authors and Contributors

* Carlus Deneke (@deneke)
* Holger Brendebach (@Brendy)
* Andreas Stroehlein (@stroehleina)

## References

### Software

* snakemake: https://github.com/snakemake/snakemake
* abricate: https://github.com/tseemann/abricate
* NCBI AMRFinderPlus - doi: 10.1128/AAC.00483-19
* mash doi: 10.1186/s13059-016-0997-x
* Blast: https://doi.org/10.1186/1471-2105-10-421
* sistr: https://doi.org/10.1371/journal.pone.0147101
* fastANI: https://doi.org/10.1038/s41467-018-07641-9
* R: https://www.R-project.org/
* platon: https://doi.org/10.1099/mgen.0.000398
* prokka: https://github.com/tseemann/prokka PMID:24642063
* bakta: https://doi.org/10.1099/mgen.0.000685
* mlst: https://github.com/tseemann/mlst
* lissero: https://github.com/MDU-PHL/LisSero
* staphopia-sccmec: https://github.com/staphopia/staphopia-sccmec

### Databases

* NCBI AMRFinderPlus - doi: 10.1128/AAC.00483-19
* PlasmidFinder - doi:10.1128/AAC.02412-14
* VFDB: doi:10.1093/nar/gkv1239
* NCBI Refseq:  doi: 10.1093/nar/gkv1189
* Platon database: https://doi.org/10.5281/zenodo.4066768
* Bakta database: https://doi.org/10.5281/zenodo.4247252
* EcOH - doi:10.1099/mgen.0.000064
* CGE virulencefinder database: DOI: 10.1128/JCM.03617-13 
* BTyper: https://github.com/lmc297/BTyper
* PubMLST: https://pubmlst.org/ doi: 10.1186/1471-2105-11-595
* bioconda: doi:10.1038/s41592-018-0046-7.
