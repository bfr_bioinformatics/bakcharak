# rules -----------------------------------------


rule run_amrfinder_sample:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        #ref = os.path.join(config["params"]["amrfinder"]["path"],"AMR_CDS"),
        version = os.path.join(config["params"]["amrfinder"]["path"],"version.txt"),
    output:
        report = "results/{sample}/amrfinder/results_plus.tsv",
        version = "results/{sample}/amrfinder/version.txt", 
        mutations = "results/{sample}/amrfinder/mutations_all.tsv",
    message: "Running run_sample on {wildcards.sample}"
    #conda:
    #   "../envs/amrfinder.yaml"
    log:
       "logs/amrfinder_{sample}.log"
    threads: config["smk_params"]["threads"]
    params:
        species = config["params"]["amrfinder"]["species"], #"-O Campylobacter", #
        extra = "--plus",
        MIN_COV = config["params"]["amrfinder"]["mincov"], #0.5,
        MIN_IDENT = config["params"]["amrfinder"]["minid"], #-1,
        database = config["params"]["amrfinder"]["path"] # either provided by conda env or separate path
    shell:
        """
        cat {input.version} > {output.version}
        amrfinder --version > {log}
        echo "amrfinder --threads {threads} {params.extra} --nucleotide {input.contigs} {params.species} -o {output.report} --mutation_all {output.mutations} --database {params.database} --ident_min {params.MIN_IDENT} --coverage_min {params.MIN_COV}" >> {log}
        amrfinder --threads {threads} {params.extra} --nucleotide {input.contigs} {params.species} -o {output.report} --mutation_all {output.mutations} --database {params.database} --ident_min {params.MIN_IDENT} --coverage_min {params.MIN_COV} &>> {log}
        """


rule match_phenotype_amrgenes:
    input:
        amrfinder_report = "results/{sample}/amrfinder/results_plus.tsv",
    output:
        amrfinder_phenotype_results = "results/{sample}/amrfinder/results_phenotype.tsv",
    message: "Add phenotype information to amrfinder results for sample {wildcards.sample}"
    #log: "logs/classify_mobile_amrgenes_{sample}.log"
    params:
        filter_amr = False, # TODO add to config
        #phenomatch_db = "/home/carlus/BfR/Snakemake/bakcharak/databases/phenotypes/phenotype_amrfinder_matchtable_wPhenotypes.tsv" # TODO add to config
        #phenomatch_db = "../databases/phenotypes/phenotype_amrfinder_matchtable_wPhenotypes.tsv" # TODO add to config
        phenomatch_db = config["params"]["amrfinder"]["phenomatch_db"],
    script:
        "../scripts/phenotype_matching.R"
        

rule decorate_amrgenes:
    input:
        #amrfinder_report = "results/{sample}/amrfinder/results_phenotype.tsv",
        amrfinder_report = "results/{sample}/amrfinder/results_phenotype.tsv" if config["params"]["amrfinder"]["match_phenotype"] else "results/{sample}/amrfinder/results_plus.tsv",
        platon_tsv = "results/{sample}/platon/{sample}.tsv", # version with prefix
        platon_contig_classification = "results/{sample}/platon/contig_classification.csv",
        report_contigstats = "results/{sample}/stats/contig_stats.tsv",
    output:
        amrfinder_decorated = "results/{sample}/amrfinder/results_decorated.tsv",
    message: "Decorate amrfinder results with contig information for sample {wildcards.sample}"
    #log: "logs/classify_mobile_amrgenes_{sample}.log"
    #params:
        #filter_amr = False, # TODO add to config
    script:
        "../scripts/decorate_amrfinder.R"


rule classify_mobile_amrgenes:
    input:
        #amrfinder_report = "results/{sample}/amrfinder/results_plus.tsv",
        #platon_contig_classification = "results/{sample}/platon/contig_classification.csv"
        amrfinder_report = "results/{sample}/amrfinder/results_decorated.tsv",
    output:
        amrfinder_platon_combi = "results/{sample}/amrfinder/mobile_genes.tsv",
    message: "Inferring mobile AMR genes for sample {wildcards.sample}"
    #log: "logs/classify_mobile_amrgenes_{sample}.log"
    params:
        element_type = "AMR", # "all", "STRESS", "VIRULENCE"
        sample = "{sample}"
    script:
        "../scripts/classify_mobile_amrgenes.R"


rule summarize_mobile_amr_classification:
    input:
        amrfinder_platon_combi = expand("results/{sample}/amrfinder/mobile_genes.tsv", sample= samples.index),
    output:
        mobileamr_summary = "results/summary/summary_mobile_amrgenes.tsv"
    message: "summarize_mobile_amr_classification"
    run:
        block_copy(input_files=input.amrfinder_platon_combi,output_file = output.mobileamr_summary)

    #run:
        #with open(output.mobileamr_summary, 'wb') as outfile:
            #for i, fname in enumerate(input.amrfinder_platon_combi):
                #with open(fname, 'rb') as infile:
                    #if i != 0:
                        #infile.readline()  # Throw away header on all but first file
                    #shutil.copyfileobj(infile, outfile)      # Block copy rest of file from input to output without parsing


rule summary_amrfinder:
    input:
        #report = expand("results/{sample}/amrfinder/results_plus.tsv",sample=samples.index)
        report = expand("results/{sample}/amrfinder/results_decorated.tsv",sample=samples.index)
    output:
        summary = "results/summary/summary_amrfinder.tsv",
    message: "Running summary_amrfinder"
    #log:
    #   "logs/summary_amrfinder.log"
    threads: 1 #    config
    script:
        "../scripts/summary_amrfinder.R"


rule summary_genes_amrfinder:
    input:
        report = expand("results/{sample}/amrfinder/results_plus.tsv",sample=samples.index)
        #report = expand("results/{sample}/amrfinder/results_decorated.tsv",sample=samples.index)
    output:
        class_summary = "results/summary/summary_amrfinder_classes.tsv",
        genecoverage_summary = "results/summary/summary_amrfinder_genecoverage.tsv",
        geneidentity_summary = "results/summary/summary_amrfinder_geneidentity.tsv",
    message: "Running genesummary_amrfinder"
    log:
       "logs/genesummary_amrfinder.log"
    threads: 1 #    config
    params:
        duplication_rule = config["params"]["amrfinder"]["duplication_rule"], #"all" #sum=sum values for each hit, max=best hit, first=first gene, all= add all values seperated by ';'
    script:
        "../scripts/genesummary_amrfinder.R"



# more -------------------------------------------------------------------------------------------------------------------------------------------------------------

# use amrfinder with prokka: https://github.com/ncbi/amr/wiki/Running-AMRFinderPlus#prokka-gff-files-incompatible

#USAGE:   amrfinder [--protein PROT_FASTA] [--nucleotide NUC_FASTA] [--gff GFF_FILE] [--database DATABASE_DIR] [--update] [--ident_min MIN_IDENT] [--coverage_min MIN_COV] [--organism ORGANISM] [--translation_table TRANSLATION_TABLE] [--plus] [--report_common] [--point_mut_all POINT_MUT_ALL_FILE] [--blast_bin BLAST_DIR] [--parm PARM] [--output OUTPUT_FILE] [--quiet] [--gpipe] [--threads THREADS] [--debug]
#HELP:    amrfinder --help
#VERSION: amrfinder --version

#OPTIONAL PARAMETERS:
#-p PROT_FASTA, --protein PROT_FASTA
    #Protein FASTA file to search
#-n NUC_FASTA, --nucleotide NUC_FASTA
    #Nucleotide FASTA file to search
#-g GFF_FILE, --gff GFF_FILE
    #GFF file for protein locations. Protein id should be in the attribute 'Name=<id>' (9th field) of the rows with type 'CDS' or 'gene' (3rd field).
#-d DATABASE_DIR, --database DATABASE_DIR
    #Alternative directory with AMRFinder database. Default: $AMRFINDER_DB
#-u, --update
    #Update the AMRFinder database
#-i MIN_IDENT, --ident_min MIN_IDENT
    #Minimum identity for nucleotide hit (0..1). -1 means use a curated threshold if it exists and 0.9 otherwise
    #Default: -1
#-c MIN_COV, --coverage_min MIN_COV
    #Minimum coverage of the reference protein (0..1)
    #Default: 0.5
#-O ORGANISM, --organism ORGANISM
    #Taxonomy group
    #Campylobacter|Escherichia|Klebsiella|Salmonella|Staphylococcus|Vibrio_cholerae
#-t TRANSLATION_TABLE, --translation_table TRANSLATION_TABLE
    #NCBI genetic code for translated BLAST
    #Default: 11
#--plus
    #Add the plus genes to the report
#--report_common
    #Report proteins common to a taxonomy group
#--point_mut_all POINT_MUT_ALL_FILE
    #File to report all target positions of reference point mutations
#--blast_bin BLAST_DIR
    #Directory for BLAST. Deafult: $BLAST_BIN
#--parm PARM
    #amr_report parameters for testing: -nosame -noblast -skip_hmm_check -bed
#-o OUTPUT_FILE, --output OUTPUT_FILE
    #Write output to OUTPUT_FILE instead of STDOUT
#-q, --quiet
    #Suppress messages to STDERR
#--gpipe
    #Protein identifiers in the protein FASTA file have format 'gnl|<project>|<accession>'
#--threads THREADS
    #Max. number of threads
    #Default: 4
#--debug
    #Integrity checks




# aggregate
#rule collect_fastp_stats:
    #input:
        #expand('trimmed/reports/{sample}.tsv', sample=samples.index)
    #output:
        #"fastp_summary.tsv"
    #message: "Collecting fastp stats."
    #shell:
        #"""
        #cat {input[0]} | head -n 1 > {output}
        #for i in {input}; do 
        #cat ${{i}} | tail -n +2 >> {output}
        #done
        #"""

# write report
#rule qc_html_report:
    #input:
        #workdir = config["workdir"],
        #stats = "fastp_summary.tsv",
    #output:
        #"fastp_summary.html"
    #conda:
        #"envs/rmarkdown_env.yaml"
    #message: "Creating QC Rmarkdown report"
    #script:
        #"scripts/write_QC_report.Rmd"
