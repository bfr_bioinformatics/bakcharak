#TODO reorder mash output columns, clean output and add sample name
#TODO mash instead of mash screen for genomes

# --------------------------------------------------------------------------------------------------------------------


rule run_mash_genome:
  input: 
    contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
    mashDB = config["params"]["mash"]["genome_reference"],
  output: 
    distance = temp("results/{sample}/reffinder/mash_genome.tsv") # make this a temporary file
  params: 
    mashDB = config["params"]["mash"]["genome_reference"], #"/cephfs/abteilung4/NGS/ReferenceDB/mash/refseq.genomes.k21s1000.msh",
    kmersize = config["params"]["mash"]["kmersize"], #21
    sketchsize = config["params"]["mash"]["sketchsize"] #1000
  message:
    "Running rule run_mash on {wildcards.sample}"
  log:
    "logs/{sample}_mash_genome.log"
  shell: 
    """
    mash dist  -s {params.sketchsize} {input.mashDB} {input.contigs} | awk -F '\t' 'BEGIN {{OFS="\t"}}; {{split($5,a,"/"); print $1, $2, $3, $4, a[1], a[2]}}' | sort -nr -k 5 > {output.distance}  2>&1 | tee {log}
    """
##echo "mash dist  -s {params.sketchsize} {input.mashDB} {input.contigs} | awk -F '\t' 'BEGIN {{OFS=\"\t\"}}; {{split($5,a,\"/\"); print $1, $2, $3, $4, a[1], a[2]}}' | sort -nr -k 5 > {output.distance}" > {log}

rule parse_mash_genome:
  input: 
    distance = "results/{sample}/reffinder/mash_genome.tsv"
  output: 
      best = "results/{sample}/reffinder/mash_genome_best.tsv"
  params: 
    mashinfo = config["params"]["mash"]["genome_info"]
  message:
    "Running rule parse_mash_genome on {wildcards.sample}"
  shell: 
      "paste <(head -n 1 {input.distance}) <(grep -f <(head -n 1 {input.distance} | cut -f 1) {params.mashinfo} | sed -E 's/^[[:space:]]+//' | sed -E 's/[[:space:]]{{2,}}/\t/g' | cut -f 4) > {output}"
     


rule parse_mash_best_genome:
  input: 
    best = "results/{sample}/reffinder/mash_genome_best.tsv"
  output: 
      best = "results/{sample}/reffinder/mash_genome_best.tmp"
  message:
    "Running rule parse_mash_best_genome on {wildcards.sample}"
  shell: 
      """
      awk -F'\t' -v sample={wildcards.sample} 'BEGIN{{OFS="\t"}};{{print sample,$3,$4,$5,$6,$1,$7}}' {input} > {output}
      """


rule summary_mash_genome:
  input: 
    tsv = expand("results/{sample}/reffinder/mash_genome_best.tmp", sample=samples.index)
    #expand("results/{sample}/reffinder/mash_genome_best.tsv", sample=samples.index)
  output:
    summary = "results/summary/summary_bestrefererence.tsv",
  message:
    "Running rule summary_mash_genome"
  run:
        header = "Sample\tdistance\tpvalue\tshared_hashes\ttotal_hashes\treference_accession\treference_info"
        with open(output.summary,'w') as outfile:
            outfile.write(header+"\n")
            for f in input.tsv:
                with open(f,'r') as fd:
                    shutil.copyfileobj(fd, outfile)



# -------------

rule mash_screen_genome:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        mash_reference = config["params"]["mash"]["genome_reference"],
    output:
        temp("results/{sample}/reffinder/mashscreen_genome.tsv")
    message: "Computing mash screen with genome reference for sample {wildcards.sample}"
    #log: "logs/mashscreen_genome_{sample}.log"
    threads: 1
    params:
        mash_reference = config["params"]["mash"]["genome_reference"],
    shell:
        """
        mash screen -w -p {threads} {input.mash_reference} {input.contigs}  | awk -F '\t' 'BEGIN {{OFS="\t"}}; {{split($2,a,"/"); print $1, a[1], a[2], $3, $4, $5, $6}}' | sort -nr -k 2 > {output}
        """

rule mash_screen_plasmid:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        mash_reference = config["params"]["mash"]["plasmid_reference"],
    output:
        temp("results/{sample}/reffinder/mashscreen_plasmid.tsv")
    message: "Computing mash screen with plasmid reference for sample {wildcards.sample}"
    threads: 1
    #log: "logs/mash_plasmid_{sample}.log"
    params:
        mash_reference = config["params"]["mash"]["plasmid_reference"], #"/cephfs/abteilung4/NGS/ReferenceDB/mash/refseq.plasmid.k21s1000.msh",
    shell:
        """
        mash screen -w -p {threads} {input.mash_reference} {input.contigs}  | awk -F '\t' 'BEGIN {{OFS="\t"}}; {{split($2,a,"/"); print $1, a[1], a[2], $3, $4, $5, $6}}' | sort -nr -k 2 > {output}
        """

       

rule parse_mashscreen_genome:
    input:
        "results/{sample}/reffinder/mashscreen_genome.tsv"
    output:
        "results/{sample}/reffinder/mashscreen_genome_best.tsv"
    message: "Summary of mash screen with genome reference for sample {wildcards.sample}"
    params:
        tmp = "results/{sample}/reffinder/mashscreen_genome_clean.tsv"
    shell:
        """
        grep -v -i 'phage' {input} > {params.tmp}
        head -n 1 {params.tmp} > {output}
        rm {params.tmp}
        """


rule parse_mash_plasmid:
    input:
        "results/{sample}/reffinder/mashscreen_plasmid.tsv"
    output:
        "results/{sample}/reffinder/mashscreen_plasmid_top.tsv"
    message: "Summary of mash screen with plasmid reference for sample {wildcards.sample}"
    params:
        identity_threshold = config["params"]["mash"]["identity_threshold"], #0.9, # identity threshold
        hash_threshold = config["params"]["mash"]["hash_threshold"], #300,
    shell:
        "awk -F'\t' -v identity_thr={params.identity_threshold} -v hash_thr={params.hash_threshold} '$1 > identity_thr && $2 > hash_thr ' {input} > {output}"


rule summary_mashscreen_genome:
    input:
        "results/{sample}/reffinder/mashscreen_genome_best.tsv"
    output:
        "results/summary/summary_mashscreen_genome.tsv"
    message: "Summary of mash screen with genome reference"
    shell:
        "cat {input} > {output}"
#TODO add sample, cleanup input before


# --------------------
# plasmidblaster


rule blast_plasmiddb:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        #reference= config["params"]["plasmidblast"]["reference"], # blank blast db name without extension cannot be found
    output:
        "results/{sample}/plasmidBlaster/results_blast.tsv"
    message: "Computing plasmidblaster for sample {wildcards.sample}"
    log: "logs/plasmidblast_{sample}.log"
    params:
        reference= config["params"]["plasmidblast"]["reference"], #"/cephfs/abteilung4/NGS/ReferenceDB/NCBI/Plasmids/plasmid_db", # NOTE make input?
        max_target_seqs = config["params"]["plasmidblast"]["max_target_seqs"], #500,
        max_hsps = config["params"]["plasmidblast"]["max_hsps"], #500,
        # qcov_hsp_perc = config["params"]["plasmidblast"]["qcov_hsp_perc"], #dropped!
        extra = "-subject_besthit", # only for newer blast BLAST+ 2.8.1 is released
        min_alignment_length = 1000, # TODO add to config config["params"]["plasmidblast"]["min_alignment_length"],
    shell:
        """
        echo "blastn -query {input} -db {params.reference} -outfmt '6 qaccver saccver pident length mismatch gapopen qstart qend sstart send evalue bitscore slen qlen qcovs qcovhsp qcovus' -max_target_seqs {params.max_target_seqs} -max_hsps {params.max_hsps} {params.extra} | awk -v min_length={params.min_alignment_length} '\$4 > min_length'  > {output}" > {log} 
        blastn -query {input} -db {params.reference} -outfmt '6 qaccver saccver pident length mismatch gapopen qstart qend sstart send evalue bitscore slen qlen qcovs qcovhsp qcovus' -max_target_seqs {params.max_target_seqs} -max_hsps {params.max_hsps} {params.extra} | awk -v min_length={params.min_alignment_length} '$4 > min_length'  > {output} 2>>{log} 
        """





rule parse_plasmidblast:
    input:
        "results/{sample}/plasmidBlaster/results_blast.tsv"
    output:
        "results/{sample}/plasmidBlaster/results_stats.tsv"
    message: "Parsing plasmidblaster for sample {wildcards.sample}"
#    log: "logs/contigstats_{sample}.log"
    params:
        tempfile = "results/{sample}/plasmidBlaster/results_stats.tmp.tsv"
    shell:
        """
        touch {params.tempfile}
        echo -e "Contig\tAccession\tIdentity\tQuery_length\tBitscore\tQuery_coverage" > {output}
        cut -f 1 {input} | sort | uniq | while read query; do
            awk -v query=$query 'BEGIN{{OFS="\t"}};{{if($1 == query) print $1,$2,$3,$4,$12,$15}}' {input} > {params.tempfile}
            head -n 1 {params.tempfile} >> {output}
        done
        rm {params.tempfile}
        """



rule plasmidblast_info:
    input:
        "results/{sample}/plasmidBlaster/results_stats.tsv"
    output:
        "results/{sample}/plasmidBlaster/results_stats_info.tsv"
    message: "Parsing plasmidblaster info for sample {wildcards.sample}"
#    log: "logs/contigstats_{sample}.log"
    params:
        plasmid_db_table = config["params"]["plasmidblast"]["plasmid_db_table"], #"/cephfs/abteilung4/NGS/ReferenceDB/NCBI/Plasmids/plasmid_db_table.tsv",
    shell:
        """
        echo -e "Contig\tAccession\tIdentity\tQuery_length\tBitscore\tQuery_coverage\tDetails" > {output}
            tail -n +2 {input} | while read Contig Accession Identity Query_length Bitscore Query_coverage; do
            search=`grep "$Accession" {params.plasmid_db_table} | cut -f 2`
            echo -e "$Contig\t$Accession\t$Identity\t$Query_length\t$Bitscore\t$Query_coverage\t$search" >> {output}
        done
        """


# new rule
rule run_plasmid_reference_finder:
  input:
      plasmidblast = "results/{sample}/plasmidBlaster/results_blast.tsv",
      amrfinder = "results/{sample}/amrfinder/results_plus.tsv"
  output:
      summary_per_subject_nonredundant = "results/{sample}/plasmidBlaster/summary_per_subject_nonredundant.tsv",
      summary_per_subject_allbesthits = "results/{sample}/plasmidBlaster/summary_per_subject_allbesthits.tsv",
      summary_per_subject_filtered = "results/{sample}/plasmidBlaster/summary_per_subject_filtered.tsv",
      results_blast_noredundancy = "results/{sample}/plasmidBlaster/results_blast_noredundancy.filtered.tsv"
  message: "Subject summary for plasmidblaster info for sample {wildcards.sample}"
  params:
    add_amr = True,
    sample = "{sample}",
  script:
      "../scripts/plasmid_reference_finder.R"

#aggregate
#rule aggregate_plasmidblaster_bestsubject_nonredundant:
  #input:
    #results_sample = expand("results/{sample}/plasmidBlaster/summary_per_subject_nonredundant.tsv",sample=samples.index)
  #output:
    #summary_tsv = "results/summary/summary_plasmidblaster_persubject_nonredundant.tsv"
  #message: "aggregate_plasmidblaster_bestsubject for all samples"
  #run:
    #block_copy(input_files=input.results_sample,output_file = output.summary_tsv)


#rule aggregate_plasmidblaster_filtered:
  #input:
    #results_sample = expand("results/{sample}/plasmidBlaster/summary_per_subject_filtered.tsv",sample=samples.index)
  #output:
    #summary_tsv = "results/summary/summary_plasmidblaster_persubject_filtered.tsv"
  #message: "aggregate_plasmidblaster_bestsubject for all samples"
  #run:
    #block_copy(input_files=input.results_sample,output_file = output.summary_tsv)



#rule aggregate_plasmidblaster_bestsubject:
  #input:
    #results_sample = expand("results/{sample}/plasmidBlaster/summary_per_subject_allbesthits.tsv",sample=samples.index)
  #output:
    #summary_tsv = "results/summary/summary_plasmidblaster_persubject_allbesthits.tsv"
  #message: "aggregate_plasmidblaster_bestsubject for all samples"
  #run:
    #block_copy(input_files=input.results_sample,output_file = output.summary_tsv)


# one rule for all
rule aggregate_plasmidblaster_all:
  input:
    summary_per_subject_filtered = expand("results/{sample}/plasmidBlaster/summary_per_subject_filtered.tsv",sample=samples.index),
    summary_per_subject_allbesthits = expand("results/{sample}/plasmidBlaster/summary_per_subject_allbesthits.tsv",sample=samples.index),
    summary_per_subject_nonredundant = expand("results/{sample}/plasmidBlaster/summary_per_subject_nonredundant.tsv",sample=samples.index)
  output:
    summary_plasmidblaster_persubject_filtered = "results/summary/summary_plasmidblaster_persubject_filtered.tsv",
    summary_plasmidblaster_persubject_allbesthits = "results/summary/summary_plasmidblaster_persubject_allbesthits.tsv",
    summary_plasmidblaster_persubject_nonredundant = "results/summary/summary_plasmidblaster_persubject_nonredundant.tsv"
  message: "aggregate_plasmidblaster for all samples"
  run:
    block_copy(input_files=input.summary_per_subject_filtered,output_file = output.summary_plasmidblaster_persubject_filtered)
    block_copy(input_files=input.summary_per_subject_allbesthits,output_file = output.summary_plasmidblaster_persubject_allbesthits)
    block_copy(input_files=input.summary_per_subject_nonredundant,output_file = output.summary_plasmidblaster_persubject_nonredundant)




#
# ----------------------------------------------------------------
# Reference/ Further ideas


# blastn2nr yields many chromosomal hits where blastn2plasmids yields plasmid hits
#NZ_CP027322.1
#blastdbcmd -db nr -entry "CP009863.1" -outfmt %t
#blastdbcmd -entry all -db nt | grep "CP009863.1"

#blastn -query /cephfs/abteilung4/deneke/Projects/AssemblyDB/Klebsiella/fasta/1701.fasta -db /cephfs/abteilung4/NGS/ReferenceDB/NCBI/Plasmids/plasmid_db -outfmt '6 qaccver saccver pident length mismatch gapopen qstart qend sstart send evalue bitscore slen qlen qcovs qcovhsp qcovus' -max_target_seqs 100 -max_hsps 1 -qcov_hsp_perc 50 > results/1701/plasmidBlaster/results_blast.tsv

# more hsps
#blastn -query /cephfs/abteilung4/deneke/Projects/AssemblyDB/Klebsiella/fasta/10088-2.fasta -db /cephfs/abteilung4/NGS/ReferenceDB/NCBI/Plasmids/plasmid_db -outfmt '6 qaccver saccver pident length mismatch gapopen qstart qend sstart send evalue bitscore slen qlen qcovs qcovhsp qcovus' -max_target_seqs 500 -max_hsps 500 -qcov_hsp_perc 50 > results/10088-2/plasmidBlaster/results_blast_full.tsv

# against nr
#blastn -query /cephfs/abteilung4/deneke/Projects/AssemblyDB/Klebsiella/fasta/10088-2.fasta -db /cephfs/abteilung4/NGS/ReferenceDB/NCBI/BLAST/nt -outfmt '6 qaccver saccver pident length mismatch gapopen qstart qend sstart send evalue bitscore slen qlen qcovs qcovhsp qcovus' -max_target_seqs 500 -max_hsps 500 -qcov_hsp_perc 50 > results/10088-2/plasmidBlaster/results_blast_nr.tsv

# parse best hit per query
#blast_result="results/10088-2/plasmidBlaster/results_blast_test.tsv"
#blastn -query /cephfs/abteilung4/deneke/Projects/AssemblyDB/Klebsiella/fasta/10088-2.fasta -db /cephfs/abteilung4/NGS/ReferenceDB/NCBI/Plasmids/plasmid_db -outfmt '6 qaccver saccver pident length mismatch gapopen qstart qend sstart send evalue bitscore slen qlen qcovs qcovhsp qcovus' -max_target_seqs 100 -max_hsps 1 -qcov_hsp_perc 20 > $blast_result
#output="results/10088-2/plasmidBlaster/results_stats_test.tsv"
#echo -e "Contig\tAccession\tIdentity\tQuery_length\tBitscore\tQuery_coverage" > $output
#cut -f 1 $blast_result | sort | uniq | while read query; do
  #awk -v query=$query 'BEGIN{{OFS="\t"}};{if($1 == query) print $1,$2,$3,$4,$12,$15}' $blast_result | head -n 1 >> $output
#done

# blast stats
#echo -e "Accession\tContig_count\tSummed_length\tReference_length\tReference_fraction\tReference_name" > {output}
#paste <(sort -k 2 {input} | awk 'BEGIN{{OFS="\t"}};{{plength[$2]+=$4; contigcount[$2]+=1}}; END{{for(q in plength)print q,plength[q],contigcount[q]}}' | sort -k 1) <(sort -k 2 {input} | cut -f 2 | uniq | grep -f - {params.plasmid_db_table} | sort -k 1) | awk -F'\t' 'BEGIN{{OFS="\t"}};{{print $1,$3,$2,$6,$2/$6,$5}}' | sort -nr -k 3 >> {output}








# plasmid_masher
#%i not useful: since contigs are small compared to genome, shared hashes are small
# mash against entire plasmid+genomes with -i option; keep relevant info only
#mash_ref="/cephfs/abteilung4/NGS/ReferenceDB/mash/refseq.plasmid.k21s1000.msh"
#mash_ref="/cephfs/abteilung4/NGS/ReferenceDB/mash/refseq.genomes+plasmid.k21s1000.msh"
#fasta="/cephfs/abteilung4/deneke/Projects/AssemblyDB/Klebsiella/fasta/10088-2.fasta"
#mashout="mash_sketch"
#echo "mash sketch -i -p 10  -k 21 -s 1000 -o $mashout $fasta"
#mash sketch -i -p 10  -k 21 -s 1000 -o $mashout $fasta
#mash dist $mash_ref $mashout.msh | sed 's/\/1000/\t1000/' > mash.dist








