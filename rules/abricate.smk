# rules for resgenes

# -----------------------------

#rule run_abricate_resfinder:
    #input:
        #contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        #ref = os.path.join(config["params"]["resfinder"]["path"],config["params"]["resfinder"]["name"],"sequences")
    #output:
        #report = "results/{sample}/resfinder/report.tsv"
    #message: "Running rule run_abricate_resfinder on {wildcards.sample} with contigs"
    #log:
       #"logs/abricate_resfinder_{sample}.log"
    #threads:
       #config["smk_params"]["threads"]
    #params:
        #refdb = config["params"]["resfinder"]["path"], #"/cephfs/abteilung4/NGS/ReferenceDB/NCBI/ResDB",
        #dbname = config["params"]["resfinder"]["name"], #"ncbi",
        #minid = config["params"]["resfinder"]["minid"],
        #mincov = config["params"]["resfinder"]["mincov"]
    #shell: 
       #"abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report} 2> >(tee {log} >&2)"


# TODO convert database file from ncbi raw to abricate ready
#>1028110379|WP_063864749.1|NG_050142|1|1|blaSRT-1|blaSRT|hydrolase|class_C_extended-spectrum_beta-lactamase_SRT-1 1035504224:101-1237
#>ncbi~~~blaSRT-1~~~A7J11_01107 class C extended-spectrum beta-lactamase SRT-1
# TODO check parameters in nullarbor


#rule abricate_summary_resfinder:
    #input:
        #expand("results/{sample}/resfinder/report.tsv", sample=samples.index)
    #output:
        #"results/summary/summary_resfinder.tsv"
    #message: "Running rule abricate_summary_resfinder for all samples"
    #log: "logs/abricate_summary.log"
    #params:
        #tmpfile = "results/summary/summary_resfinder.tmp"
    #shell:
        #"""
        #abricate --summary {input} > {params.tmpfile}
        #paste <(cut -f 1 {params.tmpfile} | awk -F'/' '{{print $2}}' | sed 's/^$/#Sample/') <(cut -f 2- {params.tmpfile}) > {output}
        #"""

#rule abricate_summary_resfinder_short:
    #input:
        #"results/summary/summary_resfinder.tsv"
    #output:
        #"results/summary/summary_resfinder_short.tsv"
    #message: "Runnign rule abricate_summary_resfinder for all samples"
    #log: "logs/abricate_summary.log"
    #params:
    #shell:
        #"""
        #echo -e "#Sample\tcount_resgenes\tresgenes" > {output}
        #paste <(cut -f 1-2 {input} | tail -n +2 ) <(cut -f 3- {input} | awk 'BEGIN{{OFS="\t"}}; {{ if(NR == 1) {{for(i = 1; i <= NF; i++) {{genes[i] = $i }} }}  else {{for(i = 1; i <= NF; i++) {{ if($i != "." ) {{$i=genes[i]; }} }} print $0 }} }}; ' | tr '\t' ';' | sed 's/[.];//g' | sed 's/;[.]//g' | sed 's/[.]/NA/') >> {output}
        #"""

#rule abricate_resfinder_phenotype:
    #input:
        #expand("results/{sample}/resfinder/report.tsv", sample=samples.index)
    #output:
        #"results/summary/summary_resfinder_phenotype.tsv"
    #message: "Parsing resistance phenotype"
    #log: "logs/resfinder_phenotype.log"
    #shell:
        #"""
            #echo -e "#Sample\tphenotype_count\tphenotype" > {output}
            #for report in {input}; do
                #sample=`basename $(dirname $(dirname $report))`
                #phenotypes=`tail -n +2 $report | cut -f 15 | sort | uniq | tr '\n' ';'`
                #phenotype_count=`tail -n +2 $report | cut -f 15 | sort | uniq | wc -l`
                #echo -e "$sample\t$phenotype_count\t$phenotypes" >> {output}
            #done
        #"""


#rule run_abricate_plasmidfinder:
#"abricate --nopath --db plasmidfinder $file > $abricate_plasmidfinder/${sample}.out 2> >(tee $abricate_plasmidfinder/log/${sample}_abricate.log >&2)"

rule run_abricate_plasmidfinder:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        ref = os.path.join(config["params"]["plasmidfinder"]["path"],config["params"]["plasmidfinder"]["name"],"sequences")
    output:
        report = "results/{sample}/plasmidfinder/report.tsv"
    message: "Running rule run_abricate_plasmidfinder on {wildcards.sample} with contigs"
    log:
       "logs/abricate_plasmidfinder_{sample}.log"
    threads:
       config["smk_params"]["threads"]
    params:
        refdb = config["params"]["plasmidfinder"]["path"], #"/cephfs/abteilung4/NGS/ReferenceDB/NCBI/ResDB",
        dbname = config["params"]["plasmidfinder"]["name"], #"ncbi",
        minid = config["params"]["plasmidfinder"]["minid"],
        mincov = config["params"]["plasmidfinder"]["mincov"]
    shell: 
        """
            abricate --version > {log}
            if [[ -f {params.refdb}/{params.dbname}/version.yaml ]]; then
                cat {params.refdb}/{params.dbname}/version.yaml >> {log}
            fi
            echo "date of analysis: `date -I`"  >> {log}
            echo "abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report}" >> {log}
            abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report} 2> >(tee -a {log} >&2)
        """



rule abricate_summary_plasmidfinder:
    input:
        abricate_report = expand("results/{sample}/plasmidfinder/report.tsv", sample=samples.index)
    output:
        summary_matrix = "results/summary/summary_plasmidfinder.tsv",
        summary_short = "results/summary/summary_plasmidfinder_short.tsv"
    message: "Running rule abricate_summary_plasmidfinder for all samples"
    params:
        duplication_rule = "all",
        returnvalue = "coverage"
    script:
        "../scripts/abricate_summary.R"



rule run_abricate_vfdb:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        ref = os.path.join(config["params"]["vfdb"]["path"],config["params"]["vfdb"]["name"],"sequences")
    output:
        report = "results/{sample}/vfdb/report.tsv"
    message: "Running rule run_abricate_vfdb on {wildcards.sample} with contigs"
    log:
       "logs/abricate_vfdb_{sample}.log"
    threads:
       config["smk_params"]["threads"]
    params:
        refdb = config["params"]["vfdb"]["path"], #"/cephfs/abteilung4/NGS/ReferenceDB/NCBI/ResDB",
        dbname = config["params"]["vfdb"]["name"], #"ncbi",
        minid = config["params"]["vfdb"]["minid"],
        mincov = config["params"]["vfdb"]["mincov"]
    shell: 
        """
        abricate --version > {log}
        if [[ -f {params.refdb}/{params.dbname}/version.yaml ]]; then
            cat {params.refdb}/{params.dbname}/version.yaml >> {log}
        fi
        echo "date of analysis: `date -I`"  >> {log}
        echo "abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report}" >> {log}
        abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report} 2>> >(tee -a {log} >&2)
        """


rule abricate_summary_vfdb:
    input:
        abricate_report = expand("results/{sample}/vfdb/report.tsv", sample=samples.index)
    output:
        summary_matrix = "results/summary/summary_vfdb.tsv",
        summary_short = "results/summary/summary_vfdb_short.tsv"
    message: "Running rule abricate_summary_plasmidfinder for all samples"
    params:
        duplication_rule = "all",
        returnvalue = "coverage"
    script:
        "../scripts/abricate_summary.R"
