# fastani for selected organisms
# need to prepare selected reference files, e.g. one per each species

rule compute_ani:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly')
    output:
        "results/{sample}/reffinder/fastani.tsv"
    message: "Computing fastani for sample {wildcards.sample}"
    log: "logs/fastani_{sample}.log"
    params:
        reference = config["params"]["fastani"]["reference_list"],
    shell:
        """
        echo "fastANI -q {input} --rl {params.reference} -o {output}" > {log}
        fastANI -q {input} --rl {params.reference} -o {output}
        """

rule sample_summary_ani:
    input:
        report = "results/{sample}/reffinder/fastani.tsv"
    output:
        summary = "results/{sample}/reffinder/fastani.summary.tsv"
    message: "fastani summary for sample"
    script:
        "../scripts/fastani_sample_summary.R"


rule summary_ani:
    input:
        tsv = expand("results/{sample}/reffinder/fastani.summary.tsv", sample=samples.index)
    output:
        tsv = "results/summary/summary_fastani.tsv",
    message: "Computing fastani summary"
    #shell:
        #"""
        #head -n 1 {input[0]} > {output} #header
        #for file in {input}; do tail -n +2 $file | head -n 1 >> {output}; done #top hit per sample
        #"""
    run:
        block_copy_first(input.tsv,output.tsv)

    # Do not apply here since it takes all values!
    #    block_copy(input_files=input.tsv,output_file = output.tsv)








