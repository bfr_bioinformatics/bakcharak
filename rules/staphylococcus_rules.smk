# extra rules for staph


# saureus_toxin
rule run_abricate_saureus_toxin:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        ref = os.path.join(config["params"]["saureus_toxin"]["path"],config["params"]["saureus_toxin"]["name"],"sequences")
    output:
        report = "results/{sample}/saureus_toxin/report.tsv"
    message: "Running rule run_abricate_saureus_toxin on {wildcards.sample} with contigs"
    log:
       "logs/abricate_saureus_toxin_{sample}.log"
    threads:
       config["smk_params"]["threads"]
    params:
        refdb = config["params"]["saureus_toxin"]["path"],
        dbname = config["params"]["saureus_toxin"]["name"],
        minid = config["params"]["saureus_toxin"]["minid"],
        mincov = config["params"]["saureus_toxin"]["mincov"]
    shell: 
       """
       echo "abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report}" | tee {log}
       abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report} 2> >(tee -a {log} >&2)
       """



rule abricate_summary_saureus_toxin:
    input:
        abricate_report = expand("results/{sample}/saureus_toxin/report.tsv", sample=samples.index)
    output:
        summary_matrix = "results/summary/summary_saureus_toxin.tsv",
        summary_short = "results/summary/summary_saureus_toxin_short.tsv"
    message: "Running rule abricate_summary_saureus_toxin for all samples"
    #log: "logs/abricate_summary.log"
    params:
        #tmpfile = "results/summary/summary_saureus_toxin.tmp"
        duplication_rule = "all",
        returnvalue = "coverage"
    script:
        "../scripts/abricate_summary.R"
    #shell:
        #"""
        #abricate --summary {input} > {params.tmpfile}
        #paste <(cut -f 1 {params.tmpfile} | awk -F'/' '{{print $2}}' | sed 's/^$/#Sample/') <(cut -f 2- {params.tmpfile}) > {output}
        #rm {params.tmpfile}
        #"""

# sccmec
rule run_abricate_sccmec:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        ref = os.path.join(config["params"]["sccmec"]["path"],config["params"]["sccmec"]["name"],"sequences")
    output:
        report = "results/{sample}/sccmec/report.tsv"
    message: "Running rule run_abricate_sccmec on {wildcards.sample} with contigs"
    log:
       "logs/abricate_sccmec_{sample}.log"
    threads:
       config["smk_params"]["threads"]
    params:
        refdb = config["params"]["sccmec"]["path"],
        dbname = config["params"]["sccmec"]["name"],
        minid = config["params"]["sccmec"]["minid"],
        mincov = config["params"]["sccmec"]["mincov"]
    shell: 
        """
        abricate --version > {log}
        cat {params.refdb}/{params.dbname}/version.yaml >> {log}
        echo "date of analysis: `date -I`"  >> {log}
        echo "abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report}" >> {log}
        abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report} 2> >(tee -a {log} >&2)
        """

rule abricate_summary_sccmec:
    input:
        #expand("results/{sample}/sccmec/report.tsv", sample=samples.index)
        abricate_report = expand("results/{sample}/sccmec/report.tsv", sample=samples.index)
    output:
        #"results/summary/summary_sccmec.tsv"
        summary_matrix = "results/summary/summary_sccmec.tsv",
        summary_short = "results/summary/summary_sccmec_short.tsv"
    message: "Running rule abricate_summary_sccmec for all samples"
    #log: "logs/abricate_summary.log"
    params:
#        tmpfile = "results/summary/summary_sccmec.tmp"
        duplication_rule = "all",
        returnvalue = "coverage"
    script:
        "../scripts/abricate_summary.R"
    #shell:
        #"""
        #abricate --summary {input} > {params.tmpfile}
        #paste <(cut -f 1 {params.tmpfile} | awk -F'/' '{{print $2}}' | sed 's/^$/#Sample/') <(cut -f 2- {params.tmpfile}) > {output}
        #rm {params.tmpfile}
        #"""


# get type from CGE sccmecfinder


rule get_sccmec_class:
    input:
        report = "results/{sample}/sccmec/report.tsv"
    output:
        classreport = "results/{sample}/sccmec/classreport.tsv"
    message: "getting sccmec class from abricate {wildcards.sample}"
    shell:
        """
        echo "{workflow.basedir}/scripts/get_sccmec_class.py -i {input.report} -s {wildcards.sample} -o {output.classreport}"
        {workflow.basedir}/scripts/get_sccmec_class.py -i {input.report} -s {wildcards.sample} -o {output.classreport}
        """


rule collect_sccmec_class_reports:
    input:
        classreport = expand("results/{sample}/sccmec/classreport.tsv",sample=samples.index)
    output:
        summary = "results/summary/summary_sccmec_classes.tsv"
    message: "collecting sccmec class reports for all samples"
    run:
        block_copy(input_files=input.classreport,output_file = output.summary)


# staphopia-sccmec
# mamba create -n staphopia-sccmec -c conda-forge -c bioconda staphopia-sccmec
# staphopia-sccmec --assembly /cephfs/abteilung4/deneke/Projects/bakcharak_dev/Staphyloccocus/sccmec/test/22-ST00789-0.fasta --ext fasta > 22-ST00789-0.tsv
#--sccmec SCCMEC_DATA
#--ext STR             Extension used by assemblies. (Default: fna)
#  --hamming             Report the hamming distance of each type.
#  --json                Report the output as JSON (Default: tab-delimited)
#--version

rule run_staphopia_sccmec:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        #ref = os.path.join(config["params"]["saureus_toxin"]["path"],config["params"]["saureus_toxin"]["name"],"sequences")
    output:
        report = "results/{sample}/sccmec/results.tsv",
        hamming = "results/{sample}/sccmec/results_hamming.tsv",
    message: "Running rule run_staphopia-sccmec on {wildcards.sample}"
    log:
       "logs/sccmec_{sample}.log"
    threads:
       config["smk_params"]["threads"]
    params:
        #db = config["params"]["sccmec"]["path"], # OR use from conda
    shell: 
       """
       # GET file extension
       ext=`echo {input.contigs} | awk -F. '{{print $NF}}'`
       echo "ext: $ext"
       echo "staphopia-sccmec --assembly {input.contigs} --ext $ext > {output.report}"
       staphopia-sccmec --assembly {input.contigs} --ext $ext > {output.report}
       echo "staphopia-sccmec --hamming --assembly {input.contigs} --ext $ext > {output.hamming}"
       staphopia-sccmec --hamming --assembly {input.contigs} --ext $ext > {output.hamming}
       """


rule aggregate_staphopia_sccmec:
    input:
        files = expand("results/{sample}/sccmec/results.tsv", sample=samples.index),
        files_hamming = expand("results/{sample}/sccmec/results_hamming.tsv", sample=samples.index)
    output:
        summary = "results/summary/summary_staphopia_sccmec.tsv",
        summary_hamming = "results/summary/summary_staphopia_sccmec_hamming.tsv"
    message: "Summarizing all sccmec reports"
    run:
        block_copy(input_files=input.files,output_file = output.summary)
        block_copy(input_files=input.files_hamming,output_file = output.summary_hamming)

    


# vfdb_saureus_extra ( genes collected from different sources, DTU, ANSES)
rule run_abricate_vfdb_extra:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        ref = os.path.join(config["params"]["vfdb_extra"]["path"],config["params"]["vfdb_extra"]["name"],"sequences")
    output:
        report = "results/{sample}/vfdb_extra/report.tsv"
    message: "Running rule run_abricate_vfdb_extra on {wildcards.sample} with contigs"
    log:
       "logs/abricate_vfdb_extra_{sample}.log"
    threads:
       config["smk_params"]["threads"]
    params:
        refdb = config["params"]["vfdb_extra"]["path"],
        dbname = config["params"]["vfdb_extra"]["name"],
        minid = config["params"]["vfdb_extra"]["minid"],
        mincov = config["params"]["vfdb_extra"]["mincov"]
    shell: 
        """
        abricate --version > {log}
        cat {params.refdb}/{params.dbname}/version.yaml >> {log}
        echo "date of analysis: `date -I`"  >> {log}
        echo "abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report}" >> {log}
        abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report} 2>> >(tee -a {log} >&2)
        """

rule abricate_summary_vfdb_extra:
    input:
        abricate_report = expand("results/{sample}/vfdb_extra/report.tsv", sample=samples.index)
    output:
        summary_matrix = "results/summary/summary_vfdb_extra.tsv",
        summary_short = "results/summary/summary_vfdb_extra_short.tsv"
    message: "Running rule abricate_summary_extra for all samples"
    params:
        duplication_rule = "all",
        returnvalue = "coverage"
    script:
        "../scripts/abricate_summary.R"

