# primary rules -----------------------------------------------------------------------

rule run_sistr:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly')
    output:
#        serovar = "results/{sample}/sistr/{sample}_serovar.txt",
        json = "results/{sample}/sistr/{sample}_allele-results.json",
        novel_alleles = "results/{sample}/sistr/{sample}_novel-alleles.fasta",
        cgmlst_profile = "results/{sample}/sistr/{sample}_cgmlst-profiles.csv",
        tsv = "results/{sample}/sistr/{sample}_sistr-output.tab"
    message: "Running rule run_sistr on {wildcards.sample}"
    log: "logs/sistr_{sample}.log"
    params:
#        serovar = os.path.join(config["workdir"],"results/{sample}/sistr/{sample}_serovar.txt"),
        json = os.path.join(config["workdir"],"results/{sample}/sistr/{sample}_allele-results.json"),
        novel_alleles = os.path.join(config["workdir"],"results/{sample}/sistr/{sample}_novel-alleles.fasta"),
        cgmlst_profile = os.path.join(config["workdir"],"results/{sample}/sistr/{sample}_cgmlst-profiles.csv"),
        tsv = os.path.join(config["workdir"],"results/{sample}/sistr/{sample}_sistr-output.tab")
    shell:
        """
        sistr --version > {log}
        echo "sistr --qc -vv --alleles-output {params.json} --novel-alleles {params.novel_alleles} --cgmlst-profiles {params.cgmlst_profile} -f tab --output-prediction {params.tsv} --run-mash {input}" >> {log}
        sistr --qc -vv --alleles-output {params.json} --novel-alleles {params.novel_alleles} --cgmlst-profiles {params.cgmlst_profile} -f tab --output-prediction {params.tsv} --run-mash {input} 2>&1 | tee -a {log}
        """

rule fix_sistr_output:
    input:
        tsv = "results/{sample}/sistr/{sample}_sistr-output.tab"
    output:
        "results/{sample}/sistr/{sample}_sistr-output.tsv"
    message: "Running rule fix_sistr_output on {wildcards.sample}"
    shell:
        """
        paste <(echo "ID") <(head -n 1 {input} | cut -f 1-7,9-) > {output} # header
        paste <(echo {wildcards.sample}) <(tail -n 1 {input} | sed 's/^\t/NA\t/'  | cut -f 1-7,9-) >> {output} # body
        """

rule sistr_summary:
    input:
        raw = expand("results/{sample}/sistr/{sample}_sistr-output.tab", sample=samples.index),
        tsv = expand("results/{sample}/sistr/{sample}_sistr-output.tsv", sample=samples.index)
    output:
        final = "results/summary/summary_sistr.tsv",
    message: "Compute summary of all sistr results"
    #log: "logs/sistr_summary.log"
    run:
        block_copy(input_files=input.tsv,output_file = output.final)
        #with open(output.final, 'wb') as outfile:
            #for i, fname in enumerate(input.tsv):
                #with open(fname, 'rb') as infile:
                    #if i != 0:
                        #infile.readline()  # Throw away header on all but first file
                    #shutil.copyfileobj(infile, outfile)      # Block copy rest of file from input to output without parsing

rule sistr_summary_short:
    input:
        tsv = "results/summary/summary_sistr.tsv",
    output:
        tsv = "results/summary/summary_sistr_short.tsv",
    message: ""
    #log: "logs/sistr_summary_short.log"
    run:
        sistr_data = pd.read_table(input.tsv, index_col=False)
        sistr_data_short = sistr_data[["ID","mash_serovar","serovar","serovar_cgmlst"]]
        sistr_data_short.to_csv(output.tsv, index=False, sep = "\t")


# salmonella subspecies typing
rule salmonella_subspecies_typing:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        db = config["params"]['Salmonella_subspecies']['mashdb']
    output:
        tsv = "results/{sample}/salmonella/{sample}_mashscreen.tsv",
    message: "salmonella_subspecies_typing for {wildcards.sample}"
    #log: "logs/salmonella_subtyping_{sample}.log"
    params:
        db = config["params"]['Salmonella_subspecies']['mashdb'] #"/home/DenekeC/Snakefiles/bakcharak/databases/mash/salmonella.msh"
    shell:
        """
        mash screen -p {threads} -w {params.db} {input.contigs} > {output.tsv}
        """


rule parse_salmonella_subspecies_typing:
    input:
        tsv = "results/{sample}/salmonella/{sample}_mashscreen.tsv",
    output:
        tsv = "results/{sample}/salmonella/{sample}_subspecies.tsv",
    message: "parse salmonella_subspecies_typing for {wildcards.sample}"
    #log: "logs/salmonella_subtyping_{sample}.log"
    shell:
        """
        echo -e "sample\tsubspecies\tscore\tfasta" > {output.tsv}
        count_mash=`wc -l {input.tsv} | cut -f 1 -d ' '`
        if [[ $count_mash -gt 0 ]]; then
            best_fasta=`sort -k 1 -nr {input.tsv} | head -n 1 | cut -f 5`
            best_type=`basename $(dirname $best_fasta)`
            best_score=`sort -k 1 -nr {input.tsv} | head -n 1 | cut -f 2 | awk -F '/' '{{print $1/$2}}'`
            echo -e "{wildcards.sample}\t$best_type\t$best_score\t$best_fasta" >> {output.tsv}
        else
            echo "WARNING: Empty mash result"
            echo -e "{wildcards.sample}\tNA\tNA\tNA" >> {output.tsv}
        fi
        """


rule summary_salmonella_subspecies_typing:
    input:
        #tsv = expand("results/{sample}/salmonella/{sample}_mashscreen.tsv", sample=samples.index),
        tsv = expand("results/{sample}/salmonella/{sample}_subspecies.tsv",sample=samples.index),
    output:
        tsv = "results/summary/summary_salmonella_subspecies.tsv"
    message: "summary_salmonella_subspecies_typing for all species"
    #log: "logs/salmonella_subtyping_{sample}.log"
    params:
        db = "/home/DenekeC/Snakefiles/bakcharak/databases/mash/salmonella.msh"
    run:
        block_copy(input_files=input.tsv,output_file = output.tsv)
        #with open(output.tsv, 'wb') as outfile:
            #for i, fname in enumerate(input.tsv):
                #with open(fname, 'rb') as infile:
                    #if i != 0:
                        #infile.readline()  # Throw away header on all but first file
                    #shutil.copyfileobj(infile, outfile)      # Block copy rest of file from input to output without parsing


# mash screen -p 10  -w /cephfs/abteilung4/NGS/ReferenceDB/OTHER/Kalamari/chromosomes/Salmonella/enterica/salmonella.msh /cephfs/abteilung4/Projects_NGS/Validation_ProxiMeta/aquamis/trimmed/MG-00078-0_R1.fastq.gz > $outdir_mash/${sample}_mashscreen.tsv



#TODO rule join_sistr_allele_profiles
#TODO rule compute_sistr_tree


rule run_abricate_SPI:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        ref = os.path.join(config["params"]["SPI"]["path"],config["params"]["SPI"]["name"],"sequences")
    output:
        report = "results/{sample}/SPI/report.tsv"
    message: "Running rule run_abricate_SPI on {wildcards.sample} with contigs"
    log:
       "logs/abricate_SPI_{sample}.log"
    threads:
       config["smk_params"]["threads"]
    params:
        refdb = config["params"]["SPI"]["path"],
        dbname = config["params"]["SPI"]["name"],
        minid = config["params"]["SPI"]["minid"],
        mincov = config["params"]["SPI"]["mincov"]
    shell: 
       """
       abricate --version > {log}
       cat {params.refdb}/{params.dbname}/version.yaml >> {log}
       echo "date of analysis: `date -I`"  >> {log}
       echo "abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report}" >> {log}
       abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report} 2> >(tee -a {log} >&2)
       """


rule abricate_summary_SPI:
    input:
        abricate_report = expand("results/{sample}/SPI/report.tsv", sample=samples.index)
    output:
        summary_matrix = "results/summary/summary_SPI.tsv",
        summary_short = "results/summary/summary_SPI_short.tsv"
    message: "Running rule abricate_summary_SPI for all samples"
    params:
        duplication_rule = "sum", # TODO sum?
        returnvalue = "coverage"
    script:
        "../scripts/abricate_summary.R"



