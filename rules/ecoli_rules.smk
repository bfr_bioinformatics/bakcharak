# rules for e. coli

# What 

# E. coli O-H groups
# E. coli serotyping: /cephfs/abteilung4/NGS/ReferenceDB/CGEdb/serotypefinder_db
# E. coli virulence factors: ecoli_vf
# Determine VTEC, STEC etc by seach of specific toxins
# FIM typing: https://cge.cbs.dtu.dk/services/FimTyper/
# CHTyper identifies the FimH type and FumC type in total or partial sequenced isolates of E. coli. The FumC alleles are downloaded from PubMLST


# OH groups

rule run_abricate_OH:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        ref = os.path.join(config["params"]["OH"]["path"],config["params"]["OH"]["name"],"sequences")
    output:
        report = "results/{sample}/OH/report.tsv"
    message: "Running rule run_abricate_OH on {wildcards.sample} with contigs"
    log:
       "logs/abricate_OH_{sample}.log"
    threads:
       config["smk_params"]["threads"]
    params:
        refdb = config["params"]["OH"]["path"], #"/cephfs/abteilung4/NGS/ReferenceDB/NCBI/ResDB",
        dbname = config["params"]["OH"]["name"], #"ncbi",
        minid = config["params"]["OH"]["minid"],
        mincov = config["params"]["OH"]["mincov"]
    shell: 
       """
        abricate --version > {log}
        if [[ -f {params.refdb}/{params.dbname}/version.yaml ]]; then
            cat {params.refdb}/{params.dbname}/version.yaml >> {log}
        fi
        echo "date of analysis: `date -I`"  >> {log}
        echo "abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report}" >> {log}
        abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report} 2> >(tee -a {log} >&2)
        """


rule abricate_summary_OH:
    input:
        expand("results/{sample}/OH/report.tsv", sample=samples.index)
    output:
        "results/summary/summary_OH.tsv"
    message: "Running rule abricate_summary_OH for all samples"
    log: "logs/abricate_summary.log"
    params:
        tmpfile = "results/summary/summary_OH.tmp"
    shell:
        """
        abricate --summary {input} > {params.tmpfile}
        paste <(cut -f 1 {params.tmpfile} | awk -F'/' '{{print $2}}' | sed 's/^$/#Sample/') <(cut -f 2- {params.tmpfile}) > {output}
        """

rule abricate_summary_OH_short:
    input:
        "results/summary/summary_OH.tsv"
    output:
        "results/summary/summary_OH_short.tsv"
    message: "Runnign rule abricate_summary_OH for all samples"
    log: "logs/abricate_summary.log"
    params:
    shell:
        """
        echo -e "#Sample\tcount_OHgenes\tOHgenes" > {output}
        paste <(cut -f 1-2 {input} | tail -n +2 ) <(cut -f 3- {input} | awk 'BEGIN{{OFS="\t"}}; {{ if(NR == 1) {{for(i = 1; i <= NF; i++) {{genes[i] = $i }} }}  else {{for(i = 1; i <= NF; i++) {{ if($i != "." ) {{$i=genes[i]; }} }} print $0 }} }}; ' | tr '\t' ';' | sed 's/[.];//g' | sed 's/;[.]//g' | sed 's/[.]/NA/') >> {output}
        """

# ecoli virulence factors (ecoli_vf)

rule run_abricate_ecoli_vf:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        ref = os.path.join(config["params"]["ecoli_vf"]["path"],config["params"]["ecoli_vf"]["name"],"sequences")
    output:
        report = "results/{sample}/ecoli_vf/report.tsv"
    message: "Running rule run_abricate_ecoli_vf on {wildcards.sample} with contigs"
    log:
       "logs/abricate_ecoli_vf_{sample}.log"
    threads:
       config["smk_params"]["threads"]
    params:
        refdb = config["params"]["ecoli_vf"]["path"], #"/cephfs/abteilung4/NGS/ReferenceDB/NCBI/ResDB",
        dbname = config["params"]["ecoli_vf"]["name"], #"ncbi",
        minid = config["params"]["ecoli_vf"]["minid"],
        mincov = config["params"]["ecoli_vf"]["mincov"]
    shell: 
        """
        abricate --version > {log}
        if [[ -f {params.refdb}/{params.dbname}/version.yaml ]]; then
            cat {params.refdb}/{params.dbname}/version.yaml >> {log}
        fi
        echo "date of analysis: `date -I`"  >> {log}
        echo "abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report}" >> {log}
        abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report} 2> >(tee -a {log} >&2)
        """


rule abricate_summary_ecoli_vf:
    input:
        abricate_report = expand("results/{sample}/ecoli_vf/report.tsv", sample=samples.index)
    output:
        summary_matrix = "results/summary/summary_ecoli_vf.tsv",
        summary_short = "results/summary/summary_ecoli_vf_short.tsv"
    message: "Running rule abricate_summary_ecoli_vf for all samples"
    params:
        duplication_rule = "all",
        returnvalue = "coverage"
    script:
        "../scripts/abricate_summary.R"

# DEPRECATED >>>>>>>
#rule abricate_summary_ecoli_vf:
    #input:
        #expand("results/{sample}/ecoli_vf/report.tsv", sample=samples.index)
    #output:
        #"results/summary/summary_ecoli_vf.tsv"
    #message: "Running rule abricate_summary_ecoli_vf for all samples"
    #log: "logs/abricate_summary.log"
    #params:
        #tmpfile = "results/summary/summary_ecoli_vf.tmp"
    #shell:
        #"""
        #abricate --summary {input} > {params.tmpfile}
        #paste <(cut -f 1 {params.tmpfile} | awk -F'/' '{{print $2}}' | sed 's/^$/#Sample/') <(cut -f 2- {params.tmpfile}) > {output}
        #"""

#rule abricate_summary_ecoli_vf_short:
    #input:
        #"results/summary/summary_ecoli_vf.tsv"
    #output:
        #"results/summary/summary_ecoli_vf_short.tsv"
    #message: "Runnign rule abricate_summary_ecoli_vf for all samples"
    #log: "logs/abricate_summary.log"
    #params:
    #shell:
        #"""
        #echo -e "#Sample\tcount_resgenes\tresgenes" > {output}
        #paste <(cut -f 1-2 {input} | tail -n +2 ) <(cut -f 3- {input} | awk 'BEGIN{{OFS="\t"}}; {{ if(NR == 1) {{for(i = 1; i <= NF; i++) {{genes[i] = $i }} }}  else {{for(i = 1; i <= NF; i++) {{ if($i != "." ) {{$i=genes[i]; }} }} print $0 }} }}; ' | tr '\t' ';' | sed 's/[.];//g' | sed 's/;[.]//g' | sed 's/[.]/NA/') >> {output}
        #"""
# <<<<<<<<<<<<<<<<<<<


# stx search

rule run_abricate_stx:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        ref = os.path.join(config["params"]["stx"]["path"],config["params"]["stx"]["name"],"sequences")
    output:
        report = "results/{sample}/stx/report.tsv"
    message: "Running rule run_abricate_stx on {wildcards.sample} with contigs"
    log:
       "logs/abricate_stx_{sample}.log"
    threads:
       config["smk_params"]["threads"]
    params:
        refdb = config["params"]["stx"]["path"],
        dbname = config["params"]["stx"]["name"],
        minid = config["params"]["stx"]["minid"],
        mincov = config["params"]["stx"]["mincov"]
    shell: 
        """
        abricate --version > {log}
        cat {params.refdb}/{params.dbname}/version.yaml >> {log}
        echo "date of analysis: `date -I`"  >> {log}
        echo "abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report}" >> {log}
        abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report} 2> >(tee -a {log} >&2)
        """

# TODO by abricate summary function

rule abricate_summary_stx:
    input:
        abricate_report = expand("results/{sample}/stx/report.tsv", sample=samples.index)
    output:
        summary_matrix = "results/summary/summary_stx.tsv",
        summary_short = "results/summary/summary_stx_short.tsv"
    message: "Running rule abricate_summary_stx for all samples"
    params:
        duplication_rule = "all",
        returnvalue = "coverage"
    script:
        "../scripts/abricate_summary.R"


# DEPRECATED >>>>>>>>>>>>>>>>>>>>>>>>
#rule abricate_summary_stx:
    #input:
        #expand("results/{sample}/stx/report.tsv", sample=samples.index)
    #output:
        #"results/summary/summary_stx.tsv"
    #message: "Running rule abricate_summary_stx for all samples"
    ##log: "logs/abricate_summary.log"
    #params:
        #tmpfile = "results/summary/summary_stx.tmp"
    #shell:
        #"""
        #abricate --summary {input} > {params.tmpfile}
        #paste <(cut -f 1 {params.tmpfile} | awk -F'/' '{{print $2}}' | sed 's/^$/#Sample/') <(cut -f 2- {params.tmpfile}) > {output}
        #rm {params.tmpfile}
        #"""

#rule abricate_summary_stx_short:
    #input:
        #"results/summary/summary_stx.tsv"
    #output:
        #"results/summary/summary_stx_short.tsv"
    #message: "Runnign rule abricate_summary_OH for all samples"
    #log: "logs/abricate_summary.log"
    #params:
    #shell:
        #"""
        #echo -e "#Sample\tcount_stx\tstx" > {output}
        #paste <(cut -f 1-2 {input} | tail -n +2 ) <(cut -f 3- {input} | awk 'BEGIN{{OFS="\t"}}; {{ if(NR == 1) {{for(i = 1; i <= NF; i++) {{genes[i] = $i }} }}  else {{for(i = 1; i <= NF; i++) {{ if($i != "." ) {{$i=genes[i]; }} }} print $0 }} }}; ' | tr '\t' ';' | sed 's/[.];//g' | sed 's/;[.]//g' | sed 's/[.]/NA/') >> {output}
        #"""

# <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

# additional ecoli report:
# pathotyping

rule ecoli_pathotyping:
    input:
        amrfinder = "results/{sample}/amrfinder/results_plus.tsv",
        stx = "results/{sample}/stx/report.tsv",
    output:
        gene_search = "results/{sample}/amrfinder/pathotyping_genes.tsv",
        pathosummary = "results/{sample}/amrfinder/pathotyping_summary.tsv",
    message: "Running ecoli pathotyping for sample {wildcards.sample}"
    #log: "logs/abricate_summary.log"
    params:
        sample = "{sample}", #{wildcards.sample}
        pathorules = config["params"]["ecoli_pathotyping_rules"],# "/home/DenekeC/Snakefiles/bakcharak/databases/pathotyping/ecoli_typing_rules.tsv" # TODO add to config
    script:
        "../scripts/ecoli_pathotyper.R"



rule summarize_ecoli_pathotyping:
    input:
        pathosummary = expand("results/{sample}/amrfinder/pathotyping_summary.tsv",sample=samples.index)
    output:
        summary_tsv = "results/summary/summary_ecoli_pathotyping.tsv"
    message: "Aggregate ecoli pathotyping"
    #log: "logs/abricate_summary.log"
    run:
        block_copy(input_files=input.pathosummary,output_file = output.summary_tsv)
       #with open(output.summary_tsv, 'wb') as outfile:
            #for i, fname in enumerate(input.pathosummary):
                #with open(fname, 'rb') as infile:
                    #if i != 0:
                        #infile.readline()  # Throw away header on all but first file
                    #shutil.copyfileobj(infile, outfile)      # Block copy rest of file from input to output without parsing



#TODO ezclermont (bakcharak_dev)
rule compute_ezclermont:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
    output:
        summary = "results/{sample}/ezclermont/summary.tsv",
        report = "results/{sample}/ezclermont/report.txt"
    message: "Computing ezclermont for sample {wildcards.sample}"
    params:
        outdir = "results/{sample}/ezclermont"
    shell:
        """
        mkdir -p {params.outdir}
        echo "ezclermont {input} > {output.summary}"
        ezclermont {input} > {output.summary} 2> {output.report} || true # NOTE ezclermont always returns the wrong exit code (1)
        """

rule aggregate_ezclermont:
    input:
        summary = expand("results/{sample}/ezclermont/summary.tsv",sample=samples.index)
    output:
        summary = "results/summary/summary_ecoli_clermonttyping.tsv"
    message: "Aggregating ezclermont for all samples"
    shell:
        """
        echo  -e "sample\tclermont_type" > {output}
        cat {input} >> {output}
        """


