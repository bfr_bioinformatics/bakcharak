# PlasmidFinder -------------------------------------------------


# call plasmidfinder

# using ariba

# needs database



rule ariba_run:
  input:
    read1 = lambda wildcards: _get_fastq(wildcards, 'fq1'),
    read2 = lambda wildcards: _get_fastq(wildcards, 'fq2'),
    ref= "ref/{resdb}"
  output:
    dir = "results/{resdb}/{sample}/",
    report = "results/{resdb}/{sample}/report.tsv",
  message: "Runnning ariba for sample {wildcards.sample} on reference DB {wildcards.resdb}"
  log:     "logs/{sample}_{resdb}_ariba.log"
  threads: config["smk_params"]["threads"]
  params:
    assembler = config["params"]["assembler"] #"spades",
  shell:
    "ariba run --assembler {params.assembler} --threads {threads} {input.ref} {input.read1} {input.read2} {output.dir} --force  2>&1 | tee {log}"

#TODO more parameters
 #--assembly_cov INT
 #--force


rule ariba_summary:
	input:
		report = expand("results/{{resdb}}/{sample}/report.tsv", sample = samples.index) # double braces to mask expansion of resdb
	output:
		summary = "results/{resdb}/summary.csv",
#		prefix = "results/{resdb}/summary"
	message: "Summarizing ariba results for reference DB {wildcards.resdb}"
	log: "logs/{resdb}_ariba_summary.log"
	params:
		prefix = "results/{resdb}/summary"
	shell:
		"ariba summary --preset cluster_all --verbose --col_filter y --row_filter n {params.prefix} {input} 2>&1 | tee {log}"

# TODO change --col_filter argument
# TODO more parameters
#--min_id 50 


# TODO more rules


rule ariba_expand_flag:
	input:
#		report = expand("results/{{resdb}}/{sample}/report.tsv", sample = samples.index) # double braces to mask expansion of resdb
		report = "results/{resdb}/{sample}/report.tsv"
	output:
		"results/{resdb}/{sample}/report_expandedflag.tsv"
	message: "Expanding flag of ariba report for sample {wildcards.sample} on reference DB {wildcards.resdb}"
	shell:
		"ariba expandflag {input} {output}"

