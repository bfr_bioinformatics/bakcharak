# Gene mapping  -------------------------------------------------
# One rule to map to resfinder, plasmidfinder, mlst, vfdb and custom db
# using ariba



rule ariba_run:
  input:
    read1 = lambda wildcards: _get_fastq(wildcards, 'fq1'),
    read2 = lambda wildcards: _get_fastq(wildcards, 'fq2'),
#    ref= {aribaDB[resdb]} #
#    ref = ref/{resdb}
#    ref = lambda wildcards: get_db(wildcards, {{resdb}})
    ref = lambda wildcards: get_db(wildcards.resdb)
#    ref = "/cephfs/abteilung4/NGS/ReferenceDB/AribaDB/{resdb}/"
#    ref = "/home/carlus/BfR/Projects/ResDB_comparison/ref/{resdb}/"
#    ref = lambda wildcards: config["aribaDB"][wildcards.resdb]
  output:
    dir = "results/{sample}/{resdb}/",
    report = "results/{sample}/{resdb}/report.tsv",
#TODO add mlst report results/{sample}/mlst/mlst_report.tsv? 
  message: "Runnning ariba for sample {wildcards.sample} on reference DB {wildcards.resdb}"
  log:     "logs/{sample}_{resdb}_ariba.log"
  threads: config["smk_params"]["threads"]
  params:
    assembler = config["params"]["ariba"]["assembler"], #"spades",
#    db = config["aribaDB"][resdb]
  shell:
    "ariba run --assembler {params.assembler} --threads {threads} {input.ref} {input.read1} {input.read2} {output.dir} --force  2>&1 | tee {log}"

#TODO more parameters
 #--assembly_cov INT
 #--force






rule ariba_summary: # original
    input:
        report = expand("results/{sample}/{{resdb}}/report.tsv", sample = samples.index) # double braces to mask expansion of resdb
    output:
        summary = "results/{resdb}/summary.csv"
#       prefix = "results/{resdb}/summary"
    message: "Summarizing ariba results for reference DB {wildcards.resdb}"
    log: "logs/{resdb}_ariba_summary.log"
    params:
        prefix = "results/{resdb}/summary"
        #--min_id 50 
    shell:
        "ariba summary --row_filter n --col_filter n --cluster_cols assembled,match,ref_seq,pct_id,ctg_cov,known_var,novel_var --verbose --no_tree {params.prefix} {input} 2>&1 | tee {log}"
#       "ariba summary --preset cluster_all --verbose --col_filter y --row_filter n {params.prefix} {input} 2>&1 | tee {log}"


#TODO fix names in ariba summary: remove path, e.g. results/02-01726/mlst/report.tsv -> 02-01726
rule ariba_fix_summary:
    input:
        "results/{resdb}/summary.csv",
    output:
        "results/{resdb}/summary_fixed.csv"
    message: "Fixing summary for ariba summary report for reference DB {wildcards.resdb} "
    log: "logs/{resdb}_ariba_fix_summary.log"
    params:
    shell:
        """
        sed 's/results\///g' {input} | sed 's/\/resfinder\|\/plasmidfinder\|\/vfdb\|mlst//g'  | sed 's/\/report.tsv//g' > {output}
        """





#summary format for mlst should be different; rule order to give precedence for mlst summary rule over general rule

ruleorder: ariba_mlst_summary > ariba_summary

rule ariba_mlst_summary:
    input:
        report = expand("results/{sample}/mlst/mlst_report.tsv", sample = samples.index) # double braces to mask expansion of resdb
    output:
        summary = "results/mlst/summary.csv"
    message: "Summarizing ariba results for reference DB mlst (special)"
    log: "logs/mlst_ariba_summary.log"
    params:
    shell:
        """
#        head -n 1 {input[0]}> {output}
        header=`head -n 1 {input[0]}`
        echo -e "name\t${{header}}" > {output}
#        cat {input} | head -n 1 > {output}
#         for file in {input}; do cat ${{file}} | tail -n +2 >> {output}; done
        for file in {input}; do 
          samplename=`echo ${{file}} | sed 's/results\///g' | sed 's/\/mlst\/mlst_report.tsv//g'`
          reportdata=`tail -n +2 ${{file}}`
          echo -e "${{samplename}}\t${{reportdata}}" >> {output}; 
        done
        """
#TODO? Remove '*'? Convert ND to NA?
    




# TODO change --col_filter argument
# TODO? more parameters in ariba summary rules?


# expand flags in ariba result files
rule ariba_expand_flag:
    input:
#       report = expand("results/{sample}/{{resdb}}/report.tsv", sample = samples.index) # double braces to mask expansion of resdb
#       report = expand("results/{{sample}}/{{resdb}}/report.tsv")
        report = "results/{sample}/{resdb}/report.tsv"
    output:
        "results/{sample}/{resdb}/report_expandedflag.tsv"
#    wildcard_constraints:
#        sample="SRR"
    message: "Expanding flag of ariba report for sample {wildcards.sample} on reference DB {wildcards.resdb}"
    shell:
        "ariba expandflag {input} {output}"




# other rules ----------


rule mlst2serotype:
    input:
        mlst_report = "results/{sample}/mlst/mlst_report.tsv"
    output:
        serotype_report = "results/{sample}/mlst/serotype.txt"
    message: "Converting mlst type to serotype for sample {wildcards.sample}"
    log: "logs/mlst2serotype_{sample}.log"
    params:
        db = config["DB"]["SalmoTyper"],#"/home/DenekeC/software/salmonellatypefinder/database/db.json",
        path2script = config["paths"]["path2script"] #"/home/DenekeC/Snakefiles/bakcharac/scripts/"
    shell:
        "{params.path2script}MLST2Serotype.py -d {params.db} $(cut -f 1 {input} | tail -n +2) > {output}"

rule mlst2serotype_summary:
    input:
        report = expand("results/{sample}/mlst/serotype.txt", sample = samples.index) # double braces to mask expansion of resdb
    output:
        summary = "results/mlst/serotype.tsv"
    message: "Summarizing mlst2serotype results"
    log: "logs/mlst2serotype_summary.log"
    params:
    shell:
        """
        echo -e "#sample\tserotype" > {output}
        for file in {input}; do 
        echo $file
        samplename=`echo ${{file}} | sed 's/results\///g' | sed 's/\/mlst\/serotype.txt//g'`
        serotype=`cat $file`
        echo -e "$samplename\t$serotype" >> {output}
        #cat $file >> {output}
        done
        """

#TODO better way to define path to script
#TODO use snakemake script directive
#TODO in script: parse input, write output, remove argparse


# dummy rule:
#rule ariba_mlst_summary:
    #input:
        
    #output:
        
    #message: ""
    #log: "logs/"
    #params:
    #shell:
        #""


