# rules for cgmlst profiling

# cgMLST -------------------------------------------------

rule run_cgmlst:
    input:
        r1 = lambda wildcards: _get_fastq(wildcards, 'fq1'),
        r2 = lambda wildcards: _get_fastq(wildcards, 'fq2'),
        r1u = lambda wildcards: _get_fastq(wildcards, 'fq1U'),
        r2u = lambda wildcards: _get_fastq(wildcards, 'fq2U')#,
    output:
#        "analysis/{sample}/cgmlst/results.tsv"
        main = "analysis/{sample}/cgmlst/results.tsv",
        novelFasta = "analysis/{sample}/cgmlst/results.tsv.novel.fa",
        novelTxt = "analysis/{sample}/cgmlst/results.tsv.novel.txt",
        special = "analysis/{sample}/cgmlst/results.tsv.special_cases.fa",
        coverage = "analysis/{sample}/cgmlst/results.tsv.coverage.txt",
        ties = "analysis/{sample}/cgmlst/results.tsv.ties.txt"#,
    message: "Calling mentalist2 cgMLST for sample {wildcards.sample}."
    log: "logs/mentalist_cgmlst_{sample}.log"
    conda:
       "envs/mentalist.yml"
    params:
        path = config["paths"]["Mentalist2"],
        db = config["DB"]["Salmonella_mentalist2-Enterobase-cgMLSTv2"],
        MUTATION_THRESHOLD = "6", #config["parameters"]["Mentalist2"]["MUTATION_THRESHOLD"],
        KT = "10" #config["parameters"]["Mentalist2"]["KT"]
    shell:
        "{params.path}MentaLiST.jl call -o {output.main} -s {wildcards.sample} --db {params.db} -t {params.MUTATION_THRESHOLD} --kt {params.KT} --output_votes --output_special {input.r1} {input.r2} {input.r1u} {input.r2u}  2>&1 | tee {log}"


rule collect_cgmlst_matrix:
    input:
        matrix = expand('analysis/{sample}/cgmlst/results.tsv', sample=samples.index),
        novelFasta = expand('analysis/{sample}/cgmlst/results.tsv.novel.fa', sample=samples.index),
        novelTxt = expand('analysis/{sample}/cgmlst/results.tsv.novel.txt', sample=samples.index),
    output:
        matrix = "analysis/cgmlst_matrix.tsv",
        novelFasta = "analysis/cgmlst_novelAlleles.fasta",
        novelTxt = "analysis/cgmlst_novelAlleles.txt"
    message: "Collecting cgmlst_matrix"
    shell:
        """
        cat {input.matrix[0]} | head -n 1 > {output.matrix}
        for i in {input.matrix}; do cat ${{i}} | tail -n +2 >> {output.matrix}; done
        
        cat {input.novelFasta} > {output.novelFasta}
        
        cat {input.novelTxt[0]} | head -n 1 > {output.novelTxt}
        for i in {input.novelTxt}; do cat ${{i}} | tail -n +2 >> {output.novelTxt}; done
        """

rule collect_cgmlst:
    input:
        #main = expand('analysis/{sample}/cgmlst/results.tsv', sample=samples.index),
        #special = expand('analysis/{sample}/cgmlst/results.tsv.special_cases.fa', sample=samples.index),
        coverage = expand('analysis/{sample}/cgmlst/results.tsv.coverage.txt', sample=samples.index),
        #ties = expand('analysis/{sample}/cgmlst/results.tsv.ties.txt', sample=samples.index)#,
    output:
        summary = "analysis/cgmlst_summary.tsv"
    message: "Collect cgMLST summary."
    run:
        with open(output.summary, "w") as out:
            out.write("cgmlst_complete" + '\t' "cgmlst_mean_complete"+ '\t' + "cgmlst_notFound" + '\t' + "cgmlst_belowThresh" + '\t' + "cgmlst_partial" + '\n')
        
        for g in input.coverage:
            print("Parsing file" + g)
            coverage_raw = open(g).read()
            #print(coverage_raw.count("no kmers found"))
            count_notFound = coverage_raw.count("no kmers found")
            count_belowThresh = coverage_raw.count("below threshold")
            count_novel = coverage_raw.count("Novel")
            count_partial = coverage_raw.count("Partially covered")
            count_complete = coverage_raw.count("Called allele")
            mean_complete = count_complete/ (coverage_raw.count("\n")-1)
            #mean_minDepth=
            print(str(count_complete) + '\t' + str(mean_complete) + '\t' + str(count_notFound) + '\t' + str(count_belowThresh) + '\t' + str(count_partial) + '\n')

            with open(output.summary, "a") as out:
                out.write(str(count_complete) + '\t' + str(mean_complete) + '\t' + str(count_notFound) + '\t' + str(count_belowThresh) + '\t' + str(count_partial) + '\n')
        
        
        #for f in input.ties:
            #count_ties = str(len(open(f).readlines(  )))
            #with open(output.summary, "a") as out:
                #out.write(count + '\n')
        #print("Parsing coverage file")
        #print(input.coverage)
        
        #print(open(input.coverage[0], 'r').read().count(input("no kmers found")))

                
    #shell:
        #"""
        ## create summary
        #count_novelAllel="$(grep -c '>' {input.novelFasta})"
        #count_special="$(grep -c '>' {input.special})"
        #count_ties="$(wc -l {input.ties})"
        #count_notFound="(grep -c "no kmers found" {input.coverage})"
        #count_belowThresh="$(grep -c "below threshold" {input.coverage})"
        #count_novel_2="$(grep -c "Novel" {input.coverage})"
        #count_partialCov="$(grep -c "Partially covered" {input.coverage})"
        #count_complete="$(grep -c "Called allele" {input.coverage})"
        #count_fullCov="$(tail -n +2 {input.coverage} | cut -f 2 | awk '{{if($1 == 1) count+=$1}};END{{print count/NR}}')"
        #mean_minDepth="$(tail -n +2 {input.coverage} | cut -f 3 | awk '{{avg+=$1}};END{{print avg/NR}}')"
        ## write output

        ## Matrix
        #cat {input.main[0]} | head -n 1 > {output.matrix}
        #for i in {input.main}; do cat ${{i}} | tail -n +2 >> {output.matrix}; done

        #cat {input.novelFasta} > {output.novelFasta}
        #cat {input.novelTxt} > {output.novelTxt}
        #"""
#        "cat {input} >> {output}"

        #echo -e "{wildcards.sample}\tcount_novelAllel\tcount_special\tcount_ties\tcount_complete\tcount_partial\tcount_belowThresh\tcount_notFound\tcount_fullCov\tmean_minDepth" >> {output.summary}
        #echo -e "{wildcards.sample}\tcount_novelAllel\tcount_special\tcount_ties\tcount_complete\tcount_partial\tcount_belowThresh\tcount_notFound\tcount_fullCov\tmean_minDepth" >> {output.summary}


# what else
#results.tsv.coverage.txt  results.tsv.novel.fa  results.tsv.novel.txt  results.tsv.special_cases.fa

# coverage histogram
#tail -n +2 results.tsv.coverage.txt | cut -f 2

# average coverage
#tail -n +2 results.tsv.coverage.txt | cut -f 2 | awk '{avg+=$1};END{print avg/NR}'

# share of genes that is covered 100%
#tail -n +2 results.tsv.coverage.txt | cut -f 2 | awk '{if($1 == 1) count+=$1};END{print count/NR}'

# histogram of minimal kmer depth
#tail -n +2 results.tsv.coverage.txt | cut -f 3

# average minimal kmer depth
#tail -n +2 results.tsv.coverage.txt | cut -f 3 | awk '{avg+=$1};END{print avg/NR}'

# TODO use cgmlst pipeline
