# rules for typing


# MLST -------------------------------------------------

rule run_mentalist:
    input:
        r1 = lambda wildcards: _get_fastq(wildcards, 'fq1'),
        r2 = lambda wildcards: _get_fastq(wildcards, 'fq2'),
        r1u = lambda wildcards: _get_fastq(wildcards, 'fq1U'),
        r2u = lambda wildcards: _get_fastq(wildcards, 'fq2U')#,
    output:
        "analysis/{sample}/mlst/results.tsv"
    message: "Calling mentalist 7-gene for sample {wildcards.sample}."
    log: "logs/mentalist_{sample}.log"
    conda:
       "envs/mentalist.yml"
    params:
        #path = config["paths"]["SalmoTyper"],
        db = config["DB"]["Salmonella_mentalist-7"],
    shell:
        "MentaLiST.jl call -o {output} -s {wildcards.sample} --db {params.db} {input.r1} {input.r2} {input.r1u} {input.r2u}  2>&1 | tee {log}"


rule collect_mentalist:
    input:
        expand('analysis/{sample}/mlst/results.tsv', sample=samples.index)
    output:
        "analysis/mlst7_summary.tsv"
    shell:
#        "cat {input} >> {output}"
#        "for i in {input}; do cat ${{i}} | tail -n +2 >> {output}; done"
        """
        cat {input[0]} | head -n 1 > {output}
        for i in {input}; do cat ${{i}} | tail -n +2 >> {output}; done
        """


# SalmoTyper -------------------------------------------------

rule run_SalmoTyper:
    input:
        r1 = lambda wildcards: _get_fastq(wildcards, 'fq1'),
        r2 = lambda wildcards: _get_fastq(wildcards, 'fq2')#,
#        r1u = lambda wildcards: _get_fastq(wildcards, 'fq1U'),
#        r2u = lambda wildcards: _get_fastq(wildcards, 'fq2U')#,
    output:
#        dir="analysis/{sample}/MOST/"
#        "$AnalysisDir/results.txt"
        "analysis/{sample}/SalmonellaTypeFinder/results.txt"
    message: "Running rule salmonellatypefinder on {wildcards.sample}"
    conda:
       "envs/salmonellatypefinder.yml" # todo
    log:
       "logs/salmonellaTypeFinder_{sample}.log"
    params:
        path = config["paths"]["SalmoTyper"],
        db = config["DB"]["SalmoTyper"],
        cd = config["paths"]["SalmoTyper_config_db"],
        cp = config["paths"]["SalmoTyper_config_prg"],
        tmpDir = "analysis/{sample}/SalmonellaTypeFinder/tmp"
    shell:
#        "{params.path}SalmonellaTypeFinder.py -t {params.tmpDir} --output {output} -cp {params.cp} -cd {params.cd} -d {params.db} {input.r1} {input.r2} {input.r1u} {input.r2u}"
        "{params.path}SalmonellaTypeFinder.py -t {params.tmpDir} --output {output} -cp {params.cp} -cd {params.cd} -d {params.db} {input.r1} {input.r2}"


rule collect_SalmoTyper:
    input:
        expand('analysis/{sample}/SalmonellaTypeFinder/results.txt', sample=samples.index)
    output:
        "analysis/SalmonellaTypeFinder_summary.tsv"
    shell:
#        "cat {input} >> {output}"
        """
        cat {input} | head -n 1 > {output}
        for i in {input}; do cat ${{i}} | tail -n +2 >> {output}; done
        """

# -------------------------------------------------

rule run_MOST:
    input:
        r1 = lambda wildcards: _get_fastq(wildcards, 'fq1'),
        r2 = lambda wildcards: _get_fastq(wildcards, 'fq2'),
        r1u = lambda wildcards: _get_fastq(wildcards, 'fq1U'),
        r2u = lambda wildcards: _get_fastq(wildcards, 'fq2U')#,
        #r3 = lambda wildcards: _get_fastq(wildcards, 'fq3'),
    output:
        dir="analysis/{sample}/MOST/"
    message: "Running rule MOST on {wildcards.sample}"
    conda:
       "envs/MOST.yml"
    log:
       "logs/most_{sample}.log"
    params:
        path = config["paths"]["MOST"],
        db = config["DB"]["mlst_MOST"]
    shell:
        "{params.path}MOST.py -1 {input.r1} -2 {input.r2} -o {output} -st {params.db} -serotype True"
        

rule summarize_most:
    input:
#       "_L001_R1_001.fastq_MLST_result.csv"
#       expand('analysis/{sample}/MOST/{sample}_MLST_result.csv', sample=samples.index)
        path = expand('analysis/{sample}/MOST/', sample=samples.index),
#       file = expand('analysis/{sample}/MOST/{readfile1}_MLST_result.csv', sample=samples.index, readfile1=readnames)
# drop path
# drop gz
#       09-SA03308_S23_L001_R1_001.fastq_MLST_result.csv
#       14-SA03263_S23_L001.1P.fastq_MLST_result.csv
#       {forward_read}
    output:
        "analysis/MOST_summary.tsv"
    message: "Summarizing MOST reports"
    shell:
#       "echo -e "{sample} \t $(head -n 1 {input} | sed 's/st value:,/ST/g' | sed -e 's/\r$//g') \t $(tail -n +4 {sample} | cut -d, -f 1-2 | tr ',' '-' | tr '\n' ',')" >>  {output} "
#       "cat {input.path}/{readnames}_MLST_result.csv >> {output}"
        "cat {input.path}ComponentComplete.txt >> {output}"

#09-SA03308_S23_L001_R1_001.fastq_MLST_result.csv
# check here: https://stackoverflow.com/questions/42365128/snakemake-using-regex-in-expand
# https://stackoverflow.com/questions/45805217/how-to-gather-files-from-subdirectories-to-run-jobs-in-snakemake



# -------------------------------------------------

rule run_seqsero:
    input:
        r1 = lambda wildcards: _get_fastq(wildcards, 'fq1'),
        r2 = lambda wildcards: _get_fastq(wildcards, 'fq2')
    output:
        #"analysis/{sample}/seqsero.txt"
        "results/{sample}/seqsero/seqsero.txt"
    message: "Running rule seqsero on {wildcards.sample}"
    conda:
       "envs/SeqSero.yml"
    log:
       "logs/seqsero_{sample}.log"
    params:
        path=config["paths"]["seqsero"]
    shell:
         "{params.path}SeqSero.py -m 2  -b sam -i {input.r1} {input.r2} > {output} 2> {log}"


rule report_seqsero:
    input:
        "analysis/{sample}/seqsero.txt"
    output:
        "analysis/{sample}/seqsero_report.tsv"
    message: "Running rule report_seqsero on {wildcards.sample}"
    shell:
        """
        Oantigen="$(grep "O antigen" {input}  | cut -d : -f 2 | tr -d '[:space:]')"
        H1_antigen="$(grep "H1 antigen" {input}  | cut -d : -f 2 | tr -d '[:space:]')"
        H2_antigen="$(grep "H2 antigen" {input}  | cut -d : -f 2 | tr -d '[:space:]')"
        antigen_profile="$(grep "Predicted antigenic profile" {input}  | cut -d : -f 2- | tr -d '[:space:]')"
        serotype="$(grep "Predicted serotype" {input}  | cut -d : -f 2 | tr -d '[:space:]')"
        if [[ $serotype =~ .*doesnotexist* ]]; then serotype="NA"; fi
        result="$(grep "Result:" {input}  | cut -d : -f 2 | tr -d '[:space:]')"
        if [[ $result == "" ]]; then result="NA"; fi
        #
        echo -e "#Sample\tO-antigen\tH1-antigen\tH2-antigen\tantigen_profile\tserotype\tresult" > {output}
        echo -e "{wildcards.sample}\t$Oantigen\t$H1_antigen\t$H2_antigen\t$antigen_profile\t$serotype\t$result" >> {output}
        """


rule summarize_seqsero:
    input:
        expand('analysis/{sample}/seqsero_report.tsv', sample=samples.index)
    output:
        "analysis/seqsero_summary.tsv"
    shell:
#        "cat {input} >> {output}"
        """
        cat {input} | head -n 1 > {output}
        for i in {input}; do cat ${{i}} | tail -n +2 >> {output}; done
        """


# -------------------------------------------------


rule run_salmid:
    input:
        r1 = lambda wildcards: _get_fastq(wildcards, 'fq1'),
        r2 = lambda wildcards: _get_fastq(wildcards, 'fq2'),
        r1u = lambda wildcards: _get_fastq(wildcards, 'fq1U'),
        r2u = lambda wildcards: _get_fastq(wildcards, 'fq2U')#,
        #r3 = lambda wildcards: _get_fastq(wildcards, 'fq3'),
    output:
        dir="analysis/{sample}/salmid.txt"
    message: "Running rule seqsero on {wildcards.sample}"
#    conda:
#       "envs/SeqSero.yml"
    log:
       "logs/salmid_{sample}.log"
    params:
        config["paths"]["salmid"]
    shell:
        "{params}SalmID.py -r taxonomy -i {input.r1} > {output} "
#         "{params}SeqSero.py -m 2  -b sam -i {input.r1} {input.r2} > {output} 2> {log}"


rule summarize_salmid:
    input:
        expand('analysis/{sample}/salmid.txt', sample=samples.index)
    output:
        "analysis/salmid_summary.tsv"
    shell:
#        "cat {input} >> {output}"
        """
        cat {input} | head -n 1 > {output}
        for i in {input}; do cat ${{i}} | tail -n +2 >> {output}; done
        """


