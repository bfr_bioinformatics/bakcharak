# Extr rules not related to a specific module
# contig stats

rule get_contig_stats:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly')
    output:
        "results/{sample}/stats/contig_stats.tsv"
    message: "Computing contig stats for sample {wildcards.sample}"
    log: "logs/contigstats_{sample}.log"
    params:
    shell:
        """
            echo -e 'Contig\tLength\tDepth' > {output}
            if grep -Fq "depth" {input}; then # unicycler style
                echo "unicycler style" > {log}
                grep '>' {input} | tr -d '>' | tr ' ' $'\t' | sed 's/length=//' | sed 's/depth=//' | tr -d 'x' | cut -f 1-3 >> {output}
            elif grep -Fq "cov=" {input}; then # shovill style
                echo "shovill style" > {log}
                grep '>' {input} | tr -d '>' | tr ' ' $'\t' | cut -f 1-3 | sed 's/len=//' | sed 's/cov=//' >> {output}
            elif grep -Fq ">NODE_" {input}; then # spades style
                echo "spades style" > {log}
                paste <(grep --color=never -oE "NODE_[0-9]+" {input}) <(grep --color=never -oE "length_[0-9]+" {input} | sed 's/length_//') <(grep --color=never -oE "cov_[0-9]+" {input} | sed 's/cov_//')  >> {output}
            elif grep -Fq ">tig0" {input}; then # canu style
                echo "canu style" > {log}
                paste <(grep --color=never -oE "tig[0-9]+" {input}) <(grep --color=never -oE "len=[0-9]+" {input} | sed 's/len=//') <(grep --color=never -oE "reads=[0-9]+" {input} | sed 's/reads=//')  >> {output}
            else
                echo "WARNING: style NOT recognized/supported" > {log}
                grep '>' {input} | tr -d '>' | cut -f 1 -d ' ' | awk '{{print $1,"\tNA\tNA"}}' >> {output}
                #echo -e "{wildcards.sample}\tNA\tNA" >> {output}
            fi
        """

# canu:
# >tig00000001 len=5185698 reads=16816 class=contig suggestRepeat=no suggestBubble=no suggestCircular=yes trim=14204-5153563
# flye:
#>contig_1

rule get_file_info:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        stats = "results/{sample}/stats/contig_stats.tsv"
    output:
        "results/{sample}/stats/file_info.tsv"
    message: "Computing file info for sample {wildcards.sample}"
    shell:
        """
        #sample_name={wildcards.sample}
        #date_added=`date -I` # temporary fix needed, activate later
        date_added=`date -r {input.stats} -I`
        checksum=`md5sum {input.contigs} | cut -f 1 -d ' '`
        fasta_path={input.contigs}
        
        echo -e "sample\tdate_added\tfasta_md5\tfasta_path" > {output}
        echo -e "{wildcards.sample}\t$date_added\t$checksum\t$fasta_path" >> {output}
        """

# expand all file info
rule fileinfo_summary:
    input:
        tsv = expand("results/{sample}/stats/file_info.tsv", sample=samples.index)
    output:
        tsv = "results/summary/summary_fileinfo.tsv",
    message: "Compute summary of all file info files"
    run:
        with open(output.tsv, 'wb') as outfile:
            for i, fname in enumerate(input.tsv):
                with open(fname, 'rb') as infile:
                    if i != 0:
                        infile.readline()  # Throw away header on all but first file
                    shutil.copyfileobj(infile, outfile)      # Block copy rest of file from input to output without parsing
        

# ----------
# combine platon and amrfinder results
