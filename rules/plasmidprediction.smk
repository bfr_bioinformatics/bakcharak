# plasmid prediction

rule run_platon:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly')
    output:
        tsv = "results/{sample}/platon/{sample}.tsv",
        fasta_chromosome = "results/{sample}/platon/{sample}.chromosome.fasta",
        fasta_plasmid = "results/{sample}/platon/{sample}.plasmid.fasta"
        #outdir = directory("results/{sample}/platon")
    message: "Running platon for {wildcards.sample}"
    log: "logs/platon_{sample}.log"
    threads: 
        config["smk_params"]["threads"]
    params:
        db = config["params"]["platon"]["db"], #"/cephfs/abteilung4/NGS/ReferenceDB/OTHER/platon/db"
        outdir = "results/{sample}/platon/",
        options = config["params"]["platon"]["options"], #"--verbose"
        mode = config["params"]["platon"]["mode"] # {sensitivity,accuracy,specificity}
    shell:
        """
            echo "platon --threads {threads} {params.options} --mode {params.mode} --prefix {wildcards.sample} --output {params.outdir} --db {params.db} {input.contigs} &> {log}"
            platon --threads {threads} {params.options} --mode {params.mode} --prefix {wildcards.sample} --output {params.outdir} --db {params.db} {input.contigs} &>> {log}
            
            # try catch routine
            echo "Before Trying ..."
            if [[ ! -f {output.tsv} ]]; then # 1. command failed
                echo "Trying ..."
                if grep -q "no potential plasmid contigs" {params.outdir}/{wildcards.sample}.log; then # 2. Error is known
                    echo "WARNING: platon failed"
                    touch {output.fasta_plasmid}
                    cat {input.contigs} > {output.fasta_chromosome}
                    echo -e "ID\tLength\tCoverage\t# ORFs\tRDS\tCircular\tInc Type(s)\t# Replication\t# Mobilization\t# OriT\t# Conjugation\t# AMRs\t# rRNAs\t# Plasmid Hits" > {output.tsv}
                    echo "WARNING: Platon FAILED" >> {log}
                fi
            fi
        """

rule parse_platon:
    input:
        #platon_dir = "results/{sample}/platon"
        fasta_chromosome = "results/{sample}/platon/{sample}.chromosome.fasta",
        fasta_plasmid = "results/{sample}/platon/{sample}.plasmid.fasta"
    output:
        contig_classification = "results/{sample}/platon/contig_classification.csv"
    message: "Parse platon for {wildcards.sample}"
    #log: "logs/platon_parse_{sample}.log"
    shell:
        """
        echo "Contig,Classification" > {output.contig_classification}
        if [[ -s {input.fasta_chromosome} ]]; then
            cat {input.fasta_chromosome} | grep '>' | tr -d '>' | awk '{{print $0",""chromosome"}}' >> {output.contig_classification}
        fi

        if [[ -s {input.fasta_plasmid} ]]; then
            cat {input.fasta_plasmid} | grep '>' | tr -d '>' | awk '{{print $0",""plasmid"}}' >> {output.contig_classification}
        fi
        """
        

# summary: how many and how long cumulative, etc
rule summary_platon:
    input:
        #platondirs = expand("results/{sample}/platon", sample=samples.index)
        report_tsv = expand("results/{sample}/platon/{sample}.tsv", sample=samples.index) # requires prefix in platon
    output:
        summary = "results/summary/summary_platon.tsv"
    message: "Running summary of platon for all samples"
    script:
        "../scripts/platon_summary.R"

