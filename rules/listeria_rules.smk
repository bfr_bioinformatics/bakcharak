# new listeria rules

rule run_lissero:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
    output:
        report = "results/{sample}/lissero/report.tsv"
    message: "Running rule run_lissero on {wildcards.sample} with contigs"
    log:
       "logs/lissero_{sample}.log"
    threads:
       config["smk_params"]["threads"]
    params:
        #serotype_db = config["params"]["lissero"]["serotype_db"],
        extra = "", # e.g. --serotype_db path/2/db
        minid = 95, #config["params"]["lissero"]["minid"],
        mincov = 95 # config["params"]["lissero"]["minid"]
    shell: 
        """
        {{
            lissero {params.extra} --min_cov {params.mincov} --min_id {params.minid} {input.contigs} | cut -f 2- | awk -v sample="{wildcards.sample}" '{{print sample"\t"$0}}' | sed "1s/{wildcards.sample}/ID/"  > {output.report} 2>> {log}
        }} || {{
            echo -e "ID\tSEROTYPE\tPRS\tLMO0737\tLMO1118\tORF2110\tORF2819\tCOMMENT" > {output.report}
            echo -e "{wildcards.sample}\tNON-LMO\tNA\tNA\tNA\tNA\tNA\tlissero_fail" >> {output.report}
            echo "WARNING: lissero FAILED" >> {log}
        }}
        """

# convert to serogroup
rule extend_lissero_results:
    input:
        report = "results/{sample}/lissero/report.tsv"
    output:
        report = "results/{sample}/lissero/report_extended.tsv"
    message: "Extending lissero report for {wildcards.sample}"
    params:
        fix_IVa = True
    script:
        "../scripts/convert_lissero.R"

# aggregate 
rule aggregate_lissero:
    input:
        report = expand("results/{sample}/lissero/report_extended.tsv",sample=samples.index) # new
        #report = expand("results/{sample}/lissero/report.tsv",sample=samples.index) # old
    output:
        summary_tsv = "results/summary/summary_listeria_serotyping.tsv"
    message: "Aggregate lissero"
    run:
        block_copy(input.report,output.summary_tsv)



# TODO fix path information
#       "lissero /cephfs/abteilung4/NGS/Data/MiSeq/190913_M03380_0214_000000000-CD4RM/Analysis/Assembly/assembly/17-LI00393-0.fasta > lissero/results_17-LI00393-0.tsv"
#--serotype_db {params.serotype_db}
 #-s, --serotype_db TEXT  [default: /home/DenekeC/miniconda3/envs/bakcharak_de
                          #v3_2/lib/python3.7/site-packages/lissero/db]
  #--min_id FLOAT          Minimum percent identity to accept a match. [0-100]
                          #[default: 95.0]
  #--min_cov FLOAT         Minimum coverage of the gene to accept a match.
                          #[0-100]  [default: 95.0]
