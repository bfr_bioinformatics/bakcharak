# rules for bakta

rule bakta:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly')
    output:
        summary = "results/{sample}/bakta/{sample}.txt",
        report = "results/{sample}/bakta/{sample}.tsv",
        gff = "results/{sample}/bakta/{sample}.gff3",
        ffn = "results/{sample}/bakta/{sample}.ffn",
        faa = "results/{sample}/bakta/{sample}.faa",
    message: "Running bakta for {wildcards.sample}"
    log: "logs/bakta_{sample}.log"
    threads: 
        config["smk_params"]["threads"]
    params:
        genus = config["params"]["bakta"]["genus"],
        proteins = config["params"]["bakta"]["proteins"],
        #proteins = "", #"--proteins" + config["params"]["bakta"]["proteins"] if config["params"]["bakta"]["extra_proteins"] else "",
        extra = config["params"]["bakta"]["extra"],
        db = config["params"]["bakta"]["db"],
        outdir = "results/{sample}/bakta/"
    shell:
        """
        LOCUS_TAG=`echo {wildcards.sample} | cut -c 1-24`
        echo "bakta --force --db {params.db} --threads {threads} {params.extra} {params.proteins} --genus {params.genus} --keep-contig-headers --locus-tag $LOCUS_TAG --output {params.outdir} --prefix {wildcards.sample} {input}" > {log}
        bakta --version >> {log}
        bakta --force --db {params.db} --threads {threads} {params.extra} {params.proteins} --genus {params.genus} --keep-contig-headers --locus-tag $LOCUS_TAG --output {params.outdir} --prefix {wildcards.sample} {input} | tee -a {log} 2>&1
        """

# NOTE fixed locus_tag limit to 24 characters

#extra = "--complete --compliant" # Force Genbank/ENA/DDJB compliance
#  --proteins PROTEINS   Fasta file of trusted protein sequences for CDS annotation
# --species marmotae
# --gram -
#echo "bakta --threads 10 --gram - --genus Escherichia --species marmotae -o $sample -p $sample $fasta"

# summarize bakta findings
rule summarize_bakta:
    input:
        summary = "results/{sample}/bakta/{sample}.txt",
        report = "results/{sample}/bakta/{sample}.tsv",
        faa = "results/{sample}/bakta/{sample}.faa",
        ffn = "results/{sample}/bakta/{sample}.ffn",
    output:
        summary = "results/{sample}/bakta/{sample}.summary.txt",
    params:
        sample = "{sample}"
    message: "Summarizing bakta for {wildcards.sample}"
    script:
        #"../scripts/bakta_summary.R"
        "../scripts/parse_bakta_summary.py"


rule collect_bakta_summaries:
    input:
        summary = expand("results/{sample}/bakta/{sample}.summary.txt",sample=samples.index),
    output:
        summary = "results/summary/summary_bakta.tsv",
    message: "Collecting all bakta summaries"
    run:
        block_copy(input_files=input.summary,output_file = output.summary)
