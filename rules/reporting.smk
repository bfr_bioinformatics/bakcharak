# additional rules for reporting etc

# -----------------------------------------------------------------------

rule summary_report:
    input:
        summary_all = "results/summary/summary_allsamples.tsv",
        genebrowser_all = "results/summary/genebrowser.tsv",
        genebrowser_slim = "results/summary/genebrowser_slim.tsv",
        # old files just for path information:
        #summary_plasmidfinder = "results/summary/summary_plasmidfinder_short.tsv",
        #summary_resfinder = "results/summary/summary_amrfinder.tsv", # amrfinder
        #summary_mobile_amr = "results/summary/summary_mobile_amrgenes.tsv",
        #summary_vfdb = "results/summary/summary_vfdb_short.tsv",
        #summary_vfdb_categories = "results/summary/summary_vfdb_categories.tsv",
        #summary_mlst = "results/summary/summary_mlst_short.tsv",
        #report_plasmidfinder = "results/summary/summary_plasmidfinder.tsv",
        #report_resfinder = "results/summary/summary_amrfinder_genecoverage.tsv", # amrfinder
        #report_vfdb = "results/summary/summary_vfdb.tsv",
        #report_mlst = "results/summary/summary_mlst.tsv",
        #summary_mash = "results/summary/summary_bestrefererence.tsv",
        # platon
        summary_platon = "results/summary/summary_platon.tsv",
        # if Salmonella
        summary_serotype = "results/summary/summary_sistr_short.tsv" if config["species"] == "Salmonella" else [],
        report_serotype = "results/summary/summary_sistr.tsv" if config["species"] == "Salmonella" else [],
        summary_salmonella_subspecies = "results/summary/summary_salmonella_subspecies.tsv" if config["species"] == "Salmonella" else [],
        summary_SPI_short = "results/summary/summary_SPI_short.tsv" if config["species"] == "Salmonella" else [],
        # if E.coli
        summary_OH = "results/summary/summary_OH_short.tsv" if config["species"] == "Ecoli" else [],
        summary_ecoli_vf = "results/summary/summary_ecoli_vf_short.tsv" if config["species"] == "Ecoli" else [],
        report_OH = "results/summary/summary_OH.tsv" if config["species"] == "Ecoli" else [],
        report_ecoli_vf = "results/summary/summary_ecoli_vf.tsv" if config["species"] == "Ecoli" else [],
        summary_ecoli_pathotyping = "results/summary/summary_ecoli_pathotyping.tsv" if config["species"] == "Ecoli" else [],
        summary_clermont = "results/summary/summary_ecoli_clermonttyping.tsv" if config["species"] == "Ecoli" else [],
        # if Bacillus
        report_btyper_vf = "results/summary/summary_btyper_vf.tsv" if config["species"] == "Bacillus" else [],
        summary_btyper_vf = "results/summary/summary_btyper_vf_short.tsv" if config["species"] == "Bacillus" else [],
        bacillus_summary = "results/summary/bacillus_summary.tsv" if config["species"] == "Bacillus" else [],
        # if Staphylococcus
        #report_saureus_setB = "results/summary/summary_vfdb_saureus_setB.tsv" if config["species"] == "Staphylococcus" else [], # deprecated
        summary_vfdb_extra = "results/summary/summary_vfdb_extra_short.tsv" if config["species"] == "Staphylococcus" else [], # new
        summary_saureus_sccmec = "results/summary/summary_sccmec_short.tsv" if config["species"] == "Staphylococcus" else [],
        summary_saureus_toxin = "results/summary/summary_saureus_toxin_short.tsv" if config["species"] == "Staphylococcus" else [],
        report_saureus_sccmec = "results/summary/summary_sccmec.tsv" if config["species"] == "Staphylococcus" else [],
        report_saureus_toxin = "results/summary/summary_saureus_toxin.tsv" if config["species"] == "Staphylococcus" else [],
        # database and software versions versions
        database_versions = "results/summary/database_versions.tsv",
        software_versions = "results/summary/software_versions.tsv",
    output:
        html = "results/reports/summary.html",
    message: "Creating Rmarkdown summary report - new style"
    log: "logs/summary_report.log"
    #conda: "../envs/rmarkdown_env.yaml"
    params:
        workdir = config["workdir"], # move to params
        species = config["species"],
        samplelist = config["samples"],
        #heatmaps = config["params"]["plots"],
        config = config,
        # former input files
        summary_plasmidfinder = "results/summary/summary_plasmidfinder_short.tsv",
        summary_resfinder = "results/summary/summary_amrfinder.tsv", # amrfinder
        summary_mobile_amr = "results/summary/summary_mobile_amrgenes.tsv",
        summary_platon = "results/summary/summary_platon.tsv",
        summary_vfdb = "results/summary/summary_vfdb_short.tsv",
        summary_vfdb_categories = "results/summary/summary_vfdb_categories.tsv",
        summary_mlst = "results/summary/summary_mlst_short.tsv",
        report_plasmidfinder = "results/summary/summary_plasmidfinder.tsv",
        report_resfinder = "results/summary/summary_amrfinder_genecoverage.tsv", # amrfinder
        report_vfdb = "results/summary/summary_vfdb.tsv",
        report_mlst = "results/summary/summary_mlst.tsv",
        summary_mash = "results/summary/summary_bestrefererence.tsv",
        # if CC
        summary_mlstcc = "results/summary/summary_mlstCC.tsv" if config["params"]["mlst"]["cc"] == True else [],
        # if fastani
        summary_fastani = "results/summary/summary_fastani.tsv" if config["params"]["do_ani"] == True else [],
        # platon
        #stats = "results/summary/summary_all.tsv",
        #do_ani = config["params"]["do_ani"],
        #show_bestreference = config["params"]["show_bestreference"],
        #do_cc = config["params"]["mlst"]["cc"],
        #runname_position = config["params"]["runname_position"],
    script:
        "../scripts/summary_report_new.Rmd"





# ---

rule summary_report_legacy:
    input:
        #workdir = config["workdir"],
        summary_plasmidfinder = "results/summary/summary_plasmidfinder_short.tsv",
        summary_resfinder = "results/summary/summary_amrfinder.tsv", # amrfinder
        summary_mobile_amr = "results/summary/summary_mobile_amrgenes.tsv",
        summary_vfdb = "results/summary/summary_vfdb_short.tsv",
        summary_mlst = "results/summary/summary_mlst_short.tsv",
        report_plasmidfinder = "results/summary/summary_plasmidfinder.tsv",
        report_resfinder = "results/summary/summary_amrfinder_genecoverage.tsv", # amrfinder
        report_vfdb = "results/summary/summary_vfdb.tsv",
        report_mlst = "results/summary/summary_mlst.tsv",
        summary_mash = "results/summary/summary_bestrefererence.tsv",
        # platon
        summary_platon = "results/summary/summary_platon.tsv",
        # if Salmonella
        summary_serotype = "results/summary/summary_sistr_short.tsv" if config["species"] == "Salmonella" else [],
        report_serotype = "results/summary/summary_sistr.tsv" if config["species"] == "Salmonella" else [],
        # if E.coli
        summary_OH = "results/summary/summary_OH_short.tsv" if config["species"] == "Ecoli" else [],
        summary_ecoli_vf = "results/summary/summary_ecoli_vf_short.tsv" if config["species"] == "Ecoli" else [],
        report_OH = "results/summary/summary_OH.tsv" if config["species"] == "Ecoli" else [],
        report_ecoli_vf = "results/summary/summary_ecoli_vf.tsv" if config["species"] == "Ecoli" else [],
        summary_ecoli_pathotyping = "results/summary/summary_ecoli_pathotyping.tsv" if config["species"] == "Ecoli" else [],
        summary_clermont = "results/summary/summary_ecoli_clermonttyping.tsv" if config["species"] == "Ecoli" else [],
        # if Bacillus
        report_btyper_vf = "results/summary/summary_btyper_vf.tsv" if config["species"] == "Bacillus" else [],
        summary_btyper_vf = "results/summary/summary_btyper_vf_short.tsv" if config["species"] == "Bacillus" else [],
        bacillus_summary = "results/summary/bacillus_summary.tsv" if config["species"] == "Bacillus" else [],
        # if Staphylococcus
        #report_saureus_setB = "results/summary/summary_vfdb_saureus_setB.tsv" if config["species"] == "Staphylococcus" else [], # deprecated
        report_saureus_sccmec = "results/summary/summary_sccmec.tsv" if config["species"] == "Staphylococcus" else [],
        report_saureus_toxin = "results/summary/summary_saureus_toxin.tsv" if config["species"] == "Staphylococcus" else [],
        # if CC
        summary_mlstcc = "results/summary/summary_mlstCC.tsv" if config["params"]["mlst"]["cc"] == True else [],
        # if fastani
        summary_fastani = "results/summary/summary_fastani.tsv" if config["params"]["do_ani"] == True else [],
        # database and software versions versions
        #database_versions = "logs/database_versions.tsv",
        #software_versions = "logs/software_versions.tsv"
        database_versions = "results/summary/database_versions.tsv",
        software_versions = "results/summary/software_versions.tsv",
    output:
        html = "results/reports/summary_legacy.html",
    message: "Creating Rmarkdown summary report - legacy version"
    log: "logs/summary_report_legacy.log"
    #conda: "../envs/rmarkdown_env.yaml"
    params:
        workdir = config["workdir"],
        species = config["species"],
        samplelist = config["samples"],
        #heatmaps = config["params"]["plots"],
        stats = "results/summary/summary_all.tsv",
        do_ani = config["params"]["do_ani"],
        show_bestreference = config["params"]["show_bestreference"],
        do_cc = config["params"]["mlst"]["cc"],
        runname_position = config["params"]["runname_position"],
    script:
        "../scripts/summary_report.Rmd"



# per sample
rule collect_genefinder:
    input:
        report_plasmidfinder = "results/{sample}/plasmidfinder/report.tsv",
        report_resfinder = "results/{sample}/amrfinder/results_decorated.tsv", # amrfinder decorated with plasmid info
        report_vfdb = "results/{sample}/vfdb/report.tsv",
        report_mlst = "results/{sample}/mlst/report.tsv",
        report_mlstcc = "results/{sample}/mlst/report_clonalcomplex.tsv" if config["params"]["mlst"]["cc"] == True else [],         # only when available switched on
        report_contigstats = "results/{sample}/stats/contig_stats.tsv",
        report_mashplasmid = "results/{sample}/reffinder/mashscreen_plasmid_top.tsv",
        report_mashscreengenome = "results/{sample}/reffinder/mashscreen_genome_best.tsv",
        report_mashgenome = "results/{sample}/reffinder/mash_genome_best.tsv",
        report_plasmidblaster = "results/{sample}/plasmidBlaster/results_stats_info.tsv",
        # platon
        platon_tsv = "results/{sample}/platon/{sample}.tsv", # version with prefix
        platon_contig_classification = "results/{sample}/platon/contig_classification.csv",
        # if prokka
        #report_prokka = "results/{sample}/prokka/{sample}.features.tsv" if config["params"]["do_prokka"] == True else [],
        # if Salmonella
        report_serotype = "results/{sample}/sistr/{sample}_sistr-output.tab" if config["species"] == "Salmonella" else [],
        # if E.coli
        report_OH = "results/{sample}/OH/report.tsv" if config["species"] == "Ecoli" else [],
        report_ecoli_vf = "results/{sample}/ecoli_vf/report.tsv" if config["species"] == "Ecoli" else [],
        results_ecoli_pathotyping = "results/{sample}/amrfinder/pathotyping_genes.tsv" if config["species"] == "Ecoli" else [],
        summary_clermont = "results/{sample}/ezclermont/summary.tsv" if config["species"] == "Ecoli" else [],
        # if Bacillus
        bacillus_sample_summary = "results/{sample}/summary/bacillus_summary.tsv" if config["species"] == "Bacillus" else [],        
        #if fastani
        report_fastani = "results/{sample}/reffinder/fastani.summary.tsv" if config["params"]["do_ani"] == True else [],
    output:
        genebrowser = "results/{sample}/report/genebrowser.tsv",
        genebrowser_slim = "results/{sample}/report/genebrowser_slim.tsv"
    message: "Collecting and decorating genefinder results for sample {wildcards.sample}"
    params:
        contig_edge_distance = 1000,
        database_species = config["species"],
        do_prokka = False, #config["params"]["do_prokka"],
        has_mlstcc = config["params"]["mlst"]["cc"],
        do_ani = config["params"]["do_ani"]
    script:
        "../scripts/collect_gene_finder.R"
    
rule aggregate_genefinder:
    input: sample_genes = expand("results/{sample}/report/genebrowser.tsv",sample = samples.index)
    output: genebrowser = "results/summary/genebrowser.tsv"
    message: "aggregating decoreated genefinder across all samples"
    run:
        with open(output.genebrowser, 'wb') as outfile:
            for i, fname in enumerate(input.sample_genes):
                with open(fname, 'rb') as infile:
                    if i != 0:
                        infile.readline()  # Throw away header on all but first file
                    shutil.copyfileobj(infile, outfile)      # Block copy rest of file from input to output without parsing

rule aggregate_genefinder_slim:
    input: sample_genes = expand("results/{sample}/report/genebrowser_slim.tsv",sample = samples.index)
    output: genebrowser = "results/summary/genebrowser_slim.tsv"
    message: "aggregating decoreated genefinder_slim across all samples"
    run:
        with open(output.genebrowser, 'wb') as outfile:
            for i, fname in enumerate(input.sample_genes):
                with open(fname, 'rb') as infile:
                    if i != 0:
                        infile.readline()  # Throw away header on all but first file
                    shutil.copyfileobj(infile, outfile)      # Block copy rest of file from input to output without parsing

        
# TODO        
rule genefinder_html_browser:
    input:
        genebrowser = "results/reports/genebrowser.tsv"
    output:
        html = "results/reports/genebrowser.html"
    message: "Creating genefinder_html_browser"
    params:
        species = config["species"]
    script:
        "../scripts/genebrowser_report.Rmd"
    

# report per sample
rule sample_report:
    input:
        #workdir = config["workdir"],
        report_plasmidfinder = "results/{sample}/plasmidfinder/report.tsv",
        #report_resfinder = "results/{sample}/resfinder/report.tsv", # abricate
        #report_resfinder = "results/{sample}/amrfinder/results_plus.tsv", # amrfinder
        report_resfinder = "results/{sample}/amrfinder/results_decorated.tsv", # amrfinder decorated with plasmid info
        report_vfdb = "results/{sample}/vfdb/report.tsv",
        report_mlst = "results/{sample}/mlst/report.tsv",
        report_mlstcc = "results/{sample}/mlst/report_clonalcomplex.tsv" if config["params"]["mlst"]["cc"] == True else [],         # only when available switched on
        report_contigstats = "results/{sample}/stats/contig_stats.tsv",
        report_mashplasmid = "results/{sample}/reffinder/mashscreen_plasmid_top.tsv",
        report_mashscreengenome = "results/{sample}/reffinder/mashscreen_genome_best.tsv",
        report_mashgenome = "results/{sample}/reffinder/mash_genome_best.tsv",
        report_plasmidblaster = "results/{sample}/plasmidBlaster/results_stats_info.tsv",
        summary_plasmidblaster_nonredundant = "results/{sample}/plasmidBlaster/summary_per_subject_nonredundant.tsv",
        # platon
        #platon_dir = "results/{sample}/platon", #version with dir
        platon_tsv = "results/{sample}/platon/{sample}.tsv", # version with prefix
        platon_contig_classification = "results/{sample}/platon/contig_classification.csv",
        # if prokka
        #report_prokka = "results/{sample}/prokka/{sample}.features.tsv" if config["params"]["do_prokka"] == True else [],
        # bakta
        summary_bakta = "results/{sample}/bakta/{sample}.txt" if config["params"]["do_bakta"] == True else [],
        # if Salmonella
        report_serotype = "results/{sample}/sistr/{sample}_sistr-output.tab" if config["species"] == "Salmonella" else [],
        # if E.coli
        report_OH = "results/{sample}/OH/report.tsv" if config["species"] == "Ecoli" else [],
        report_ecoli_vf = "results/{sample}/ecoli_vf/report.tsv" if config["species"] == "Ecoli" else [],
        results_ecoli_pathotyping = "results/{sample}/amrfinder/pathotyping_genes.tsv" if config["species"] == "Ecoli" else [],
        summary_clermont = "results/{sample}/ezclermont/summary.tsv" if config["species"] == "Ecoli" else [],
        # if Bacillus
        bacillus_sample_summary = "results/{sample}/summary/bacillus_summary.tsv" if config["species"] == "Bacillus" else [],        
        #if fastani
        report_fastani = "results/{sample}/reffinder/fastani.summary.tsv" if config["params"]["do_ani"] == True else [],
        #report_fastani = "results/{sample}/reffinder/fastani.tsv" if config["params"]["do_ani"] == True else [],
        genebrowser = "results/{sample}/report/genebrowser.tsv",
        # json and flat files
        sample_yaml = "results/{sample}/report/{sample}.bakcharak.yaml",
        sample_flat = "results/{sample}/report/{sample}.bakcharak.flat"
    output:
        "results/{sample}/report/report_{sample}.html",
    message: "Creating report for sample {wildcards.sample}"
    log: "logs/report_{sample}.log"
    #conda: "../envs/rmarkdown_env.yaml"
    params:
        workdir = config["workdir"],
        species = config["species"],
        sample = "{sample}",
        do_ani = config["params"]["do_ani"],
        do_bakta = config["params"]["do_bakta"],
        print_prokka = "FALSE" #"TRUE",
    script:
        "../scripts/sample_report.Rmd"


rule sample_json_summary:
    input:
        #workdir = config["workdir"], # TODO needed as input? The R script reads from params. Important for trigger?
        report_contigstats = "results/{sample}/stats/contig_stats.tsv",
        file_info = "results/{sample}/stats/file_info.tsv",
        report_plasmidfinder = "results/{sample}/plasmidfinder/report.tsv",
        report_resfinder = "results/{sample}/amrfinder/results_decorated.tsv", # amrfinder decorated with plasmid info
        report_vfdb = "results/{sample}/vfdb/report.tsv",
        report_mlst = "results/{sample}/mlst/report.tsv",
        report_mlstcc = "results/{sample}/mlst/report_clonalcomplex.tsv" if config["params"]["mlst"]["cc"] == True else [],         # only when available switched on
        report_mashplasmid = "results/{sample}/reffinder/mashscreen_plasmid_top.tsv",
        report_mashscreengenome = "results/{sample}/reffinder/mashscreen_genome_best.tsv",
        report_mashgenome = "results/{sample}/reffinder/mash_genome_best.tsv",
        report_plasmidblaster = "results/{sample}/plasmidBlaster/results_stats_info.tsv",
        # platon
        platon_tsv = "results/{sample}/platon/{sample}.tsv", # version with prefix
        platon_contig_classification = "results/{sample}/platon/contig_classification.csv",
        # if prokka
        #report_prokka = "results/{sample}/prokka/{sample}.features.tsv" if config["params"]["do_prokka"] == True else [],
        # if bakta
        report_bakta = "results/{sample}/bakta/{sample}.tsv" if config["params"]["do_bakta"] == True else [],
        summary_bakta = "results/{sample}/bakta/{sample}.txt" if config["params"]["do_bakta"] == True else [],
        # if Salmonella
        report_serotype = "results/{sample}/sistr/{sample}_sistr-output.tab" if config["species"] == "Salmonella" else [],
        salmonella_subspecies = "results/{sample}/salmonella/{sample}_subspecies.tsv" if config["species"] == "Salmonella" else [],
        report_SPI = "results/{sample}/SPI/report.tsv" if config["species"] == "Salmonella" else [],
        # if E.coli
        report_OH = "results/{sample}/OH/report.tsv" if config["species"] == "Ecoli" else [],
        report_ecoli_vf = "results/{sample}/ecoli_vf/report.tsv" if config["species"] == "Ecoli" else [],
        results_ecoli_pathotyping = "results/{sample}/amrfinder/pathotyping_summary.tsv" if config["species"] == "Ecoli" else [],
        summary_clermont = "results/{sample}/ezclermont/summary.tsv" if config["species"] == "Ecoli" else [],
        # if Bacillus
        bacillus_sample_summary = "results/{sample}/summary/bacillus_summary.tsv" if config["species"] == "Bacillus" else [],        
        # if Staph
        report_saureus_toxin = "results/{sample}/saureus_toxin/report.tsv" if config["species"] == "Staphylococcus" else [],
        report_sccmec = "results/{sample}/sccmec/report.tsv" if config["species"] == "Staphylococcus" else [],
        report_vfdb_extra = "results/{sample}/vfdb_extra/report.tsv" if config["species"] == "Staphylococcus" else [],
        report_staphopia_sccmec = "results/{sample}/sccmec/results.tsv"  if config["species"] == "Staphylococcus" else [],
        score_staphopia_sccmec = "results/{sample}/sccmec/results_hamming.tsv"  if config["species"] == "Staphylococcus" else [],
        classreport_sccmec = "results/{sample}/sccmec/classreport.tsv"  if config["species"] == "Staphylococcus" else [],
        # if Listeria
        report_lissero = "results/{sample}/lissero/report_extended.tsv" if config["species"] == "Listeria" else [],
        #if fastani
        report_fastani = "results/{sample}/reffinder/fastani.summary.tsv" if config["params"]["do_ani"] == True else [],
        #report_fastani = "results/{sample}/reffinder/fastani.tsv" if config["params"]["do_ani"] == True else [],
        genebrowser = "results/{sample}/report/genebrowser.tsv",
        # version information
        database_versions = "results/summary/database_versions.tsv",
        software_versions = "results/summary/software_versions.tsv",
    output:
        yaml = "results/{sample}/report/{sample}.bakcharak.yaml",
        #json = "results/{sample}/report/{sample}.bakcharak.json",
        json = "results/{sample}/report/{sample}.bakcharak.json",
        # FLAT FILE = tsv
        flat = "results/{sample}/report/{sample}.bakcharak.flat",
    message: "Creating json/yaml report for sample {wildcards.sample}"
    #log: "logs/json_report_{sample}.log"
    #conda: "../envs/rmarkdown_env.yaml"
    params:
        module_species = config["species"],
        samplesheet = config["samples"],
        config = config,
        has_mlstcc = config["params"]["mlst"]["cc"],
        sample = "{sample}",
        do_prokka = False, #config["params"]["do_prokka"],
        do_bakta = config["params"]["do_bakta"],
        do_ani = config["params"]["do_ani"],
        print_prokka = "FALSE" #"TRUE",
    script:
        "../scripts/create_sample_json.R"


# TODO 
rule copy_flat_files:
    input:
        flat = "results/{sample}/report/{sample}.bakcharak.flat",
    output:
        flat_copy = "results/summary/sample_summaries/{sample}.bakcharak.flat"
    message: "Copying flat file from sample {wildcards.sample}"
    shell:
        "cp {input} {output}"


rule rbind_flat_files:
    input:
        flat_files = expand("results/{sample}/report/{sample}.bakcharak.flat", sample = samples.index),
    output:
        flat_summary = "results/summary/summary_allsamples.tsv"
    message: "Binding flat files for all samples"
    script:
        "../scripts/merge_flat_files.R"



#TODO get file addition date
    # input:
        # mlst_report = expand("results/{sample}/mlst/report.tsv", sample=samples.index)
    # output:
    # message:

# database versions
rule get_database_versions:
    input:
        #mlst_report = expand("results/{sample}/mlst/report.tsv", sample=samples.index)
        #config = config,
    message: "Getting database versions"
    output:
        #versions = "logs/database_versions.tsv"
        versions = "results/summary/database_versions.tsv"
    params:
        #resfinder_refdb = config["params"]["resfinder"]["path"],
        #resfinder_dbname = config["params"]["resfinder"]["name"],
        config = config,
        vfdb_refdb = config["params"]["vfdb"]["path"],
        vfdb_dbname = config["params"]["vfdb"]["name"],
        plasmidfinder_refdb = config["params"]["plasmidfinder"]["path"],
        plasmidfinder_dbname = config["params"]["plasmidfinder"]["name"],
        platon_path = config["params"]["platon"]["db"],
        amrfinder_path = config["params"]["amrfinder"]["path"],
        #mlst
        # species databases
        # prokka/bakta
        # ani?
    script:
        "../scripts/database_info.R"
# get conda env path
# /home/DenekeC/anaconda3/envs/bakcharak2/db/pubmlst/senterica/senterica.txt

# deprecated software versions from conda yaml
rule software_versions_fromcondayaml:
    output:
        "logs/software_versions.tsv"
    message:
        "Parsing software versions from conda yaml"
    shell:
        """
        echo -e "Task\tSoftware\tVersion" > {output}
        paste <(echo "Gene finding") <(grep abricate= {workflow.basedir}/envs/bakcharak.yaml | sed 's/  - //' | cut -f 1-2 -d = | tr '=' '\t') >> {output}
        paste <(echo "AMR prediction") <(grep amrfinder {workflow.basedir}/envs/bakcharak.yaml | sed 's/  - //' | cut -f 1-2 -d = | tr '=' '\t') >> {output}
        paste <(echo "Plasmid prediction") <(grep platon= {workflow.basedir}/envs/bakcharak.yaml | sed 's/  - //' | cut -f 1-2 -d = | tr '=' '\t') >> {output}
        paste <(echo "Reference search") <(grep mash= {workflow.basedir}/envs/bakcharak.yaml | sed 's/  - //' | cut -f 1-2 -d = | tr '=' '\t') >> {output}
        paste <(echo "mlst typing") <(grep mlst= {workflow.basedir}/envs/bakcharak.yaml | sed 's/  - //' | cut -f 1-2 -d = | tr '=' '\t') >> {output}
        paste <(echo "ANI analysis") <(grep fastani= {workflow.basedir}/envs/bakcharak.yaml | sed 's/  - //' | cut -f 1-2 -d = | tr '=' '\t') >> {output}
        paste <(echo "Salmonella serovar prediction") <(grep sistr= {workflow.basedir}/envs/bakcharak.yaml | sed 's/  - //' | cut -f 1-2 -d = | tr '=' '\t') >> {output}
        """

rule software_versions:
    output:
        versions="results/summary/software_versions.tsv"
        #versions="logs/software_versions.tsv"
    message:
        "Parsing software versions"
    params:
        config=config,
        species=config["species"],
        do_ani=config["params"]["do_ani"],
        do_bakta=config["params"]["do_bakta"],
        do_prokka=False,#config["params"]["do_prokka"],
    shell:
        """
        echo -e "Software\tVersion" > {output.versions}
        abricate --version  >> {output.versions}
        #echo -e "\t$()" >> {output.versions}
        echo -e "amrfinder\t$(amrfinder --version)" >> {output.versions}
        platon --version >> {output.versions}
        echo -e "mash $(mash --version)" >> {output.versions}
        mlst --version >> {output.versions}
        blastn -version | grep -P "^blastn" | tr -d ':+' >> {output.versions}
        # if fastani
        if [[ {params.do_ani} == True ]]; then 
            echo "fastANI\t$(fastANI --version 2>&1)" | sed 's/version //' >> {output.versions} # fix for sloppy version reporting
        #elif [[ {params.do_ani} == False ]]; then
            #echo "No ANI!"
        #else 
            #echo "DUNNO ANI"
        fi
        # if bakta
        if [[ {params.do_bakta} == True ]]; then 
            #prokka --version 2>> {output.versions}
            bakta --version >> {output.versions}
        fi
        # if Salmonella
        if [[ "{params.species}" == "Salmonella" ]]; then 
            sistr --version >> {output.versions}
        elif [[ "{params.species}" == "Ecoli" ]]; then 
        # if Ecoli
            ezclermont --version >> {output.versions}
        # if Listeria
        elif [[ "{params.species}" == "Listeria" ]]; then 
            lissero --version >> {output.versions}
        elif [[ "{params.species}" == "Staphylococcus" ]]; then
            staphopia-sccmec --version >> {output.versions}
        fi
        # if Klebsiella
        #kaptive.py --version >> {output.versions}
        #kleborate --version >> {output.versions}
        # if mobile elements
        #integron_finder --version
        # mge
        # phispy --version >> {output.versions}
        # general programs
        R --version | head -n 1 | sed 's/ version//' | cut -f 1-2 -d ' ' >> {output.versions}
        #snakemake --version >> {output.versions}
        echo "snakemake\t$(snakemake --version) " >> {output.versions}
        sed -E 's/ [v]*/\t/' -i {output.versions}
        # bakcharak version
        bakcharak_version=`cat {workflow.basedir}/VERSION`
        echo "bakcharak\t$bakcharak_version" >> {output.versions}
        """

rule bakcharak_versions:
    #input:
        #version_file = "{workflow.basedir}/VERSION"
    output:
        #version_tsv="results/summary/bakcharak_versions.tsv"
        versionfile="results/summary/bakcharak_version.txt"
        #versions="logs/software_versions.tsv"
    message:
        "Parsing software versions"
    shell:
        """
        #cp {input} {output.versionfile}
        cp {workflow.basedir}/VERSION {output.versionfile}
        """

