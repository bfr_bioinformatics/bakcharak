# rules for 7-gene mlst typing
#TODO define database path

# MLST -----------------------------


rule mlst_calling:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        db = config["params"]["mlst"]["db"] # basepath of db directory of conda env
    output:
        report = "results/{sample}/mlst/report.tsv",
        json  = "results/{sample}/mlst/report.json"
    message: "Running rule mlst_calling on {wildcards.sample}"
    log: "logs/mlst_{sample}.log"
    params:
        tmpreport = "results/{sample}/mlst/report.tsv.tmp",
        extra = config["params"]["mlst"]["extra"],
        db = config["params"]["mlst"]["db"], # basepath of db directory of conda env
        blastdb = config["params"]["mlst"]["blastdb"], # basepath of db directory of conda env
        novel_alleles = "results/{sample}/mlst/{sample}_novel_alleles.fasta"
    shell:
        """
        mlst --version > {log}
        echo "db: {params.db}" >> {log}
        echo "date of analysis: `date -I`"  >> {log}
        echo "mlst --label {wildcards.sample} {params.extra} --novel {params.novel_alleles} --blastdb {params.blastdb} --datadir {input.db} --json {output.json} {input.contigs} > {output.report}" >> {log}
        mlst --label {wildcards.sample} {params.extra} --novel {params.novel_alleles} --blastdb {params.blastdb} --datadir {input.db} --json {output.json} {input.contigs} > {output.report} 2> >(tee -a {log} >&2)
        """

rule mlst_summary:
    input:
        expand("results/{sample}/mlst/report.tsv", sample=samples.index)
    output:
        summary_tsv = "results/summary/summary_mlst.tsv"
    message: "Running rule mlst_summary for all samples"
    log: "logs/mlst_summary.log"
    #params:
    run:
        with open(output.summary_tsv, 'wb') as outfile:
            for i, fname in enumerate(input):
                with open(fname, 'rb') as infile:
                    shutil.copyfileobj(infile, outfile)      # Block copy file from input to output without parsing


rule mlst_summary_short:
    input:
        "results/summary/summary_mlst.tsv"
    output:
        "results/summary/summary_mlst_short.tsv"
    message: "Running rule mlst_summary_short for all samples"
    #log: "logs/mlst_summary_short.log"
    params:
    shell:
        """
        echo -e "#Sample\tMLST_ST" > {output}
        cut -f 1,3 {input} >> {output}
        """


# optional clonal complex
rule get_clonalcomplex:
    input:
        mlst_report = "results/{sample}/mlst/report.tsv",
    output:
        cc_report = "results/{sample}/mlst/report_clonalcomplex.tsv",
    message: "Running rule get_clonalcomplex on {wildcards.sample}"
    params:
        db = config["params"]["mlst"]["db"] # basepath of db directory of conda env
    script:
        "../scripts/get_clonalcomplex.R"


rule cc_summary:
    input:
        expand("results/{sample}/mlst/report_clonalcomplex.tsv", sample=samples.index)
    output:
        summary_tsv = "results/summary/summary_mlstCC.tsv"
    message: "Running rule cc_summary for all samples"
    run:
        with open(output.summary_tsv, 'wb') as outfile:
            for i, fname in enumerate(input):
                with open(fname, 'rb') as infile:
                    shutil.copyfileobj(infile, outfile)      # Block copy file from input to output without parsing

