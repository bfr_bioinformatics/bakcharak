# rules for bacillus

# What 

# * Btyper virulence factors


# btyper virulence factors (btyper_vf)

rule run_abricate_btyper_vf:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        ref = os.path.join(config["params"]["btyper_vf"]["path"],config["params"]["btyper_vf"]["name"],"sequences")
    output:
        report = "results/{sample}/btyper_vf/report.tsv"
    message: "Running rule run_abricate_btyper_vf on {wildcards.sample} with contigs"
    log:
       "logs/abricate_btyper_vf_{sample}.log"
    threads:
       config["smk_params"]["threads"]
    params:
        refdb = config["params"]["btyper_vf"]["path"], #"/cephfs/abteilung4/NGS/ReferenceDB/NCBI/ResDB",
        dbname = config["params"]["btyper_vf"]["name"], #"ncbi",
        minid = config["params"]["btyper_vf"]["minid"],
        mincov = config["params"]["btyper_vf"]["mincov"]
    shell: 
        """
            abricate --version > {log}
            cat {params.refdb}/{params.dbname}/version.yaml >> {log}
            echo "date of analysis: `date -I`"  >> {log}
            echo "abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report}" >> {log}
            abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report} 2>> >(tee -a {log} >&2)
        """



rule abricate_summary_btyper_vf:
    input:
        expand("results/{sample}/btyper_vf/report.tsv", sample=samples.index)
    output:
        "results/summary/summary_btyper_vf.tsv"
    message: "Running rule abricate_summary_btyper_vf for all samples"
    #log: "logs/abricate_summary.log"
    params:
        tmpfile = "results/summary/summary_btyper_vf.tmp"
    shell:
        """
        abricate --summary {input} > {params.tmpfile}
        paste <(cut -f 1 {params.tmpfile} | awk -F'/' '{{print $2}}' | sed 's/^$/#Sample/') <(cut -f 2- {params.tmpfile}) > {output}
        rm {params.tmpfile}
        """

rule abricate_summary_btyper_vf_short:
    input:
        "results/summary/summary_btyper_vf.tsv"
    output:
        "results/summary/summary_btyper_vf_short.tsv"
    message: "Runnign rule abricate_summary_btyper_vf for all samples"
    #log: "logs/abricate_summary.log"
    params:
    shell:
        """
        echo -e "#Sample\tcount_vf\tvf" > {output}
        paste <(cut -f 1-2 {input} | tail -n +2 ) <(cut -f 3- {input} | awk 'BEGIN{{OFS="\t"}}; {{ if(NR == 1) {{for(i = 1; i <= NF; i++) {{genes[i] = $i }} }}  else {{for(i = 1; i <= NF; i++) {{ if($i != "." ) {{$i=genes[i]; }} }} print $0 }} }}; ' | tr '\t' ';' | sed 's/[.];//g' | sed 's/;[.]//g' | sed 's/[.]/NA/') >> {output}
        """

# panC, legacy Version ------------------------------------------
rule run_abricate_panC_legacyclades:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        ref = os.path.join(config["params"]["panC_legacyclades"]["path"],config["params"]["panC_legacyclades"]["name"],"sequences")
    output:
        report = "results/{sample}/panC/report_legacyclades.tsv"
    message: "Running rule run_abricate_panC_legacyclades on {wildcards.sample} with contigs"
    log:
       "logs/abricate_panC_legacyclades_{sample}.log"
    threads:
       config["smk_params"]["threads"]
    params:
        refdb = config["params"]["panC_legacyclades"]["path"],
        dbname = config["params"]["panC_legacyclades"]["name"], # TODO rename to legacy_clades
        minid = config["params"]["panC_legacyclades"]["minid"],
        mincov = config["params"]["panC_legacyclades"]["mincov"]
    shell: 
        """
            abricate --version > {log}
            cat {params.refdb}/{params.dbname}/version.yaml >> {log}
            echo "date of analysis: `date -I`"  >> {log}
            echo "abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report}" >> {log}
            abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report} 2> >(tee -a {log} >&2)
        """



rule abricate_summary_panC_legacyclades:
    input:
        expand("results/{sample}/panC/report_legacyclades.tsv", sample=samples.index)
    output:
        "results/summary/summary_panC_legacyclades.tsv"
    message: "Running rule abricate_summary_panC_legacyclades for all samples"
    params:
        tmpfile = "results/summary/summary_panC.tmp"
    shell:
        """
        abricate --summary {input} > {params.tmpfile}
        paste <(cut -f 1 {params.tmpfile} | awk -F'/' '{{print $2}}' | sed 's/^$/#Sample/') <(cut -f 2- {params.tmpfile}) > {output}
        rm {params.tmpfile}
        """

rule abricate_summary_panC_short_legacyclades:
    input:
        "results/summary/summary_panC_legacyclades.tsv"
    output:
        "results/summary/summary_panC_legacyclades_short.tsv"
    message: "Runnign rule abricate_summary_panC_legacyclades for all samples"
    shell:
        """
        echo -e "#Sample\tcount_hits\tclade" > {output}
        paste <(cut -f 1-2 {input} | tail -n +2 ) <(cut -f 3- {input} | awk 'BEGIN{{OFS="\t"}}; {{ if(NR == 1) {{for(i = 1; i <= NF; i++) {{genes[i] = $i }} }}  else {{for(i = 1; i <= NF; i++) {{ if($i != "." ) {{$i=genes[i]; }} }} print $0 }} }}; ' | tr '\t' ';' | sed 's/[.];//g' | sed 's/;[.]//g' | sed 's/[.]/NA/') >> {output}
        """



# New panC version: Adjusted panC 8 groups >>>>>
# new name panC_group_adjusted
# new name panC_adjusted_groups

rule run_abricate_panC_adjustedgroups:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        ref = os.path.join(config["params"]["panC_adjustedgroups"]["path"],config["params"]["panC_adjustedgroups"]["name"],"sequences")
    output:
        report = "results/{sample}/panC/report_adjustedgroups.tsv"
    message: "Running rule run_abricate_panC_adjustedgroups on {wildcards.sample} with contigs"
    log:
       "logs/abricate_panC_adjustedgroups_{sample}.log"
    threads:
       config["smk_params"]["threads"]
    params:
        refdb = config["params"]["panC_adjustedgroups"]["path"],
        dbname = config["params"]["panC_adjustedgroups"]["name"], # TODO rename to adjustedgroups
        minid = config["params"]["panC_adjustedgroups"]["minid"],
        mincov = config["params"]["panC_adjustedgroups"]["mincov"]
    shell: 
        """
            abricate --version > {log}
            cat {params.refdb}/{params.dbname}/version.yaml >> {log}
            echo "date of analysis: `date -I`"  >> {log}
            echo "abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report}" >> {log}
            abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report} 2> >(tee -a {log} >&2)
        """



rule abricate_summary_panC_adjustedgroups:
    input:
        expand("results/{sample}/panC/report_adjustedgroups.tsv", sample=samples.index)
    output:
        "results/summary/summary_panC_adjustedgroups.tsv"
    message: "Running rule abricate_summary_panC_adjustedgroups for all samples"
    params:
        tmpfile = "results/summary/summary_panC.tmp"
    shell:
        """
        abricate --summary {input} > {params.tmpfile}
        paste <(cut -f 1 {params.tmpfile} | awk -F'/' '{{print $2}}' | sed 's/^$/#Sample/') <(cut -f 2- {params.tmpfile}) > {output}
        rm {params.tmpfile}
        """

rule abricate_summary_panC_short_adjustedgroups:
    input:
        "results/summary/summary_panC_adjustedgroups.tsv"
    output:
        "results/summary/summary_panC_adjustedgroups_short.tsv"
    message: "Runnign rule abricate_summary_panC_adjustedgroups for all samples"
    shell:
        """
        echo -e "#Sample\tcount_hits\tclade" > {output}
        paste <(cut -f 1-2 {input} | tail -n +2 ) <(cut -f 3- {input} | awk 'BEGIN{{OFS="\t"}}; {{ if(NR == 1) {{for(i = 1; i <= NF; i++) {{genes[i] = $i }} }}  else {{for(i = 1; i <= NF; i++) {{ if($i != "." ) {{$i=genes[i]; }} }} print $0 }} }}; ' | tr '\t' ';' | sed 's/[.];//g' | sed 's/;[.]//g' | sed 's/[.]/NA/') >> {output}
        """




# <<<<<


# Additional ANI round >>>>>>>>>>>>>>
# new name: bestANI_proposed_species
rule compute_ani_proposed_species:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly')
    output:
        "results/{sample}/reffinder/fastani_proposed_species.tsv"
    message: "Computing fastani proposed_species for sample {wildcards.sample}"
    log: "logs/fastani_proposed_species_{sample}.log"
    params:
        reference = config["params"]["fastani_proposed_species"]["reference_list"], # TODO add new key to to config in wrapper
    shell:
        """
        echo "fastANI -q {input} --rl {params.reference} -o {output}" > {log}
        fastANI -q {input} --rl {params.reference} -o {output}
        """

rule sample_summary_ani_proposed_species:
    input:
        report = "results/{sample}/reffinder/fastani_proposed_species.tsv"
    output:
        summary = "results/{sample}/reffinder/fastani_proposed_species.summary.tsv"
    message: "fastani proposed_species summary for sample {wildcards.sample}"
    script:
        "../scripts/fastani_sample_summary.R"


rule summary_ani_proposed_species:
    input:
        tsv = expand("results/{sample}/reffinder/fastani_proposed_species.summary.tsv", sample=samples.index)
    output:
        tsv = "results/summary/summary_fastani_proposed_species.tsv",
    message: "Computing fastani proposed_species summary across all samples"
    shell:
        """
        head -n 1 {input[0]} > {output} #header
        for file in {input}; do tail -n +2 $file | head -n 1 >> {output}; done #top hit per sample
        """



# <<<<<<<<<<<<<<<<


# meso vs. psychrophil ----------------

rule run_abricate_mesopsychro:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
        ref = os.path.join(config["params"]["mesopsychro"]["path"],config["params"]["mesopsychro"]["name"],"sequences")
    output:
        report = "results/{sample}/mesopsychro/report.tsv"
    message: "Running rule run_abricate_mesopsychro on {wildcards.sample} with contigs"
    log: "logs/abricate_mesopsychro_{sample}.log"
    threads:
       config["smk_params"]["threads"]
    params:
        refdb = config["params"]["mesopsychro"]["path"], #"/cephfs/abteilung4/NGS/ReferenceDB/NCBI/ResDB",
        dbname = config["params"]["mesopsychro"]["name"], #"ncbi",
        minid = config["params"]["mesopsychro"]["minid"],
        mincov = config["params"]["mesopsychro"]["mincov"] # 99
    shell: 
        """
            abricate --version > {log}
            cat {params.refdb}/{params.dbname}/version.yaml >> {log}
            echo "date of analysis: `date -I`"  >> {log}
            echo "abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report}" >> {log}
            abricate --threads {threads} --nopath --db {params.dbname} --minid {params.minid} --mincov {params.mincov} --datadir {params.refdb} {input.contigs} > {output.report} 2> >(tee -a {log} >&2)
        """



rule samplesummary_mesopsychro:
    input:
        abricate_report = "results/{sample}/mesopsychro/report.tsv"
    output:
        summary = "results/{sample}/mesopsychro/summary.tsv"
    message: "Running rule summary_mesopsychro on sample {wildcards.sample} "
    #log: "logs/summary_mesopsychro_{sample}.log"
    params:
        sample = "{sample}"
    script:
        "../scripts/samplesummary_mesopsychro.R"


rule summary_mesopsychro:
    input:
        expand("results/{sample}/mesopsychro/summary.tsv", sample=samples.index)
    output:
        "results/summary/summary_mesopsychro.tsv"
    message: "Running rule abricate_summary_mesopsychro for all samples"
    #log: "logs/abricate_summary_mesopsychro.log"
    shell:
        """
        echo -e "#Sample\t16S\tcspA" > {output}
        cat {input} >> {output}
        """

# NEW >>>

rule parse_mesopsychro_from_bakta:
    input:
        #abricate_report = "results/{sample}/mesopsychro/report.tsv"
        bakta_ffn = "results/{sample}/bakta/{sample}.ffn",
    output:
        summary_json = "results/{sample}/mesopsychro/summary_frombakta.json"
    message: "Parsing mesopsychro info from bakta for sample {wildcards.sample} "
    #log: "logs/summary_mesopsychro_{sample}.log"
    params:
        sample = "{sample}"
    script:
        "../scripts/extract_mesopsychro_frombakta.py"

# <<


# Bacillus summary


rule summary_sample_bacillus:
    input:
        mesopsychro = "results/{sample}/mesopsychro/summary.tsv", # legacy keep for comparison
        mesopsychro_json = "results/{sample}/mesopsychro/summary_frombakta.json",
        btyper_vf = "results/{sample}/btyper_vf/report.tsv",
        #panC = "results/{sample}/panC/report.tsv", # legacy
        panC_adjustedgroups = "results/{sample}/panC/report_adjustedgroups.tsv",
        panC_legacyclades = "results/{sample}/panC/report_legacyclades.tsv",
        bakta_results ="results/{sample}/bakta/{sample}.tsv", # requires bakta!
        # fastani = "results/{sample}/reffinder/fastani.summary.tsv" if config["params"]["do_ani"] == True else [], # not needed here
        fastani_proposed_species = "results/{sample}/reffinder/fastani_proposed_species.summary.tsv",
        mash_plasmids = "results/{sample}/reffinder/mashscreen_plasmid_top.tsv"
    output:
        summary = "results/{sample}/summary/bacillus_summary.tsv"
    message: "Running rule summary_bacillus on sample {wildcards.sample} "
    #log: "logs/summary_mesopsychro_{sample}.log"
    params:
        sample = "{sample}",
        # vf_classes = "",
        # mesopsychro_signatures = "",
    script:
        "../scripts/bacillus_sample_summary.R"


rule collect_summary_bacillus:
    input:
        summary = expand("results/{sample}/summary/bacillus_summary.tsv", sample=samples.index)
    output:
        summary = "results/summary/bacillus_summary.tsv"
    message: "Running rule summary_bacillus on all samples"
    run:
        block_copy(input_files=input.summary,output_file = output.summary)
    #shell:
        #"""
        #head -n 1 {input[0]} > {output} #header
        #for file in {input}; do tail -n +2 $file | head -n 1 >> {output}; done #top hit per sample
        #"""


# >>>>>
# new rule: run btyper3 directly
# TODO remove

rule run_btyper3:
    input:
        contigs = lambda wildcards: _get_fastq(wildcards, 'assembly'),
    output:
        btyper_summary = "results/{sample}/btyper/btyper3_final_results/{sample}_final_results.txt"
    message: "Running btyper3 for sample {wildcards.sample}"
    log:
       "logs/btyper3_{sample}.log"
    threads:
       config["smk_params"]["threads"]
       # TODO add database locations
    params:
        outdir = "results/{sample}/btyper"
        #refdb = config["params"]["btyper_vf"]["path"], #"/cephfs/abteilung4/NGS/ReferenceDB/NCBI/ResDB",
        #dbname = config["params"]["btyper_vf"]["name"], #"ncbi",
        #minid = config["params"]["btyper_vf"]["minid"],
        #mincov = config["params"]["btyper_vf"]["mincov"]
    shell:
        """
        btyper3 --version > {log}
        mkdir -p {params.outdir}
        cp {input.contigs} {params.outdir}/{wildcards.sample}.fasta
        echo "btyper3 -i {params.outdir}/{wildcards.sample}.fasta -o {params.outdir}" | tee -a {log}
        btyper3 -i {params.outdir}/{wildcards.sample}.fasta -o {params.outdir} &>> {log}
        """

        #rm {params.outdir}/{sample}.fasta


rule collect_btyper3:
    input:
        btyper_summary_sample = expand("results/{sample}/btyper/btyper3_final_results/{sample}_final_results.txt", sample= samples.index),
    output:
        btyper_summary_all = "results/summary/summary_btyper.tsv"
    message: "Collect all btyper summary"
    run:
        block_copy(input_files=input.btyper_summary_sample,output_file = output.btyper_summary_all)


# <<<<
