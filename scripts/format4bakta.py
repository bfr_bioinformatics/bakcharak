#!/usr/bin/env python3

from Bio import SeqIO
import argparse
import glob
import os
from Bio.SeqRecord import SeqRecord
parser = argparse.ArgumentParser()
parser.add_argument('--input', '-i', help = '[REQUIRED] the unformatted input file',
                        default = "/", type = os.path.abspath, required = True)
parser.add_argument('--output', '-o', help = '[REQUIRED] the formatted output file',
                        default = "/", type = os.path.abspath, required = True)
parser.add_argument('--dbname', '-n', help = '[REQUIRED] the name of the database',
                        default = "custom_database", type = str, required = True)

                        
args = parser.parse_args()
#paths = glob.glob(os.path.join(args.scheme_path,'*.fasta'))                   
#product = "cgmlst-locus"

protein_seqs = []
with open(args.input,'r') as fasta:
    for record in SeqIO.parse(fasta,'fasta'):
        gene = record.id
        id = gene
        #temp = gene.split('_')
        #id = '_'.join([val for val in temp[:len(temp)-1]])
        dbxrefs = f'{args.dbname}:{id}' # this is parsable from bakta output
        product = id
        aa_header = f'{id} {gene}~~~{product}~~~{dbxrefs}'
        #aa_seq = record.seq.translate()
        #AA_seq = aa_seq.ungap('*')
        protein_seqs.append(SeqRecord(record.seq,aa_header,'',''))


SeqIO.write(protein_seqs,args.output,'fasta')


print(f"Done. Output written to {args.output}")


