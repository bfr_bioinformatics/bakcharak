#!/usr/bin/env Rscript

# [GOAL] create sample json/yaml sumamry

require(dplyr, quietly = TRUE, warn.conflicts = FALSE)
require(yaml, quietly = TRUE, warn.conflicts = FALSE)
require(rjson, quietly = TRUE, warn.conflicts = FALSE)

# summarize all data
legacy_date <- F # T # use folder date, F use actual date
write_yaml  <- T

# definitions -------------------

if(!exists("snakemake")) {
  warning("Manual reading in parameters")
  #library("here")
  #here()
  source("parameters_create_sample_json.R")
} else {
  #message("snakemake input:")
  #print(snakemake@input)

  # input
  abricate_plasmidmarker.file <- snakemake@input[["report_plasmidfinder"]]
  amrfinder_decorated.file    <- snakemake@input[["report_resfinder"]]
  abricate_vfdb.file          <- snakemake@input[["report_vfdb"]]
  mlst.file                   <- snakemake@input[["report_mlst"]]
  report_mlstcc.file          <- snakemake@input[["report_mlstcc"]]
  #report_mlstcc = "results/{sample}/mlst/report_clonalcomplex.tsv" if config["params"]["mlst"]["cc"] == True else [],         # only when available switched on
  sample_contig_stats_file    <- snakemake@input[["report_contigstats"]]
  file_info_file              <- snakemake@input[["file_info"]]

  #report_mashscreengenome = "results/{sample}/reffinder/mashscreen_genome_best.tsv",
  reference.file                    <- snakemake@input[["report_mashgenome"]]
  #report_plasmidblaster = "results/{sample}/plasmidBlaster/results_stats_info.tsv",
  # platon
  platon_report_file                <- snakemake@input[["platon_tsv"]]
  platon_contig_classification_file <- snakemake@input[["platon_contig_classification"]]

  # # if Salmonella
  report_SPI.file            <- snakemake@input[["report_SPI"]]
  salmonella_subspecies.file <- snakemake@input[["salmonella_subspecies"]]
  report_serotype.file       <- snakemake@input[["report_serotype"]]

  # # if E.coli
  pathotyping.file <- snakemake@input[["results_ecoli_pathotyping"]]
  OH.file          <- snakemake@input[["report_OH"]]

  #Clermont
  clermont.tsv     <- snakemake@input[["summary_clermont"]]

  # stx region file
  stx.tsv          <- snakemake@input[["stx"]] # in pathotyping summary?

  #clermontreport.tsv <- snakemake@input[[""]]

  # # if Bacillus
  bacillus_sample_summary.file <- snakemake@input[["bacillus_sample_summary"]]

  # Staph
  report_saureus_toxin.file <- snakemake@input[["report_saureus_toxin"]]
  report_sccmec.file        <- snakemake@input[["report_sccmec"]]

  # sccmec type
  classreport_sccmec.file      <- snakemake@input[["classreport_sccmec"]] # CGE version
  report_staphopia_sccmec.file <- snakemake@input[["report_staphopia_sccmec"]]
  score_staphopia_sccmec.file  <- snakemake@input[["score_staphopia_sccmec"]]

  report_vfdb_extra.file <- snakemake@input[["report_vfdb_extra"]]

  # Listeria
  report_lissero.file <- snakemake@input[["report_lissero"]]

  # # if fastani
  report_fastani.file <- snakemake@input[["report_fastani"]]

  # # if prokka
  # report_prokka = "results/{sample}/prokka/{sample}.features.tsv" if config["params"]["do_prokka"] == "True" else [],
  #report_prokka.file <- snakemake@input[["report_prokka"]] 

  # add to rule bakta
  report_bakta.file  <- snakemake@input[["report_bakta"]]
  summary_bakta.file <- snakemake@input[["summary_bakta"]]

  # params
  #contig_edge_distance <- snakemake@params[["contig_edge_distance"]]  
  sample           <- snakemake@params[["sample"]]
  module_species   <- snakemake@params[["module_species"]]
  do_prokka        <- snakemake@params[["do_prokka"]]
  do_bakta         <- snakemake@params[["do_bakta"]]
  has_mlstcc       <- snakemake@params[["has_mlstcc"]]
  # has_mlstcc       <- snakemake@params[["mlst"]][["cc"]]
  do_ani           <- snakemake@params[["do_ani"]]
  samplesheet.file <- snakemake@params[["samplesheet"]]
  config.yaml      <- snakemake@params[["config"]]
  sampledir        <- dirname(dirname(mlst.file))

  # output
  yaml.outfile <- snakemake@output[["yaml"]]
  json.outfile <- snakemake@output[["json"]]
  flat.outfile <- snakemake@output[["flat"]]
}

# check data

# read in data ----

amrfinder_decorated    <- read.delim(amrfinder_decorated.file, stringsAsFactors = FALSE, check.names = FALSE)
abricate_vfdb          <- read.delim(abricate_vfdb.file, stringsAsFactors = FALSE, check.names = FALSE)
abricate_plasmidmarker <- read.delim(abricate_plasmidmarker.file, stringsAsFactors = FALSE, check.names = FALSE)
mlst                   <- read.delim(mlst.file, stringsAsFactors = FALSE, check.names = FALSE, header = FALSE, colClasses = "character")

#message("report_mlstcc.file: ","\'",report_mlstcc.file,"\'"," length ", length(report_mlstcc.file))
#if(file.exists(report_mlstcc.file)){
if(length(report_mlstcc.file) != 0) {
  report_mlstcc <- read.delim(report_mlstcc.file, stringsAsFactors = FALSE, header = FALSE, col.names = c("sample", "mlst_cc"))
  mlst_cc       <- report_mlstcc$mlst_cc[1]
} else {
  mlst_cc <- NA
}

reference_genome_raw           <- read.delim(reference.file, stringsAsFactors = FALSE, check.names = FALSE, header = FALSE)
colnames(reference_genome_raw) <- c("reference_accession", "sample", "distance", "p-value", "shared_hashes", "total_hashes", "reference_info")
reference_genome_raw$sample    <- sub(".fasta", "", tail(strsplit(reference_genome_raw$sample, split = "/")[[1]], 1))

sample_contig_stats <- read.delim(sample_contig_stats_file, sep = "\t", stringsAsFactors = FALSE, comment.char = "")
file_info           <- read.delim(file_info_file, sep = "\t", stringsAsFactors = FALSE, comment.char = "", colClasses = "character")

if(file.exists(platon_report_file)) {
  platon_report        <- read.delim(platon_report_file, stringsAsFactors = FALSE, check.names = FALSE)
  platon_plasmid_fasta <- "NA"
} else {
  platon_colnames               <- c("ID", "Length", "Coverage", "# ORFs", "RDS", "Circular", "Inc Type(s)", "# Replication", "# Mobilization", "# OriT", "# Conjugation", "# AMRs", "# rRNAs", "# Plasmid Hits")
  platon_report_dummy           <- matrix(nrow = 0, ncol = 14)
  colnames(platon_report_dummy) <- platon_colnames
  platon_report                 <- as.data.frame(platon_report_dummy)
  platon_plasmid_fasta          <- "NA"
}

if(file.exists(platon_contig_classification_file)) {
  platon_contig_classification <- read.delim(platon_contig_classification_file, sep = ",")
} else {
  platon_contig_classification <- NA
}

samplesheet <- read.delim(samplesheet.file, stringsAsFactors = FALSE, colClasses = "character")
mysample    <- sample
fasta       <- samplesheet %>%
  filter(sample == mysample) %>%
  select(assembly) %>%
  unlist()

if(exists("snakemake")) {
  config            = config.yaml
  bakcharak_version <- config$version
} else {
  config            <- read_yaml(config.yaml)
  bakcharak_version <- config$version
  #config <- yaml.load(config.tmp)
  #config.raw <- scan(config.yaml, what = character(), sep = "\n")
  #bakcharak_version <- config.raw[1]
}

# extra modules ----

if(do_bakta) {
  if(file.exists(summary_bakta.file)) {
    summary_bakta <- read.delim(summary_bakta.file, stringsAsFactors = FALSE, header = FALSE)$V1
    # remove section headers:
    #summary_bakta <- grep("^Sequence|^Bakta:|^Annotation:",summary_bakta, invert = TRUE)
    # convert to dataframe
    summary_bakta <- data.frame(info = summary_bakta) %>%
      tidyr::separate(info, c("key", "value"), sep = ": ") %>%
      filter(!is.na(value))
  }
  # if(file.exists(report_bakta.file)){
  #   report_bakta <- read.delim(report_bakta.file, stringsAsFactors = FALSE, skip = 2)
  #   bakta_extra <- data.frame(
  #     count_transposon = report_bakta %>% filter(grepl("transposon",Product, ignore.case = TRUE)) %>% nrow(),
  #     count_transposase = report_bakta %>% filter(grepl("transposase",Product, ignore.case = TRUE)) %>% nrow(),
  #     count_IS = report_bakta %>% filter(grepl("IS[0-9] ",Product, ignore.case = FALSE)) %>% nrow(),
  #     count_pseudogenes = report_bakta %>% filter(grepl("pseudo=true",Product, ignore.case = TRUE)) %>% nrow()
  #   )
  # }  
  # 
}

# species spepcific files ----

# ecoli files
if(module_species == "Ecoli") {

  pathotyping <- read.delim(pathotyping.file, stringsAsFactors = FALSE)
  summary_OH  <- read.delim(OH.file, stringsAsFactors = FALSE)

  extract_OH <- function(summary_OH) {
    H_antigen <- summary_OH %>%
      select(GENE) %>%
      filter(grepl("fliC", GENE)) %>%
      tidyr::separate(GENE, c("Gene", "Variant"), sep = "-") %>%
      select(Variant) %>%
      unlist() %>%
      unique() %>%
      paste(., collapse = "/")
    O_antigen <- summary_OH %>%
      select(GENE) %>%
      filter(grepl("wz", GENE)) %>%
      tidyr::separate(GENE, c("Gene", "Variant"), sep = "-") %>%
      select(Variant) %>%
      unlist() %>%
      unique() %>%
      paste(., collapse = "/")
    return(paste(O_antigen, H_antigen, sep = ":"))
  }

  OHgroup       <- extract_OH(summary_OH)
  clermont_type <- read.delim(clermont.tsv, header = FALSE)$V2
  #clermont_details_raw <- tail(scan(clermontreport.tsv, what = "character",sep="\n"),1)
  #clermont_details <- strsplit(gsub("\\n"," ",gsub("[() ,]","",clermont_details_raw),fixed = TRUE),"'", fixed = TRUE)[[1]][4]
} else if(module_species == "Salmonella") {
  report_serotype       <- read.delim(report_serotype.file, stringsAsFactors = FALSE)
  salmonella_subspecies <- read.delim(salmonella_subspecies.file, stringsAsFactors = FALSE)
  report_SPI.file       <- file.path(sampledir, "SPI/report.tsv")
  report_SPI            <- read.delim(report_SPI.file, stringsAsFactors = FALSE)
  report_SPI_selected   <- report_SPI %>%
    select(GENE, X.COVERAGE, X.IDENTITY) %>%
    dplyr::rename(SPI = GENE, coverage = X.COVERAGE, identity = X.IDENTITY)
} else if(module_species == "Bacillus") {
  bacillus_sample_summary <- read.delim(bacillus_sample_summary.file, stringsAsFactors = FALSE)
} else if(module_species == "Staphylococcus") {
  report_saureus_toxin    <- read.delim(report_saureus_toxin.file, stringsAsFactors = FALSE)
  report_sccmec           <- read.delim(report_sccmec.file, stringsAsFactors = FALSE)
  classreport_sccmec      <- read.delim(classreport_sccmec.file, stringsAsFactors = FALSE)
  # sccmec type
  report_staphopia_sccmec <- read.delim(report_staphopia_sccmec.file, stringsAsFactors = FALSE)
  score_staphopia_sccmec  <- read.delim(score_staphopia_sccmec.file, stringsAsFactors = FALSE)
  report_vfdb_extra       <- read.delim(report_vfdb_extra.file, stringsAsFactors = FALSE)
} else if(module_species == "Listeria") {
  report_lissero <- read.delim(report_lissero.file, stringsAsFactors = FALSE)
}

# #if fastani
if(do_ani) {
  report_fastani <- read.delim(report_fastani.file, stringsAsFactors = FALSE)
}

# # if prokka
# if(do_prokka){
#   report_prokka <- read.delim(report_prokka.file, stringsAsFactors = FALSE)
# } 

# functions -----------------------

# gene, coverage compact
compact_genes <- function(data, name_col, value_col) {
  # check columns
  stopifnot(is.data.frame(data))
  stopifnot(name_col %in% colnames(data))
  stopifnot(value_col %in% colnames(data))

  data %>%
    select(all_of(name_col), all_of(value_col)) %>%
    rowwise() %>%
    mutate(combo = paste(!!as.name(name_col), " (", !!as.name(value_col), ")", sep = "")) %>%
    arrange(combo) %>%
    select(combo) %>%
    unlist() %>%
    paste(., collapse = ";")
}

# summarize abricate by class
get_abricate_by_class <- function(abricate_vfdb) {
  if(any(!is.na(abricate_vfdb$RESISTANCE))) {
    abricate_vfdb %>%
      select(RESISTANCE, GENE) %>%
      dplyr::rename(Class = RESISTANCE, Gene = GENE) %>%
      group_by(Class) %>%
      mutate(genes_by_class = paste0(Gene, collapse = ";"), count = length(Gene)) %>%
      select(-Gene) %>%
      unique() %>%
      arrange(Class)
  } else {
    return(NA)
  }
}

# transform mlst

colnames(mlst) <- c("sample", "mlst_scheme", "mlst_st")

# transform reference mash file
bestreference_tidy <- reference_genome_raw %>%
  tidyr::separate(reference_info, c("pre", "post"), sep = "N[CZ]_[0-9A-Z]+[.][0-9]") %>%
  tidyr::separate(post, c("init", "genus", "species"), remove = FALSE, sep = " ", extra = "drop") %>%
  mutate(species = paste(genus, species)) %>%
  mutate(reference_genome = strsplit(post, ",")[[1]][1]) %>%
  mutate(reference_identity = shared_hashes / total_hashes) %>%
  mutate(reference_accession = paste(strsplit(reference_accession, "_")[[1]][1:2], collapse = "_")) %>%
  select(-c(pre, post, init))
#tidyr::separate(reference_accession, c("reference_accession"), sep = "[.][0-9]_", extra = "drop")
#select(sample,genus,species,reference_genome,reference_identity)
#select(sample,genus,species)

# metadata

database_versions.tsv <- file.path(dirname(sampledir), "summary", "database_versions.tsv")
software_versions.tsv <- file.path(dirname(sampledir), "summary", "software_versions.tsv")

if(file.exists(database_versions.tsv)) {
  database_versions_df <- read.delim(database_versions.tsv, stringsAsFactors = FALSE)
} else {
  database_versions_df <- NA
}

if(file.exists(software_versions.tsv)) {
  software_versions_df <- read.delim(software_versions.tsv, stringsAsFactors = FALSE)
} else software_versions_df <- NA

#analysis_date
# analysis_date <- ifelse(legacy_date == TRUE,
#                         as.character(as.Date(file.info(sampledir)$mtime)), # creation of folder
#                         as.character(Sys.Date())) # creation date of json

# create list of all data
#fasta_md5 <- digest::digest(file = fasta,algo = "md5", serialize = FALSE)
# other md5 function
fasta_md5 <- tools::md5sum(fasta)
stopifnot(fasta_md5 == file_info$fasta_md5)

# metadata
meta_information.df = data.frame(
  sample            = sample,
  species_module    = module_species,
  fasta             = fasta,
  fasta_md5         = fasta_md5,
  report            = file.path(sampledir, "report", paste0("report_", sample, ".html")),
  analysis_date     = as.character(Sys.Date()),
  date_added        = file_info$date_added,
  bakcharak_version = bakcharak_version
)

#TODOconfig
if(config$params$runname_position != 0) {
  meta_information.df$run <- strsplit(fasta, "/")[[1]][as.numeric(config$params$runname_position)]
}

# software and database versions
version_information_list <- list(
  database_versions = database_versions_df,
  software_versions = software_versions_df
)

# --------------------

mlst.df <- data.frame(
  mlst_st      = as.integer(mlst$mlst_st),
  mlst_scheme  = mlst$mlst_scheme,
  mlst_profile = paste(mlst[-c(1:3)], collapse = ";")
  # missing alleles
  # mlst_qc # all loci found
) %>%
  mutate(mlst_st = ifelse(is.na(mlst_st), "-", mlst_st))

if(has_mlstcc) {
  mlst.df$mlst.cc = mlst_cc # TODO read out from where?
}

# reference_information = reference_information.df,
reference_information.df <- data.frame(
  reference_species   = bestreference_tidy$species,
  reference_genus     = bestreference_tidy$genus,
  reference_identity  = bestreference_tidy$reference_identity,
  reference_accession = bestreference_tidy$reference_accession,
  reference_genome    = bestreference_tidy$reference_genome
)

# amr = amr.df,

#colnames(amrfinder_decorated)
#amrfinder_decorated %>% group_by(`Element type`) %>% count()
#amrfinder_decorated %>% group_by(`Element subtype`) %>% count()
#amrfinder_decorated %>% group_by(`Element type`) %>% 

# NOTE: Also mark incomplete stress genes?
stress_genes <- amrfinder_decorated %>%
  filter(`Element type` == "STRESS") %>%
  #filter(! `Element type` %in% c("AMR","VIRULENCE")) %>% 
  group_by(`Element subtype`) %>%
  select(`Element subtype`, `Gene symbol`) %>%
  mutate(genes_by_type = paste0(`Gene symbol`, collapse = ";"), count = length(`Gene symbol`)) %>%
  # version without `Gene symbol` >>> 
  select(-`Gene symbol`) %>%
  unique() %>%
  #distinct() #%>%
  arrange(`Element subtype`) %>%
  dplyr::rename(resistance_type = `Element subtype`)
# ---
#  distinct(across(c(`Element subtype`,genes_by_type,count))) %>% 
#  arrange(`Element subtype`) %>% rename(resistance_type = `Element subtype`)
# <<<

stress_genes_list        <- lapply(stress_genes$resistance_type, function(type) {
  sublist <- list(genes = stress_genes %>%
    ungroup() %>%
    filter(resistance_type == type) %>%
    select(genes_by_type) %>%
    unlist(),
                  count = stress_genes %>%
                    ungroup() %>%
                    filter(resistance_type == type) %>%
                    select(count) %>%
                    unlist())
})
names(stress_genes_list) <- stress_genes$resistance_type

# mark incomplete AMR genes >>>
min_id  <- 100
min_cov <- 100

amrfinder_decorated <-
  amrfinder_decorated %>%
    rowwise() %>%
    mutate(gene_symbol_mod = ifelse((`% Identity to reference sequence` < min_id | `% Coverage of reference sequence` < min_cov) & `Element subtype` != "POINT", paste0(`Gene symbol`, "(~)"), `Gene symbol`)) %>%
    ungroup()
#filter(`Element type` == "AMR" & `Element subtype` == "POINT") %>% # TODO remove
#filter(`Element type` == "AMR") %>% # TODO remove
#select(`% Coverage of reference sequence`   ,   `% Identity to reference sequence`,`Gene symbol`,gene_symbol_mod,`Element subtype`)
# <<<

amr_genes <- amrfinder_decorated %>%
  filter(`Element type` == "AMR") %>%
  select(gene_symbol_mod) %>%
  unlist() %>%
  sort() %>%
  paste(., collapse = ";")

amr_genes_acquired <- amrfinder_decorated %>%
  filter(`Element type` == "AMR" & `Element subtype` == "AMR") %>%
  select(gene_symbol_mod) %>%
  unlist() %>%
  sort() %>%
  paste(., collapse = ";")

amr_gene_count <- amrfinder_decorated %>%
  filter(`Element type` == "AMR") %>%
  select(gene_symbol_mod) %>%
  nrow()

amr_gene_count_acquired <- amrfinder_decorated %>%
  filter(`Element type` == "AMR" & `Element subtype` == "AMR") %>%
  select(gene_symbol_mod) %>%
  nrow()
amr_gene_count_points   <- amrfinder_decorated %>%
  filter(`Element type` == "AMR" & `Element subtype` == "POINT") %>%
  select(gene_symbol_mod) %>%
  nrow()

#amrfinder_decorated %>% filter(`Element type` == "AMR")  %>% count(Class)
#amrfinder_decorated %>% filter(`Element type` == "AMR")  %>% select(Subclass)

amrclassinfo <- amrfinder_decorated %>%
  filter(`Element type` == "AMR") %>%
  summarize(count_amr_classes    = length(unique(Class)),
            amr_classes          = paste(sort(unique(Class)), collapse = ";"),
            count_amr_subclasses = length(unique(Subclass)),
            amr_subclasses       = paste(sort(unique(Subclass)), collapse = ";"))

amr_genes_byclass <- amrfinder_decorated %>%
  filter(`Element type` == "AMR") %>%
  select(Class, gene_symbol_mod) %>%
  group_by(Class) %>%
  mutate(genes_by_class = paste0(gene_symbol_mod, collapse = ";"), count = length(gene_symbol_mod)) %>%
  select(-gene_symbol_mod) %>%
  unique() %>%
  #distinct(across(c(Class,genes_by_class, count))) %>% 
  arrange(Class)

amr_genes_byclass_list        <- lapply(amr_genes_byclass$Class, function(type) {
  sublist <- list(genes = amr_genes_byclass %>%
    ungroup() %>%
    filter(Class == type) %>%
    select(genes_by_class) %>%
    unlist(),
                  count = amr_genes_byclass %>%
                    ungroup() %>%
                    filter(Class == type) %>%
                    select(count) %>%
                    unlist())
})
names(amr_genes_byclass_list) <- amr_genes_byclass$Class

amr_genes_bysubclass <- amrfinder_decorated %>%
  filter(`Element type` == "AMR") %>%
  select(Class, Subclass, gene_symbol_mod) %>%
  group_by(Subclass) %>%
  mutate(genes_by_subclass = paste0(gene_symbol_mod, collapse = ";"), count = length(gene_symbol_mod)) %>%
  select(-gene_symbol_mod) %>%
  unique() %>%
  arrange(Subclass)

# phenotypes ----
phenotypes <- unique(unlist(lapply(as.character(amrfinder_decorated$Phenotype), function(x) strsplit(x, ";")[[1]])))
phenotypes <- phenotypes[!phenotypes %in% c(NA, "None")]

# plasmid classification ----

# amr_genes_byplasmidclassifictation <- amrfinder_decorated %>%
#   filter(`Element type` == "AMR") %>% 
#   select(`Plasmid classification`,`Gene symbol`) %>% 
#   group_by(`Plasmid classification`) %>% 
#   mutate(genes_by_subclass = paste0(`Gene symbol`, collapse = ";"), count = length(`Gene symbol`)) %>% 
#   select(-`Gene symbol`) %>% 
#   unique() %>%
#   arrange(`Plasmid classification`)

# -----------------
esbl_subclasses <- c("CEPHALOSPORIN", "CARBAPENEM")

esbl_detected <-
  amrfinder_decorated %>%
    filter(`Element type` == "AMR") %>%
    select(Class, Subclass) %>%
    summarise(esbl_detected = ifelse(any(Subclass %in% esbl_subclasses), T, F)) %>%
    unlist()

# -----------------

amr.list <-
  list(
    count_amr_genes                = amr_gene_count,
    count_amr_genes_acquired       = amr_gene_count_acquired,
    count_amr_genes_pointmutations = amr_gene_count_points,
    amr_genes                      = amr_genes,
    amr_genes_acquired             = amr_genes_acquired,
    count_amr_bymobility           = amrfinder_decorated %>%
      filter(`Element type` == "AMR") %>%
      count(`Plasmid classification`) %>%
      mutate(compact = paste0(`Plasmid classification`, " (", n, ")")) %>%
      select(compact) %>%
      unlist() %>%
      paste(., collapse = ";"),
    amrclassinfo                   = amrclassinfo,
    amr_genes_byclass              = amr_genes_byclass_list,
    count_amr_genes_byclass        = amr_genes_byclass %>%
      select(Class, count) %>%
      mutate(compact = paste0(Class, " (", count, ")")) %>%
      ungroup() %>%
      select(compact) %>%
      unlist() %>%
      paste(., collapse = ";"),
    amr_genes_byclass              = amr_genes_byclass %>%
      select(Class, genes_by_class) %>%
      mutate(genes_by_class = gsub(";", ",", genes_by_class)) %>%
      mutate(compact = paste0(Class, " (", genes_by_class, ")")) %>%
      ungroup() %>%
      select(compact) %>%
      unlist() %>%
      paste(., collapse = ";"),
    count_amr_genes_bysubclass     = amr_genes_bysubclass %>%
      select(Subclass, count) %>%
      mutate(compact = paste0(Subclass, " (", count, ")")) %>%
      ungroup() %>%
      select(compact) %>%
      unlist() %>%
      paste(., collapse = ";"),
    amr_genes_bysubclass           = amr_genes_bysubclass %>%
      select(Subclass, genes_by_subclass) %>%
      mutate(genes_by_class = gsub(";", ",", genes_by_subclass)) %>%
      mutate(compact = paste0(Subclass, " (", genes_by_class, ")")) %>%
      ungroup() %>%
      select(compact) %>%
      unlist() %>%
      paste(., collapse = ";"),
    count_amr_phenotypes           = length(phenotypes), #length(strsplit(phenotypes,";")[[1]]),
    amr_phenotypes                 = paste(phenotypes, collapse = ";"),
    esbl_detected                  = esbl_detected,
    stress_genes                   = stress_genes_list,
    count_stress_genes_bytype      = stress_genes %>%
      select(resistance_type, count) %>%
      mutate(compact = paste0(resistance_type, " (", count, ")")) %>%
      ungroup() %>%
      select(compact) %>%
      unlist() %>%
      paste(., collapse = ";"),
    stress_genes_bytype            = stress_genes %>%
      select(resistance_type, genes_by_type) %>%
      mutate(genes_by_type = gsub(";", ",", genes_by_type)) %>%
      mutate(compact = paste0(resistance_type, " (", genes_by_type, ")")) %>%
      ungroup() %>%
      select(compact) %>%
      unlist() %>%
      paste(., collapse = ";")
  )

amr.df <- data.frame(
  count_amr_genes                = amr_gene_count,
  count_amr_genes_acquired       = amr_gene_count_acquired,
  count_amr_genes_pointmutations = amr_gene_count_points,
  amr_genes                      = amr_genes,
  amr_genes_acquired             = amr_genes_acquired,
  count_amr_bymobility           = amrfinder_decorated %>%
    filter(`Element type` == "AMR") %>%
    count(`Plasmid classification`) %>%
    mutate(compact = paste0(`Plasmid classification`, " (", n, ")")) %>%
    select(compact) %>%
    unlist() %>%
    paste(., collapse = ";"),
  count_amr_classes              = amrclassinfo$count_amr_classes,
  count_amr_genes_byclass        = amr_genes_byclass %>%
    select(Class, count) %>%
    mutate(compact = paste0(Class, " (", count, ")")) %>%
    ungroup() %>%
    select(compact) %>%
    unlist() %>%
    paste(., collapse = ";"),
  amr_genes_byclass              = amr_genes_byclass %>%
    select(Class, genes_by_class) %>%
    mutate(genes_by_class = gsub(";", ",", genes_by_class)) %>%
    mutate(compact = paste0(Class, " (", genes_by_class, ")")) %>%
    ungroup() %>%
    select(compact) %>%
    unlist() %>%
    paste(., collapse = ";"),
  count_amr_subclasses           = amrclassinfo$count_amr_subclasses,
  count_amr_genes_bysubclass     = amr_genes_bysubclass %>%
    select(Subclass, count) %>%
    mutate(compact = paste0(Subclass, " (", count, ")")) %>%
    ungroup() %>%
    select(compact) %>%
    unlist() %>%
    paste(., collapse = ";"),
  amr_genes_bysubclass           = amr_genes_bysubclass %>%
    select(Subclass, genes_by_subclass) %>%
    mutate(genes_by_class = gsub(";", ",", genes_by_subclass)) %>%
    mutate(compact = paste0(Subclass, " (", genes_by_class, ")")) %>%
    ungroup() %>%
    select(compact) %>%
    unlist() %>%
    paste(., collapse = ";"),
  count_amr_phenotypes           = length(phenotypes),
  amr_phenotypes                 = paste(phenotypes, collapse = ";"),
  esbl_detected                  = esbl_detected,
  count_stress_genes             = sum(stress_genes$count),
  count_stress_genes_bytype      = stress_genes %>%
    select(resistance_type, count) %>%
    mutate(compact = paste0(resistance_type, " (", count, ")")) %>%
    ungroup() %>%
    select(compact) %>%
    unlist() %>%
    paste(., collapse = ";"),
  stress_genes_bytype            = stress_genes %>%
    select(resistance_type, genes_by_type) %>%
    mutate(genes_by_type = gsub(";", ",", genes_by_type)) %>%
    mutate(compact = paste0(resistance_type, " (", genes_by_type, ")")) %>%
    ungroup() %>%
    select(compact) %>%
    unlist() %>%
    paste(., collapse = ";")
)

# Virulence -----------------

# add more info of classes  
if(any(!is.na(abricate_vfdb$RESISTANCE))) {
  abricate_vfdb_byclass_compact <- abricate_vfdb %>%
    count(RESISTANCE) %>%
    arrange(RESISTANCE) %>%
    mutate(compact = paste0(RESISTANCE, " (", n, ")")) %>%
    select(compact) %>%
    unlist() %>%
    paste(., collapse = ";")
} else   abricate_vfdb_byclass_compact <- NA

abricate_vfdb_byclass <- get_abricate_by_class(abricate_vfdb)

virulence.df <- data.frame(
  count_virulencefactors         = length(abricate_vfdb$GENE),
  virulencefactors               = paste(sort(abricate_vfdb$GENE), collapse = ";"),
  count_virulencefactors_byclass = abricate_vfdb_byclass_compact
)

# plasmids --------------------
# plasmids = plasmids.df,
plasmids.df <- data.frame(
  count_INC                 = length(abricate_plasmidmarker$GENE),
  INC_types                 = paste(abricate_plasmidmarker$GENE, collapse = ";"),
  cumulative_plasmid_length = sum(platon_report$Length),
  plasmid_contigs           = nrow(platon_report),
  circular_contigs          = sum(platon_report$Circular == "yes"),
  replication_elements      = sum(platon_report$`# Replication`),
  mobilization_elements     = sum(platon_report$`# Mobilization`),
  conjugation_elements      = sum(platon_report$`# Conjugation`),
  count_OriT                = sum(platon_report$`# OriT`),
  plasmid_fasta             = platon_plasmid_fasta
)
#TODO plasmidblaster results?
#abricate_plasmidmarker_byclass <- get_abricate_by_class (abricate_plasmidmarker)

# software_versions = software_versions.df, # TODO load files
# database_versions = database_versions.df  # TODO load files

# compile big list --------------

sample_summary_list <- list(
  # meta information
  sample                   = sample,
  meta_information         = meta_information.df,
  version_information_list = version_information_list,
  mlst                     = mlst.df,
  amr                      = amr.list,
  virulence                = virulence.df,
  plasmids                 = plasmids.df,
  #software_versions = software_versions.df,
  #database_versions = database_versions.df
  reference_information    = reference_information.df #, # TODO move up
)

# optinonal modules

if(do_ani) {
  ani.df              <- data.frame(
    module = "ani"
  )
  module_list         <- list(ani.df)
  names(module_list)  <- "ani"
  sample_summary_list <- c(sample_summary_list, module_list)
}

# prokka ----

# if(do_prokka){
#   prokka.df <- data.frame(
#     module="prokka"
#     # TODO ad mode stuff here
#   )
#   module_list <- list(prokka.df)
#   names(module_list) <- "prokka"
#   sample_summary_list <- c(sample_summary_list,module_list)
# }
#

# bakta
if(do_bakta) {
  bakta.df            <- summary_bakta
  # data.frame(
  #   #summary_bakta,
  # bakta_extra,
  #   module="bakta"
  #   # TODO ad mode stuff here
  # )
  module_list         <- list(bakta.df)
  names(module_list)  <- "bakta"
  sample_summary_list <- c(sample_summary_list, module_list)
}

# fastani
if(do_ani) {
  fastani.df          <- data.frame(
    best_reference = report_fastani$ReferenceStrain[1],
    ani            = report_fastani$ANI[1],
    coverage       = report_fastani$Coverage[1]
  )
  module_list         <- list(fastani.df)
  names(module_list)  <- "fastani"
  sample_summary_list <- c(sample_summary_list, module_list)
}

# species specific modules ---------------------

# if Salmonella
if(module_species == "Salmonella") {

  # report_serotype$serovar_antigen
  # compact_genes(salmonella_subspecies,name_col = "subspecies",value_col = "score")
  # paste(report_SPI_selected$SPI, collapse = ";")
  SPI <- compact_genes(report_SPI_selected, name_col = "SPI", value_col = "coverage")

  salmonella.df       <- data.frame(
    serovar          = report_serotype$serovar,
    serogroup        = report_serotype$serogroup,
    subspecies       = salmonella_subspecies$subspecies,
    subspecies_score = salmonella_subspecies$score,
    SPI              = SPI,
    module           = "Salmonella"
  )
  module_list         <- list(salmonella.df)
  names(module_list)  <- module_species # name by species?
  #names(module_list) <- "species_module" # alternative
  sample_summary_list <- c(sample_summary_list, module_list)

} else if(module_species == "Ecoli") {
  # if Ecoli

  # now in pathotyping file?!
  #stx_type_subunits <- c(pathotyping$stxA1,pathotyping$stxB1,pathotyping$stxA2,pathotyping$stxB2)
  #stx_type_subunits <- paste(stx_type_subunits[!is.na(stx_type_subunits)],collapse = ";")
  # TODO remove in flatfile? How?
  #Ecoli_EFSA_VFs <- c("VT1_positive","VT2_positive","eae_positive","aat_positive","aggR_positive","aaiC_positive")

  Ecoli.df            <- data.frame(
    module              = "Ecoli",
    serotype            = OHgroup,
    antigens            = paste(summary_OH$GENE, collapse = ";"),
    clermont_type       = clermont_type,
    #clermont_details = clermont_details,
    pathoprofile        = pathotyping$profile,
    pathotype           = pathotyping$pathotype,
    # EFSA
    # TODO remove in flatfile? How?
    VT1_positive        = ifelse(is.na(pathotyping$stxA1) & is.na(pathotyping$stxB1), F, T),
    VT2_positive        = ifelse(is.na(pathotyping$stxA2) & is.na(pathotyping$stxB2), F, T),
    eae_positive        = ifelse(is.na(pathotyping$eae), F, T),
    aat_positive        = ifelse(is.na(pathotyping$aatA), F, T),
    aggR_positive       = ifelse(is.na(pathotyping$aggR), F, T),
    aaiC_positive       = ifelse(is.na(pathotyping$aaiC), F, T),
    # >>>
    #stx_type = stx_type 
    # stx_type_subunits = stx_type_subunits,
    # stx_type_fullregion = NA
    # ---
    # TODO shifted to ecoli_pathotyper
    stx_type_subunits   = pathotyping$stx_type_subunits,
    stx_type_fullregion = pathotyping$stx_type_fullregion
    # <<<
  )
  module_list         <- list(Ecoli.df)
  names(module_list)  <- module_species
  sample_summary_list <- c(sample_summary_list, module_list)

} else if(module_species == "Bacillus") {
  bacillus.df         <- bacillus_sample_summary %>% select(-sample)
  module_list         <- list(bacillus.df)
  names(module_list)  <- module_species
  sample_summary_list <- c(sample_summary_list, module_list)

} else if(module_species == "Staphylococcus") {
  staphylococcus.df   <- data.frame(
    count_saureus_toxin = length(report_saureus_toxin$GENE),
    genes_saureus_toxin = paste(sort(report_saureus_toxin$GENE), collapse = ";"),
    count_sccmec        = length(report_sccmec$GENE),
    genes_sccmec        = paste(sort(report_sccmec$GENE), collapse = ";"),
    # TODO add sccmec type
    type_sccmec_CGE     = classreport_sccmec$SCCmectype,
    type_sccmec         = paste(colnames(report_staphopia_sccmec)[which(report_staphopia_sccmec == "True")], collapse = "/"),
    count_enterotoxins  = length(report_vfdb_extra$GENE),
    genes_enterotoxins  = paste(sort(report_vfdb_extra$GENE), collapse = ";")

  )
  # TODO add VFDB_setB enterotoxins/Exotoxin ?
  module_list         <- list(staphylococcus.df)
  names(module_list)  <- module_species
  sample_summary_list <- c(sample_summary_list, module_list)

} else if(module_species == "Listeria") {
  listeria.df         <- report_lissero %>%
    select(-ID) %>%
    dplyr::rename(serotype = SEROTYPE, comment = COMMENT, serogroup = SEROGROUP)
  module_list         <- list(listeria.df)
  names(module_list)  <- module_species
  sample_summary_list <- c(sample_summary_list, module_list)

  listeria_simple.df <- data.frame(
    serotype = report_lissero$SEROTYPE[1]
  )
}
#else if (module_species == "Bacillus"){

#staphopia_sccmec: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
#colnames(score_staphopia_sccmec)[which(score_staphopia_sccmec == 0)] 

#colnames(score_staphopia_sccmec)[which(score_staphopia_sccmec == min(score_staphopia_sccmec[-1] ))] 
#min(score_staphopia_sccmec[-1]
#score_staphopia_sccmec

#paste(colnames(report_staphopia_sccmec)[which(report_staphopia_sccmec == "Truet")], collapse = "/")
# <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

# export ----

#sample_summary_list

# as yaml
if(write_yaml) {
  write_yaml(sample_summary_list, yaml.outfile)
}

# as json
# create json
summary_json <- toJSON(sample_summary_list, indent = 4, method = "C")

# write json
#summary_json

write(summary_json, json.outfile)
message("Done creating sample json")

# -----------------------------------------------
# flatten the output -------------------------

# make a copy of sample_summary
sample_summary_list2 <- sample_summary_list

# remove and replace list elements
sample_summary_list2     <- sample_summary_list2[names(sample_summary_list2) != "sample"]
sample_summary_list2     <- sample_summary_list2[names(sample_summary_list2) != "version_information_list"]
sample_summary_list2     <- sample_summary_list2[names(sample_summary_list2) != "bakta"]
#sample_summary_list2 <- sample_summary_list2[names(sample_summary_list2) != "amr"]
sample_summary_list2$amr <- amr.df

# simplify. reduce

# Listeria
# also keep detailed lissero results in flat file
# if (module_species == "Listeria"){
#   sample_summary_list2$Listeria <- listeria_simple.df
# }  

if(module_species == "Ecoli") {
  # remove EFSA VFs from flatfile
  sample_summary_list2$Ecoli <- Ecoli.df %>% select(grep("_positive", colnames(Ecoli.df), value = TRUE, invert = TRUE))
  # untidy way
  #Ecoli.df[,!grepl("_positive",colnames(Ecoli.df))] 
}

# convert factors to characters
sample_summary_list3 <- lapply(sample_summary_list2, function(x) {
  if(is.data.frame(x)) {
    x %>% mutate(across(where(is.factor), as.character))
  } else {
    message("not a data frame")
    return(x)
  }
})

# unlist
samples_summary_flat    <- unlist(sample_summary_list3)
# convert to dataframe of key value pairs
samples_summary_flat.df <- data.frame(key = names(samples_summary_flat), value = samples_summary_flat)

# export flat file
#samples_summary_flat.df %>% as_tibble()
#knitr::kable(samples_summary_flat.df)

write.table(samples_summary_flat.df, flat.outfile, quote = FALSE, sep = "\t", row.names = FALSE, col.names = FALSE)
message("Flat file exported to ", flat.outfile)

