#!/bin/bash

## [Goal] Run BakCharak in batches on AQUAMIS or the GMI test dataset
## [Author] Holger Brendebach
pipeline="BakCharak"

## Shell Behaviour --------------------------------------------------
set -Euo pipefail # to exit immediately, add e

## (1) Argument Logic

usage() {
  cat <<- USE

${bold}Basic usage:${normal}
bash $script_name

${bold}Description:${normal}
Run ${pipeline} in batch mode based on entries in the ${pipeline,,}_runs.tsv table.
Any script arguments that do not match the ones in the next section will be appended to the run command, e.g. "--unlock".
This wrapper may evaluate several run strategies in its run_modules() function: ${pipeline,,}.py, direct Snakemake calls or Docker container runs.

${bold}Optional Named Arguments:${normal}
  -e, --env_name             Override conda environment name derived from envs/${pipeline,,}.yaml (default=${pipeline,,})
  -p, --profile              Set a Snakemake profile, e.g. workstation (default=bfr.hpc)
  -c, --from_config {file}   Deactivate batch mode and use path to ${pipeline} config file to update the run
  --from_aquamis {file}      Deactivate batch mode and use path to AQUAMIS summary_report table (default=<${pipeline,,}>/test_data/aquamis/summary_report.tsv symlink)
  -s, --snakemake            Skip the ${pipeline,,}.py wrapper script and use a customized Snakemake command; this also skips config_${pipeline,,}.yaml recreation
  -cdb, --use_ceph_db        ${grey}[BfR-only] Use the databases on CephFS${normal}
  -cex, --use_ceph_exec      ${grey}[BfR-only] Use pipeline repository on CephFS for executing ${pipeline,,}.py${normal}
  --dryrun                   Perform a dryrun and include a Snakemake dryrun for DAG calculation (default=false)
  -n, --norun                Perform a dryrun without Snakemake commands (default=false), implies --auto
  -a, --auto                 Skip interactive confirmation dialogue (default=false), e.g. for tmux-wrapped script calls

${bold}Notes:${normal}
Check the manual_override() function to tweak script arguments for repetitive use.
Check the tweak_settings() function to review potential system-specific expert settings.
If you like script fail notifications by email, set the environment variable "email" to activate the error_handler() function.

${bold}Example:${normal}
bash $script_name -p bfr.hpc -a -n

USE
  exit 0
}

parse_args() {
  local args=("$@")

  ## default values of variables
  repo_path=$(dirname "$script_path")
  profile="bfr.hpc"
  conda_recipe="$repo_path/envs/${pipeline,,}.yaml"
  docker_image="bfrbioinformatics/${pipeline,,}:latest"
  summary_report_path="$repo_path/test_data/aquamis/summary_report.tsv"

  ## default values of switches
  mode="batch"
  snakemake=false
  use_ceph_db=false
  use_ceph_exec=false
  norun=false
  dryrun=false
  interactive=true

  while :; do
    case "${1-}" in
      -h | --help) usage ;;
      --dev) dev_notes "$script_real" ;; # from helper_functions.sh
      -v | --verbose)
        set -x
        shift
        ;;
      -e | --env_name)
        conda_recipe_env_name="$2"
        shift 2
        ;;
      -p | --profile)
        profile="$2"
        shift 2
        ;;
      -c | --from_config)
        mode="config"
        configfile="$2"
        shift 2
        ;;
      --from_aquamis)
        mode="report"
        if [[ -f $2 ]]; then
          summary_report_path="$2"
          shift 2
        else
          shift
        fi
        ;;
      -s | --snakemake)
        snakemake=true
        shift
        ;;
      -cdb | --use_ceph_db)  # BfR-specific
        use_ceph_db=true
        shift
        ;;
      -cex | --use_ceph_executable)  # BfR-specific
        use_ceph_exec=true
        shift
        ;;
      -n | --norun)
        norun=true
        dryrun=true
        interactive=false
        user_args+="--dryrun "
        shift
        ;;
      --dryrun)
        dryrun=true
        user_args+="--dryrun "
        shift
        ;;
      -a | --auto)
        interactive=false
        shift
        ;;
      -?*)
        user_args+="${1-} "
        shift
        ;;
      *) break ;;
    esac
  done

  ## checks
  [[ -f $conda_recipe ]] || die "The conda recipe does not exist: $conda_recipe"

  [[ $dryrun == true ]] && echo -e "\n${bold}${green}-->  This is a dryrun with no log  <--${normal}\n"
  [[ $dryrun == false ]] && echo -e "\n${bold}${magenta}-->  This is NOT a dryrun!  <--${normal}\n"

  return 0
}

## Source Helper Functions --------------------------------------------------
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname "$script_real")
script_name=$(basename "$script_real")
[[ ! -f "$script_path/helper_functions.sh" ]] && echo "[ERROR] File not found: $script_path/helper_functions.sh" && kill -s TERM $$
# shellcheck source=./helper_functions.sh
source "$script_path/helper_functions.sh"

## Parse Arguments --------------------------------------------------
args="$*"
parse_args "$@"

## (2) Script Logic

## Notification on unexpected behaviour
[[ $(declare -F error_handler) == error_handler ]] && [[ -n ${email+x} ]] && msg "Email notification enabled" && trap error_handler SIGINT SIGTERM # ERR # TODO: read HEREDOC causes an unknown error

manual_override() {
  ## (1) Hardcoded Parameters that will override defaults, comment-in to activate

  ## Conda Environment
#  [[ "$profile" == "workstation" ]] && conda_recipe_env_name="bakcharak3"  # NOTE: fix for non-standard env names
#  conda_recipe_env_name="bakcharak3"  # for NGSAdmin@HPC01

  ## Code Base
#  repo_path="/cephfs/abteilung4/NGS/Pipelines/bakcharak"  # production version, use flag --use_ceph_exec

## Redirect to other Databases
  [[ "$use_ceph_db" == true ]] && ceph_referencedb_path=/cephfs/abteilung4/NGS/ReferenceDB/bakcharak && bakcharak_args+=$(cat <<- EOF
    --mlst_db $ceph_referencedb_path/pubmlst
    --mlst_blastdb $ceph_referencedb_path/pubmlst/mlst.fa
    --abricate_db $ceph_referencedb_path/genefinder
    --vfdb $ceph_referencedb_path/genefinder
    --amrfinder_db $ceph_referencedb_path/amrfinder/data/latest
    --phenomatch_db $ceph_referencedb_path/phenotypes/phenotype_amrfinder_matchtable_wPhenotypes.tsv
    --mash_plasmid_db $ceph_referencedb_path/mash/plasmids/plsdb.msh
    --mash_genome_db $ceph_referencedb_path/mash/genomes/refseq.genomes.k21s1000.msh
    --mash_genome_info $ceph_referencedb_path/mash/genomes/refseq.genomes.k21s1000.info
    --plasmidblast_db $ceph_referencedb_path/plasmidblast
    --platon_db $ceph_referencedb_path/platon
    --bakta_db $ceph_referencedb_path/bakta
EOF
)

  ## Snakemake Profiles (see config.yaml within)
#  profile="bfr.hpc"
#  profile="bfr.hpc.greedy"
#  profile="workstation"

  ## Resources
  [[ "$profile" == "bfr.hpc.greedy" ]] && threads_sample=6
  [[ "$profile" == "bfr.hpc"        ]] && threads_sample=4
  [[ "$profile" == "workstation"    ]] && threads_sample=1

  ## Docker Images
#  docker_image="bfrbioinformatics/${pipeline,,}:3.1.4"

  true
}

## Define Paths --------------------------------------------------

define_paths() {
  ## [Rationale] Manual Run Parameters Definition

  ## Conda Environment
  conda_recipe_env_name=${conda_recipe_env_name:-$(head -n1 "$conda_recipe" | cut -d' ' -f2)}

  ## Resources
  profile_base="$repo_path/profiles"
  profile="$profile_base/$profile"

  ## Logging
  logfile_name=${script_name/.sh/.log}
  [[ $dryrun == false ]] && logfile="$script_path/$logfile_name"
  [[ $dryrun == false ]] && logfile_global=/dev/null

  ## Run variables; if in mode "batch" {run_name,dir_samples,dir_data,etc.} will be read from run_table
  run_table="$script_path/${pipeline,,}_runs.tsv"
}

set_run_paths() {
  [[ -d $dir_data                  ]] || logexec mkdir -p "$dir_data"
  logecho "cd $dir_data"
  cd "$dir_data" || die "Cannot change to working directory: $dir_data"

  [[ -f "$dir_samples/samples.tsv" ]] && sample_list="$dir_samples/samples.tsv"
  [[ -z "${configfile:-}"          ]] && configfile="$dir_data/config_${pipeline,,}.yaml"
  [[ -z "${run_name:-}"            ]] && run_name="test_data_$(date +%F)_$(hostname)_$mode"

  # use BfR accredited version
  [[ $use_ceph_exec == true && -d "/cephfs/abteilung4/NGS/Pipelines/bakcharak" ]] && repo_path="/cephfs/abteilung4/NGS/Pipelines/bakcharak"

  # checks
  [[ ! -d "$dir_samples" ]] && die "The sample directory does not exist: $dir_samples"
  [[ ! -f "${sample_list:-}" ]] && die "The sample list does not exist: ${sample_list:-unbound_variable}"
  true
}

## Info --------------------------------------------------

show_info() {
  logheader "General Settings:"
  echo "
Mode:|$mode
Repository:|$repo_path
Conda Recipe:|$conda_recipe
Conda Recipe Env Name:|$conda_recipe_env_name
Threads per Sample:|$threads_sample
Profile:|$profile
Snakemake User Args:|${user_args:-}
Logfile:|$logfile
" | column -t -s="|" | tee >(logglobal 1> /dev/null) && sleep 0.2
  true
}

show_info_on_run() {
  ## print current run infos
  cat <<- TXT | tee -a "$logfile" "$logfile_global"

${yellow}#################################################################### Processing:${normal}

Run:           ${inverted}$run_name${normal}
SampleList:    ${bold}${sample_list:-"not yet created"}${normal}
WorkDir:       ${bold}$dir_data${normal}
Genus:         ${bold}${sample_genus:-"not set"}${normal}

${blue}To monitor the Snakemake log use:${normal}
tail -F -s3 \$(find $dir_data/.snakemake/log -name "*.snakemake.log" -print0 | xargs -r -0 ls -1 -t | head -1)
TXT
}

show_info_on_tmux() {
  ## Prerequisite for tmux wrapping: add `set-option -g default-shell "/bin/bash"` to ~/.tmux.conf
  cat <<- TXT | tee -a "$logfile" "$logfile_global"

${blue}Best experience with tmux:${normal}
tmux new -d -s ${pipeline,,}_\$USER
tmux send-keys -t ${pipeline,,}_\${USER}.0 'bash $script_real --auto' ENTER

All steps were logged to: $logfile and $logfile_global (global)
TXT
}

## Checks --------------------------------------------------

run_checks() {
  [[ ! -d $profile ]] && die "The snakemake profile directory does not exist: $profile"
  [[ $mode == "batch"  && ! -f $run_table           ]] && die "The run table does not exist: $run_table"
  [[ $mode == "config" && ! -f $configfile          ]] && die "The config file does not exist: $configfile"
  [[ $mode == "report" && ! -f $summary_report_path ]] && die "The AQUAMIS summary report does not exist: $summary_report_path"
  # TODO: add more for novices
  true
}

## More Settings --------------------------------------------------

tweak_settings() {
  ## more privacy towards NCBI
  declare -x BLAST_USAGE_REPORT=false # see https://github.com/tseemann/mlst/issues/115
  true
}

activate_conda() {
  ## activate environment
  if [[ "$conda_env_active" != "$conda_env_name" ]]; then
    first_conda_env=$(head -n1 <<< "$conda_env_path")  # use first entry if multiple envs with the same name exist, e.g. from other users
    logecho "Conda environment $conda_env_name has not been activated yet. Activating Conda environment \"$first_conda_env\""
    set -u && source "$conda_base/etc/profile.d/conda.sh" && set +u
    conda activate "$first_conda_env"
  elif [[ $use_ceph_exec == true ]]; then  # BfR-specific
    logwarn "Overriding first conda environment path with CEPH environment path because the flag --use_ceph_executable was set"
    conda_env_path="/home/NGSAdmin/miniconda3/envs/bakcharak3"
    [[ ! -d "$conda_env_path" ]] && die "The conda environment does not exist: bakcharak3"
    set -u && source "$conda_base/etc/profile.d/conda.sh" && set +u
    conda activate $conda_env_path
  else
    logecho "Conda environment $conda_env_name was already activated"
  fi

  ## check
  logecho "Using $(conda info | grep "active env location" | sed -e "s/^\s*//")"
}

## Modules --------------------------------------------------

run_bakcharak() {
  logheader "Running ${pipeline} via ${pipeline,,}.py:"
  [[ -n ${sample_genus:-} ]] && bakcharak_args+="--species $sample_genus "
  read -r -d '' wrapper_cmd <<- EOF
  python3 $repo_path/${pipeline,,}.py
    --profile $profile
    --working_directory $dir_data
    --threads_sample $threads_sample
    --sample_list $sample_list
    ${bakcharak_args:-}
    ${user_args:-}
EOF
#    --bakta
#    --profile $profile
#    --force all_trimming_only
#    --version
#    --conda_frontend
#    --condaprefix $CONDA_BASE/envs
  run_command_array $wrapper_cmd
}

run_snakemake_report() {
  logheader "Generating Snakemake Report:"
  read -r -d '' wrapper_cmd <<- EOF
  snakemake
    --profile $profile
    --configfile $configfile
    --snakefile $repo_path/Snakefile
    --forceall
    --rulegraph | dot -Tpdf -o $dir_data/rulegraph-all.pdf
EOF
#    --dag | dot -Tpdf -o $dir_data/dag-all.pdf
#    --report $dir_data/reports/smk_report.html
  run_command_array $wrapper_cmd
}

run_modules() {
  ## [Rationale] Define per-run module set - choose between Python wrapper, Snakemake or Docker
  set_run_paths
  show_info_on_run
  [[ $snakemake == false ]] && run_bakcharak
#  [[ $snakemake == true  ]] && run_snakemake  # TODO: not yet used
#  run_snakemake_report
}

## Sample Mode --------------------------------------------------

runs_from_run_table() {
  local run_table

  run_table=${1-}

  ## loop through run_table
  mapfile -t runArray <"$run_table"
  for line in "${runArray[@]}"; do # loop through runs in run_table
    IFS=$'\t' field=($line) IFS=$' \t\n'
    [[ "${field[0]}" =~ ^#.* ]] && continue # skip commented samples
    run_name=$(eval "echo \"${field[0]}\"")
    dir_samples=$(realpath "$(eval "echo \"${field[1]}\"")")
    dir_data=$(realpath -m "$(eval "echo \"${field[2]}\"")")
    [[ ${#field[@]} -eq 4 ]] && sample_genus=$(eval "echo \"${field[3]}\"")
    run_modules
  done
}

samples_from_summary_report() {
  local summary_report
  local assembly_dir

  summary_report=${1:-}

  aquamis_working_directory="$(dirname "$(dirname "$(realpath "$summary_report")")")"
  assembly_dir=${2:-"$aquamis_working_directory/Assembly/assembly"}
  [[ -d $assembly_dir ]] || die "The AQUAMIS assembly_dir does not exist: $assembly_dir"
  assembly_sample_sheet_pass="$assembly_dir/samples_pass.tsv"
  [[ -f $assembly_sample_sheet_pass ]] || die "The AQUAMIS assembly_dir does contain a samples_pass.tsv: $assembly_sample_sheet_pass"

  dir_data="$repo_path/test_data/analysis_aquamis_data_$(date +%F)"
  assembly_sample_sheet="$dir_data/samples.tsv"
  sample_genus_dict="$dir_data/$(basename "$summary_report").tmp"

  ## generate sample sheet
  logecho "Creating temporary assembly sample sheet: $assembly_sample_sheet"
  [[ -d $dir_data ]] || logexec mkdir -p "$dir_data"
  logexec cp --force "$assembly_sample_sheet_pass" "$assembly_sample_sheet"

  ## loop through AQUAMIS summary_report.tsv
  logheader "Parsing summary report: $summary_report"

  awk -F'\t' '
    BEGIN {OFS=FS}
    NR==1 {
        for (i=1; i<=NF; i++) {
            if ($i=="Sample_Name") col1=i;
            if ($i=="Species") col2=i;
        }
    }
    NR>1 {
        gsub(/ .*/, "", $col2);
        print $col1, $col2
    }
' "$summary_report" | sort -k2 >"$sample_genus_dict"

  ## generate temp sample_sheets for each genus and append samples
  mapfile -t runArray <"$sample_genus_dict"
  for line in "${runArray[@]}"; do # loop through runs in run_table
    IFS=$'\t' field=($line)
    sample_name="${field[0]}"
    sample_genus="${field[1]}"
    dir_data_genus="$dir_data/$sample_genus"
    [[ "$sample_name" =~ ^#.* ]] && continue                                # skip commented-out samples
    if ! grep -qP "^$sample_name" "$assembly_sample_sheet"; then continue; fi # skip if sample not in sample sheet
    [[ -d $dir_data_genus ]] || logexec mkdir -p "$dir_data_genus"
    [[ -f $dir_data_genus/samples.tsv ]] || head -n1 "$assembly_sample_sheet" > "$dir_data_genus/samples.tsv" # add header to init samples.tsv if it does not exist
    if grep -qP "^$sample_name" "$dir_data_genus/samples.tsv"; then continue; fi                           # do not add duplicates
    grep -P "^$sample_name" "$assembly_sample_sheet" >> "$dir_data_genus/samples.tsv"
  done

  ## loop through sample_genus_dict
  dir_data_root="$dir_data"
  for line in "${runArray[@]}"; do # loop through runs in run_table
    IFS=$'\t' field=($line) IFS=$' \t\n'
    sample_genus="${field[1]}"
    dir_data="$dir_data_root/$sample_genus"
    dir_samples="$dir_data"
    run_modules
  done
}

run_from_configfile() {
  local configfile

  configfile=${1:-}
  bakcharak_args+="--config $configfile "

  ## get values from configfile
  sample_list=$(grep -E "^samples:" "$configfile" | awk -F': ' '{print $2}')
  dir_samples=$(dirname "$sample_list")
  dir_data=$(grep -E "^workdir:" "$configfile" | awk -F': ' '{print $2}')
  threads_sample=$(grep -E "^  threads:" "$configfile" | awk -F': ' '{print $2}')
  sample_genus=$(grep -E "^species:" "$configfile" | awk -F': ' '{print $2}')
  [[ "$sample_genus" =~ Unspecified ]] && unset sample_genus
  run_modules
}

## (3) Main

manual_override
define_paths
logglobal "[START] $script_real"
logglobal "Arguments: ${*:-"No arguments provided"}"
show_info
run_checks

## Workflow
check_conda "$conda_recipe_env_name"
activate_conda
[[ $interactive == true ]] && interactive_query
tweak_settings

## Switch between config, report or batch mode
case ${mode} in
  batch)
    logecho "Processing all active runs in ${magenta}run table${normal}: $run_table"
    runs_from_run_table "$run_table"
    ;;
  report)
    logecho "Processing all active runs in ${magenta}AQUAMIS summary report${normal}: $summary_report_path"
    samples_from_summary_report "$summary_report_path"
    ;;
  config)
    logecho "Processing run defined in ${pipeline} ${magenta}config file${normal}: $configfile"
    run_from_configfile "$configfile"
    ;;
  *) die "The {mode} was not specified or recognized. Check ${blue}bash $script_real --help${normal} for available parameters" ;;
esac

show_info_on_tmux
logglobal "[STOP] $script_real"
