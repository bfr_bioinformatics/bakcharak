#!/usr/bin/env python3

# [GOAL] python script for check for mesophylo signature

from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
#import argparse
#import snakemake
import os
import json
import pprint


# Define

# Input bakta dir
#bakta_dir = "/cephfs/abteilung4/deneke/Projects/bakcharak_dev/Bacillus/btyper3/bakcharak_newdb/results/BfR-BA-00883/bakta_backup"


# test data >>>>>>>>>>>>>>
# derived fasta file
#bakta_ffn = "/cephfs/abteilung4/deneke/Projects/bakcharak_dev/Bacillus/btyper3/bakcharak_newdb/results/BfR-BA-00883/bakta_backup/BfR-BA-00883.ffn"
# param
#sample = "BfR-BA-00883"
# output file
#outfile_json = "/home/DenekeC/tmp/bakcharak_tmp/mesopsychro/BfR-BA-00883.json"
#outfile_tsv = "/home/DenekeC/tmp/bakcharak_tmp/mesopsychro/BfR-BA-00883.tsv"
# <<<<<<<<<<<<<<<<<<


# snakemake commands

#def python_or_snakemake(debug: bool):
    #try:
        #snakemake.input[0]
    #except NameError:
        #print("snakemake attributes not available, using standard input")
        #if debug:
            #arguments_to_snakemake(args = create_arguments())
        #else:
            #arguments_to_snakemake(args = parse_arguments())


#try:
    #snakemake.input[0]
    #print("Snakemake ok")
#except NameError:
    #print("snakemake attributes not available, using standard input")

    #bakta_ffn = "/cephfs/abteilung4/deneke/Projects/bakcharak_dev/Bacillus/btyper3/bakcharak_newdb/results/BfR-BA-00883/bakta_backup/BfR-BA-00883.ffn"
    #sample = "BfR-BA-00883"
    #outfile_json = "/home/DenekeC/tmp/bakcharak_tmp/mesopsychro/BfR-BA-00883.json"


#test_file = snakemake.input['bakta_ffn']

print("snakemake:")
print(snakemake.input[0])

bakta_ffn = snakemake.input['bakta_ffn']
sample = snakemake.params['sample']
outfile_json = snakemake.output['summary_json']

print(f"Input: {bakta_ffn}")
print(f"Sample: {sample}")
print(f"Output: {outfile_json}")


# hard-coded parameters ---

#Gen	16S rRNA 		cspA	
#Signatur	CCTAGAGATAGG	TCTAGAGATAGA	GCAGTA	ACAGTT

genename_cspA = "RNA chaperone/antiterminator CspA"
signature_cspA_meso = "GCAGTA"
signature_cspA_psychro = "ACAGTT"

genename_16S = "16S ribosomal RNA"
signature_16S_meso = "CCTAGAGATAGG"
signature_16S_psychro = "TCTAGAGATAGA"

# initial values

g16S_rRNA_mesophilic = False #gene mesophilic signature
g16S_rRNA_psychrotolerant = False #gene psychrotolerant signature	
cspA_mesophilic = False #mesophilic signature	
cspA_psychrotolerant = False #psychrotolerant signature
g16S_count = 0
cspA_count = 0



# TODO argparser >>>
#parser = argparse.ArgumentParser()
#parser.add_argument('--input', '-i', help = '[REQUIRED] the unformatted input file',
                        #default = "/", type = os.path.abspath, required = True)
#parser.add_argument('--output', '-o', help = '[REQUIRED] the formatted output file',
                        #default = "/", type = os.path.abspath, required = True)
#parser.add_argument('--dbname', '-n', help = '[REQUIRED] the name of the database',
                        #default = "custom_database", type = str, required = True)

                        
#args = parser.parse_args()

# OR parse snakemake args 
# <<<

# ----------------

with open(bakta_ffn,'r') as fasta:
    for record in SeqIO.parse(fasta,'fasta'):
        gene = record.id
        description = record.description
        description_clean = ' '.join(description.split(' ')[1:])
        if genename_cspA in description_clean:
        #if description_clean == genename_cspA:
            print(f"ID: {gene}; desc: {description}")
            cspA_count += 1
            search_meso = record.seq.find(signature_cspA_meso)
            if search_meso != -1:
                cspA_mesophilic = True
                print("cspA has mesophilic signatures")

            search_psychro = record.seq.find(signature_cspA_psychro)
            if search_psychro != -1:
                cspA_psychrotolerant = True
                print("cspA has psychrotolerant signatures")
        
        # TODO replace string match by subsstring
        if genename_16S in description_clean:
        #if description_clean == genename_16S:
            print(f"ID: {gene}; desc: {description}")
            g16S_count += 1
            search_meso = record.seq.find(signature_16S_meso)
            if search_meso != -1:
                g16S_rRNA_mesophilic = True
                print("16S has mesophilic signatures")

            search_psychro = record.seq.find(signature_16S_psychro)
            if search_psychro != -1:
                g16S_rRNA_psychrotolerant = True
                print("16S has psychrotolerant signatures")

if g16S_count != 1:
    print(f"Warning: 16S gene count is NOT 1: {g16S_count}")
            
if cspA_count != 1:
    print(f"Warning: cspA gene count is NOT 1: {cspA_count}")
            
           
print(f"cspA_mesophilic: {cspA_mesophilic}")
print(f"cspA_psychrotolerant: {cspA_psychrotolerant}")
print(f"g16S_rRNA_mesophilic: {g16S_rRNA_mesophilic}")
print(f"g16S_rRNA_psychrotolerant: {g16S_rRNA_psychrotolerant}")


# export as json, yaml, tsv

summary_data = {"sample": sample, 
    "16S_rRNA_mesophilic_signature": g16S_rRNA_mesophilic,
    "16S_rRNA_psychrotolerant_signature": g16S_rRNA_psychrotolerant,
    "cspA_mesophilic_signature": cspA_mesophilic,
    "cspA_psychrotolerant_signature": cspA_psychrotolerant,
    "16S_count": g16S_count,
    "cspA_count": cspA_count
    }


# Convert and write JSON object to file
with open(outfile_json, "w") as outfile: 
    #json.dump(summary_data, outfile)
    #json.dumps(summary_data, outfile, indent=4)
    json.dump(summary_data,
          fp = outfile,
          indent = 4,
          sort_keys = False,
          ensure_ascii = False,
          allow_nan = False)



print("Done")
