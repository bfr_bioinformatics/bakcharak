#!/bin/bash
set -e
set -u
set -o pipefail

# Goal: Build custom mash db with chromosomal DNA only
version=0.1


#TODO download genomes
#TODO update instead of complete new build


# input
dbDir="/cephfs/abteilung4/NGS/ReferenceDB/NCBI/CompleteGenomes"
refbase="$dbDir"
#refbase="/cephfs/abteilung4/NGS/ReferenceDB/NCBI/Refseq/complete_genomes/ncbi"


fixed_fasta="$refbase/plasmids_removed"
mash_dir="$refbase/mash"


mash_ref="$mash_dir/Refseq_completeBacteria_chromosomes_s1000_k21.msh"
mash_info="$mash_dir/Refseq_completeBacteria_chromosomes_s1000_k21.info"

download_log="$dbDir/download.log"
log="$refbase/remove_plasmids.log"
mash_log="$mash_dir/mash_builddb.log"




## Helpfile and escape
if [ $# -eq 0 ]; then
    echo "Please provide at least one argument. For the help file, type Fix_Assemblynames.sh  --help"
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi

## search for "--help" flag
if [[ $@ =~ '--help' || $@ =~ '-h' ]]; then
    echo "Help file ----------------------"
    echo "Basic usage: mash_buildDB.sh [flags]"
    echo
    echo "--dryrun"
    echo "--build"
    echo "--interactive"
    
    echo "Paths are (currently hard.coded)"
    
    echo "refbase: $refbase"
    echo "fixed_fasta: $fixed_fasta"
    echo "mash_dir: $mash_dir"
    echo "mash_ref: $mash_ref"
    echo "mash_info: $mash_info"
    echo "log: $log"
    echo "mash_log: $mash_log"
    
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
fi    


if [[ $@ =~ '--dryrun' ]]; then
    dryrun=true
else
    dryrun=false
fi


if [[ $@ =~ '--interactive' ]]; then
    interactive=true
else
    interactive=false
fi


if [[ $@ =~ '--build' ]]; then
    build=true
else
    build=false
fi


if [[ $dryrun != "true" ]]; then
    build=false
fi


# Definitions -----------------


interactive=true

echo "Parameters:"
echo
echo "refbase: $refbase"
echo "fixed_fasta: $fixed_fasta"
echo "mash_dir: $mash_dir"
echo "mash_ref: $mash_ref"
echo "mash_info: $mash_info"
echo "log: $log"
echo "mash_log: $mash_log"
echo "interactive: $interactive"
echo "build: $build"
echo "dryrun: $dryrun"



# ask if in interactive mode
if [[ $interactive == true ]]
then
    read -p "Do you want to continue? " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1
    fi
fi

# ---------------------------------------------------------

if [[ $build == true ]];then

mkdir -p $fixed_fasta
mkdir -p $mash_dir



# 0. Download genomes
# start from NCBI Refseq folder/ bacteria
# start from assembly summary, get all genomes with rsync



#wget -P $dbDir https://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt

echo "rsync rsync://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt $dbDir"
rsync --progress rsync://ftp.ncbi.nlm.nih.gov/genomes/refseq/bacteria/assembly_summary.txt $dbDir

awk -F "\t" '$12=="Complete Genome" && $11=="latest"{print $0}' $dbDir/assembly_summary.txt > $dbDir/assembly_summary_complete.txt
cut -f 20 $dbDir/assembly_summary_complete.txt > $dbDir/ftpdirpaths

awk 'BEGIN{FS=OFS="/";filesuffix="genomic.fna.gz"}{ftpdir=$0;asm=$10;file=asm"_"filesuffix;print ftpdir,file}' $dbDir/ftpdirpaths > $dbDir/ftpfilepaths

cat $dbDir/ftpfilepaths | sed 's/ftp:/rsync:/g' > $dbDir/rsyncfilepaths



while read url; do
    echo "Downloading $url" | tee -a $download_log
    echo "rsync --copy-links --times --verbose $url $dbDir/$speciesFolder" | tee -a $download_log
    rsync --progress --copy-links --times --verbose $url $dbDir 2>&1  | tee -a $download_log
done < $dbDir/rsyncfilepaths

#refbase=$dbDir

# 1. Remove plasmid contigs with bioawk

for file in $refbase/*_genomic.fna.gz; do
    echo "Processing file $file | tee -a $log"
    filename=`basename $file`
    bioawk -cfastx '{if($comment !~ /plasmid/) print ">"$name" "$comment"\n"$seq}' $file | gzip > $fixed_fasta/$filename
done



# 2. make mash db
source activate bioinfo

echo "Starting building new mashdb on `date`" | tee $mash_log

for file in $fixed_fasta/*.fna.gz; do
    sample=`basename -s .fna.gz $file`
    echo "mash sketch -p 10  -k 21 -s 1000 -o ${mash_dir}/$sample $file" | tee -a $mash_log
    mash sketch -p 10  -k 21 -s 1000 -o ${mash_dir}/$sample $file
done





echo "Merging genome sketches ..."
echo "mash paste $mash_ref $mash_dir/GCF*.msh"
mash paste $mash_ref $mash_dir/GCF*.msh


echo "Extracting sketch info ..."
echo "mash info $mash_ref > $mash_info"
mash info $mash_ref > $mash_info


echo "Finished building new mashdb on `date`" | tee -a $mash_log


fi # do build
