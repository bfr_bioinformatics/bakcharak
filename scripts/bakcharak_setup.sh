#!/bin/bash

## [Goal] Install mamba, conda env, complement conda env, download databases, download test data for BakCharak
## [Author] Holger Brendebach, Carlus Deneke, Andreas Stroehlein
pipeline="BakCharak"

## Shell Behaviour --------------------------------------------------
set -Euo pipefail # to exit immediately, add e

## (1) Argument Logic

usage() {
  cat <<- USE

${bold}Basic usage:${normal}
bash $script_name [OPTIONS]

${bold}Description:${normal}
This script completes the installation of the ${pipeline} pipeline. The openssl library is required for hashing the downloaded files.
For ${pipeline} installations from Gitlab use the option set [--mamba --databases].
For more information, please visit https://gitlab.com/bfr_bioinformatics/${pipeline,,}

${bold}Options:${normal}
  -s, --status               Show installation status
  -e, --env_name             Override conda environment name derived from envs/${pipeline,,}.yaml (default: ${pipeline,,})
  -m, --mamba                Install the latest version of 'mamba' to the Conda base environment and
                             create the ${pipeline} environment from the git repository recipe
  -a, --amrfinder_update     Download the latest AMRfinder database
  -d, --databases            Download databases to <${pipeline}>/download and extract them in <${pipeline}>/databases
  -b, --bakta {light,full}   Download the BAKTA light (2G) or full (33G) database to <${pipeline}>/download and extract it in <${pipeline}>/databases/bakta
  -t, --test_data            Download test data to <${pipeline}>/download and extract them in <${pipeline}>/test_data
  -cdb, --symlinks_to_ceph   ${grey}[BfR-only] Create symbolic links to databases on CephFS${normal}
  -f, --force                Force overwrite for downloads in <${pipeline}>/download
  -k, --keep_downloads       Do not remove downloads after extraction
  --auto                     Do not ask for interactive confirmation, e.g. for tmux-wrapped script calls
  -n, --dryrun               Perform a dryrun to review the commands
  -h, --help                 Show this help

${bold}Example:${normal}
bash $script_real --env_name ${pipeline,,} --mamba --databases --bakta light --test_data --keep_downloads --dryrun

USE
  exit 0
}

parse_args() {
  local args=("$@")

  ## default values of variables
  repo_path=$(dirname "$script_path")
  conda_recipe="$repo_path/envs/${pipeline,,}.yaml"
  arg_dryrun=""

  ## default values of switches
  conda_status=false
  mamba_status=false
  bioconda_status=false

  arg_mamba=false
  arg_amrfinder=false
  arg_bakta=false
  arg_databases=false
  arg_test_data=false
  arg_ceph_symlinks=false
  arg_status=false
  arg_keep_dl=false
  force=false
  dryrun=false
  interactive=true

  while :; do
    case "${1-}" in
      -h | --help) usage ;;
      --dev) dev_notes "$script_real" ;; # from helper_functions.sh
      -v | --verbose)
        set -x
        shift
        ;;
      -e | --env_name)
        conda_recipe_env_name="$2"
        shift 2
        ;;
      -m | --mamba)
        arg_mamba=true
        shift
        ;;
      -a | --amrfinder_update)
        arg_amrfinder=true
        shift
        ;;
      -b | --bakta)
        arg_bakta=true
        arg_bakta_db=${2:-light}
        shift 2
        ;;
      -d | --databases)
        arg_databases=true
        shift
        ;;
      -t | --test_data)
        arg_test_data=true
        shift
        ;;
      -cdb | --symlinks_to_ceph)  # this is a BfR-specific option
        arg_ceph_symlinks=true
        shift
        ;;
      -s | --status)
        arg_status=true
        shift
        ;;
      -f | --force)
        force=true
        shift
        ;;
      -k | --keep_downloads)
        arg_keep_dl=true
        shift
        ;;
      --auto)  # -a is blocked by --amrfinder_update
        interactive=false
        shift
        ;;
      -n | --dryrun)
        dryrun=true
        arg_dryrun=" --dryrun"
        shift
        ;;
      -?*) die "Unknown option: $1. For the manual, type ${blue}bash $script_real --help${normal}" ;;
      *) break ;;
    esac
  done

  ## check required arguments
  [[ ${#args[@]} -eq 0 ]] && die "Missing script arguments. For the manual, type ${blue}bash $script_real --help${normal}"
  [[ ( "$arg_databases" == true || "$arg_bakta" == true ) && "$arg_ceph_symlinks" == true ]] && die "The options --databases or --bakta and --symlinks_to_ceph are mutually exclusive. Please choose only one."

  ## checks
  [[ $(basename "$script_path") != "scripts" ]] && die "This setup script does not reside in its original installation directory. This is a requirement for proper execution. Aborting..."
  [[ -f $conda_recipe ]] || die "The conda recipe does not exist: $conda_recipe"

  ## avoid deletion of pre-existing downloads
  [[ -d "$repo_path/download" && $arg_keep_dl == false ]] && logwarn "Skipping default clean-up because download directory already exists: $repo_path/download" && arg_keep_dl=true

  [[ $dryrun == true ]] && echo -e "\n${bold}${green}-->  This is a dryrun with no log  <--${normal}\n"

  return 0
}

## Source Helper Functions --------------------------------------------------
script_real=$(realpath -P "${BASH_SOURCE[0]}")
script_path=$(dirname "$script_real")
script_name=$(basename "$script_real")
[[ ! -f "$script_path/helper_functions.sh" ]] && echo "[ERROR] File not found: $script_path/helper_functions.sh" && kill -s TERM $$
source "$script_path/helper_functions.sh"

## Parse Arguments --------------------------------------------------
args="$*"
parse_args "$@"

## (2) Script Logic

## Clean-up on unexpected behaviour

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  [[ -d "$repo_path/download" ]] && [[ -n ${arg_keep_dl+x} ]] && logheader "Clean Up:" && rm -v -R $repo_path/download
  true
}

[[ $(declare -F cleanup) == cleanup && "$arg_keep_dl" == false ]] && logecho "Clean up of download directory is enabled" && trap cleanup SIGINT SIGTERM EXIT ERR

## Define Paths --------------------------------------------------

define_paths() {
  ## [Rationale] define_paths()

  ## Conda Environment
  conda_recipe_env_name=${conda_recipe_env_name:-$(head -n1 $conda_recipe | cut -d' ' -f2)}

  ## Logging
  logfile_name=${script_name/.sh/.log}
  [[ $dryrun == false ]] && logfile="$script_path/$logfile_name"
  [[ $dryrun == false ]] && logfile_global=/dev/null

  return 0
}

## Info --------------------------------------------------

show_info() {
  logheader "Parameters:"
  echo "
Repository:|$repo_path
Conda Recipe:|$conda_recipe
Conda Recipe Env Name:|$conda_recipe_env_name
Keep Downloads:|$arg_keep_dl
Logfile:|$logfile
" | column -t -s="|" | tee -a $logfile $logfile_global
}

## Modules --------------------------------------------------

download_file() {

  # init local variables
  local remote_archive local_archive hashfile extraction_directory
  download_success=false
  remote_archive=$1
  local_archive=$2
  hashfile=$3
  extraction_directory=$4

  # set global variable
  download_success=false

  # Download
  if ( [[ ! -f $local_archive ]] || [[ $force == true ]] ); then
    logecho "Downloading $remote_archive to $local_archive"
    logexec "wget --output-document $local_archive $remote_archive" && download_success=true

    # Verify Integrity of Download
    [[ "$download_success" == true  ]] && [[ -s $local_archive ]] && logexec "openssl dgst -r -md5 $local_archive >> $hashfile"
    [[ "$download_success" == false ]] && logwarn "A download error occurred"
  else
    logwarn "The file $local_archive already exists. Skipping download"
  fi

  if [[ -f $local_archive ]] && ( [[ "$download_success" == true  ]] || [[ $force == true ]] ); then
    # Unpack Downloads
    logecho "Extracting archive $(basename "$local_archive") to $extraction_directory"
    [[ ! -d "$extraction_directory" ]] && logexec mkdir -p $extraction_directory
    logexec tar -xzv -f $local_archive -C $extraction_directory
  fi

  return 0
}

setup_mamba() {
  # checks
  [[ "$bioconda_status" == false ]] && logecho "Installing the latest version of \"mamba\" to the Conda base environment"
  [[ "$bioconda_status" == true  ]] && die "This is a BioConda installation. There is no need to setup Mamba or an environment for ${pipeline}"

  source $conda_base/etc/profile.d/conda.sh
  [[ "$mamba_status" == false ]] && logexec conda install --channel conda-forge mamba
  [[ "$mamba_status" == true ]] && logwarn "Skipping Mamba installation. Mamba is already detected"

  # Create Conda Environment for pipeline
  logecho "Creating the ${pipeline} environment \"${conda_recipe_env_name}\" with the Conda recipe: $conda_recipe"

  set +eu  # workaround: see https://github.com/conda/conda/issues/8186
  [[ -z "${conda_env_path}" ]] && logexec "mamba env create -n ${conda_recipe_env_name} -f $conda_recipe || true"
  [[ -n "${conda_env_path}" && "$force" == true && ! -d "${conda_base}/envs/${conda_env_name}" ]] && logexec "mamba env create -p ${conda_base}/envs/${conda_env_name} -f $conda_recipe || true"  # corner case: there is already a conda env with the same name in another conda_base, e.g. NGSAdmin
  set -eu

  [[ -n "${conda_env_path}" ]] && logwarn "A Conda environment with the name \"$conda_env_name\" is already present. Skipping environment creation" && return 0
  logsuccess "Environment installation completed"
}

download_databases() {
  # Create Subdirectories
  [[ ! -d "$repo_path/download" ]] && logexec mkdir -p $repo_path/download
  hashfile="$repo_path/download/reference_db.md5"

  # Download files
  remote_archive="https://gitlab.bfr.berlin/bfr_bioinformatics/bakcharak_resources/-/raw/main/databases/plasmidblast.tar.gz" # 808M
  local_archive="$repo_path/download/plasmidblast.tar.gz"
  extraction_directory="$repo_path/databases"
  download_file "$remote_archive" "$local_archive" "$hashfile" "$extraction_directory"

  remote_archive="https://zenodo.org/record/4066768/files/db.tar.gz"  # 1.6G
  local_archive="$repo_path/download/platon-db.tar.gz"
  extraction_directory="$repo_path/databases/platon"
  download_file "$remote_archive" "$local_archive" "$hashfile" "$extraction_directory"
  # fix nested directory structure
  [[ -d "$repo_path/databases/platon/db" ]] && mv --verbose --update $repo_path/databases/platon/db/* $repo_path/databases/platon/ && rm -rf $repo_path/databases/platon/db

  remote_archive="https://gitlab.bfr.berlin/bfr_bioinformatics/bakcharak_resources/-/raw/main/databases/mash.tar.gz" # 560M
  local_archive="$repo_path/download/mash.tar.gz"
  extraction_directory="$repo_path/databases"
  download_file "$remote_archive" "$local_archive" "$hashfile" "$extraction_directory"
  
  # Clean up Downloads
  [[ "$arg_keep_dl" == false ]] && [[ -f $hashfile ]] && logexec "cat $hashfile >> $repo_path/databases/$(basename "$hashfile")"

  logsuccess "Database download completed"
}

download_bakta() {
  # Create Subdirectories
  [[ ! -d "$repo_path/download" ]] && logexec mkdir -p $repo_path/download
  hashfile="$repo_path/download/reference_db.md5"

  # Download files
  if [[ "$arg_bakta_db" == "full"  ]]; then
    remote_archive="https://jlubox.uni-giessen.de/dl/fiKeyT1huWv9vW5cXKYkZXYB/db.tar.gz"  # alternative source  33.1G  md5:3200136a0a32b3c33d1cb348ab6b87de
    remote_archive="https://zenodo.org/record/7669534/files/db.tar.gz"  # zenodo source  33.1G  md5:3200136a0a32b3c33d1cb348ab6b87de # DB5
    local_archive="$repo_path/download/bakta-db.tar.gz"
    extraction_directory="$repo_path/databases/bakta"
    download_file "$remote_archive" "$local_archive" "$hashfile" "$extraction_directory"
    [[ -d "$repo_path/databases/bakta/db" ]] && mv --verbose --update $repo_path/databases/bakta/db/* $repo_path/databases/bakta/ && rm -rf $repo_path/databases/bakta/db
  fi

  if [[ "$arg_bakta_db" == "light"  ]]; then
    remote_archive="https://jlubox.uni-giessen.de/dl/fiG6AHmHA94t4v2r2vwW91WB/db-light.tar.gz"  # alternative source  1.3G  md5:a40e680b4aab7871102f31aaac91838b
    remote_archive="https://zenodo.org/record/7669534/files/db-light.tar.gz"  # zenodo source  1.3G  md5:a40e680b4aab7871102f31aaac91838b # DB5
    local_archive="$repo_path/download/bakta-db-light.tar.gz"
    extraction_directory="$repo_path/databases/bakta"
    download_file "$remote_archive" "$local_archive" "$hashfile" "$extraction_directory"
    [[ -d "$repo_path/databases/bakta/db-light" ]] && mv --verbose --update $repo_path/databases/bakta/db-light/* $repo_path/databases/bakta/ && rm -rf $repo_path/databases/bakta/db-light
  fi

  ## update bakta's own copy of the amrfinder db
  # TODO: consider remove target and softlink to env amrfinderplus-db instead to avoid db duplication
  source_conda && conda activate $conda_env_path && logexec amrfinder_update --force_update --database $repo_path/databases/bakta/amrfinderplus-db

  # Clean up Downloads
  [[ "$arg_keep_dl" == false ]] && [[ -f $hashfile ]] && logexec "cat $hashfile >> $repo_path/databases/$(basename "$hashfile")"

  logsuccess "Bakta download completed"
}

update_amrfinder() {
  ## init conda and activate env
  source $conda_base/etc/profile.d/conda.sh
  set +eu && conda activate $conda_env_name && set -eu

  amrfinder_bin=$(which amrfinder)
  [[ -x $(command -v "$amrfinder_bin") ]] || die "Please make sure that the amrfinder software is available: Please activate the conda env ${pipeline,,}"
  logexec amrfinder --update
  logecho "NOTE: if conda does Not install the latest amrfinder version, also the latest amrfinder db cannot be used. The latest version may be installed running conda install again after the bakcharak conda package was created using e.g. conda install -n bakcharak ncbi-amrfinderplus'>=3.12.8'" 
}

symlink_to_ceph_databases() {
  cd "$repo_path/databases" || die "Could not change to $repo_path/databases"
  [[ -d /cephfs/abteilung4/NGS/ReferenceDB/bakcharak/bakta && ! -d $repo_path/databases/bakta ]] && logexec "ln -s /cephfs/abteilung4/NGS/ReferenceDB/bakcharak/bakta bakta"
  [[ -d /cephfs/abteilung4/NGS/ReferenceDB/bakcharak/mash/genomes && ! -f $repo_path/databases/mash/genomes/refseq.genomes.k21s1000.msh ]] && logexec rm $repo_path/databases/mash/genomes/.gitkeep && logexec rmdir $repo_path/databases/mash/genomes
  [[ -d /cephfs/abteilung4/NGS/ReferenceDB/bakcharak/mash/genomes && ! -d $repo_path/databases/mash/genomes ]] && logexec "ln -s /cephfs/abteilung4/NGS/ReferenceDB/bakcharak/mash/genomes mash/genomes"
  [[ -d /cephfs/abteilung4/NGS/ReferenceDB/bakcharak/mash/plasmids && ! -f $repo_path/databases/mash/plasmids/plsdb.msh ]] && logexec rm $repo_path/databases/mash/plasmids/.gitkeep && logexec rmdir $repo_path/databases/mash/plasmids
  [[ -d /cephfs/abteilung4/NGS/ReferenceDB/bakcharak/mash/plasmids && ! -d $repo_path/databases/mash/plasmids ]] && logexec "ln -s /cephfs/abteilung4/NGS/ReferenceDB/bakcharak/mash/plasmids mash/plasmids"
  [[ -d /cephfs/abteilung4/NGS/ReferenceDB/bakcharak/platon && ! -d $repo_path/databases/platon ]] && logexec "ln -s /cephfs/abteilung4/NGS/ReferenceDB/bakcharak/platon platon"
  [[ -d /cephfs/abteilung4/NGS/ReferenceDB/bakcharak/plasmidblast && ! -d $repo_path/databases/plasmidblast ]] && logexec "ln -s /cephfs/abteilung4/NGS/ReferenceDB/bakcharak/plasmidblast plasmidblast"

  logsuccess "Database symbolic linking complete"
}

download_test_data() {
  # Create Subdirectories
  [[ ! -d "$repo_path/download" ]] && logexec mkdir -p $repo_path/download
  [[ ! -d "$repo_path/test_data/assemblies" ]] && logexec mkdir -p $repo_path/test_data/assemblies
  hashfile="$repo_path/download/test_data.md5"

  # Download files
  remote_archive="https://gitlab.bfr.berlin/bfr_bioinformatics/bakcharak_resources/-/raw/main/testdata/assemblies.tar.gz"
  local_archive="$repo_path/download/assemblies.tar.gz"
  extraction_directory="$repo_path/test_data"
  download_file "$remote_archive" "$local_archive" "$hashfile" "$extraction_directory"

  # Create Sample Sheet for Test Data
  [[ "$download_success" == true ]] && logecho "Creating test data sample sheet $repo_path/test_data/assemblies/samples.tsv" 
  [[ "$download_success" == true ]] && bash ${script_path}/create_sampleSheet.sh --force --mode assemblies --fastxDir $extraction_directory/assemblies --outDir $extraction_directory/assemblies ${arg_dryrun:-} && echo

  # Clean up Downloads
  [[ "$arg_keep_dl" == false ]] && [[ -f $hashfile ]] && logexec "cat $hashfile >> $repo_path/test_data/$(basename "$hashfile")"

  logsuccess "Test data download completed"
}

check_success() {
  [[ -d ${conda_env_path:-} ]] && status_conda_env="${green}OK${normal}"
  [[ -f $repo_path/databases/bakta/version.json ]] && status_bakta="${green}OK${normal}"
  [[ -f $repo_path/databases/mash/genomes/refseq.genomes.k21s1000.msh ]] && status_mash_genomes="${green}OK${normal}"
  [[ -f $repo_path/databases/mash/plasmids/plsdb.msh ]] && status_mash_plasmids="${green}OK${normal}"
  [[ -f $repo_path/databases/plasmidblast/plsdb.nhr ]] && status_plasmidblast="${green}OK${normal}"
  [[ -f $repo_path/databases/platon/refseq-plasmids.nhr ]] && status_platon="${green}OK${normal}"
  logheader "Database Status:"
  echo "
status_conda_env:|${status_conda_env:-"${red}FAIL${normal}"}
status_bakta:|${status_bakta:-"${red}FAIL${normal}"}
status_mash_genomes:|${status_mash_genomes:-"${red}FAIL${normal}"}
status_mash_plasmids:|${status_mash_plasmids:-"${red}FAIL${normal}"}
status_plasmidblast:|${status_plasmidblast:-"${red}FAIL${normal}"}
status_platon:|${status_platon:-"${red}FAIL${normal}"}
" | column -t -s "|"
  echo
}

## (3) Main

define_paths
logglobal "[START] $script_real"
logglobal "Arguments: ${*:-"No arguments provided"}"
show_info

## Workflow
check_conda $conda_recipe_env_name
[[ "$interactive" == true ]] && interactive_query
[[ "$arg_mamba" == true ]] && setup_mamba
[[ "$arg_amrfinder" == true ]] && update_amrfinder
[[ "$arg_bakta" == true ]] && download_bakta
[[ "$arg_databases" == true ]] && download_databases
[[ "$arg_ceph_symlinks" == true ]] && symlink_to_ceph_databases
[[ "$arg_test_data" == true ]] && download_test_data
[[ "$arg_keep_dl" == false ]] && cleanup
[[ "$arg_status" == false ]] && check_conda $conda_recipe_env_name  # check changes to conda
show_info
check_success

logglobal "All steps were logged to: $logfile"
logglobal "[STOP] $script_real"
echo "Thank you for installing ${pipeline}"
