# join sistr profiles


head -n 1 results/00-00409/sistr/00-00409_cgmlst-profiles.csv > sistr_cgmlst_profiles.csv
for file in results/*/sistr/*_cgmlst-profiles.csv; do
    tail -n 1 $file >> sistr_cgmlst_profiles.csv
done

sed -i 's/^,/sample,/' sistr_cgmlst_profiles.csv

conda activate grapetree
tail -n +2 sistr_cgmlst_profiles.csv | tr ',' '\t' > sistr_cgmlst_profiles.tsv 

grapetree -p sistr_cgmlst_profiles.tsv -m distance | sed -E 's/[[:space:]]{2,}/ /' | tail -n+2 > sistr_cgmlst_distancematrix.tsv
grapetree -p sistr_cgmlst_profiles.tsv -m MSTreeV2  > sistr_cgmlst_mstree.nwk


sed -i 's/\"//g' summary_all.tsv


tail -n+2 {params.distance_matrix_phylip} > {output.distance_matrix_tsv}
        
{grapetree} -p {input} -m distance --missing 3 | sed -E 's/[[:space:]]{{2,}}/ /'  | sed 's/_trimmed_R//' > {params.distance_matrix_simple_phylip}
tail -n +2 {params.distance_matrix_simple_phylip} > {output.distance_matrix_simple_tsv}
        
{grapetree} -p {input} -m MSTreeV2  | sed 's/_trimmed_R//g' > {output.mstree}
{grapetree} -p {input} -m NJ   | sed 's/_trimmed_R//g' > {output.njtree}




