warning("Manual execution")



workdir <- "/home/carlus/BfR/Projects/Pipeline_test/bakcharak/v2/"

# parameters
sample <- "GMI15-001-DNA"

do.prokka <- FALSE

species <- "Ecoli"


sample_report_plasmidfinder_file = file.path(workdir,"results",sample,"plasmidfinder/report.tsv")
#report_resfinder_file = file.path(workdir, "results",sample,"resfinder/report.tsv") # abricate
sample_report_resfinder_file = file.path(workdir,"results",sample,"amrfinder/results_plus.tsv")
sample_report_vfdb_file = file.path(workdir,"results",sample,"vfdb/report.tsv")
sample_report_mlst_file = file.path(workdir,"results",sample,"mlst/report.tsv")
sample_report_mlstcc_file = file.path(workdir,"results",sample,"mlst/report_clonalcomplex.tsv")
sample_contig_stats_file = file.path(workdir,"results",sample,"stats/contig_stats.tsv")
sample_report_mashplasmid_file = file.path(workdir,"results",sample,"reffinder/mashscreen_plasmid_top.tsv")
sample_report_mashscreengenome_file = file.path(workdir,"results",sample,"reffinder/mashscreen_genome_best.tsv")
sample_report_mashgenome_file = file.path(workdir,"results",sample,"reffinder/mash_genome_best.tsv")
sample_report_plasmidblaster_file = file.path(workdir,"results",sample,"plasmidBlaster/results_stats_info.tsv")

# platon
sample_platon_dir = file.path(workdir,"results",sample,"platon")
platon_contig_classification_file = file.path(workdir,"results",sample,"platon","contig_classification.csv")

# if prokka
sample_report_prokka_file = file.path(workdir,"results",sample,"prokka/{sample}_features.tsv")
# if Salmonella
sample_report_serotype_file = file.path(workdir,"results",sample,"sistr/{sample}_sistr-output.tab")
# if E.coli
sample_report_OH_file = file.path(workdir,"results",sample,"OH/report.tsv")
sample_report_ecoli_vf_file = file.path(workdir,"results",sample,"ecoli_vf/report.tsv")
# if Bacillus
sample_bacillus_sample_summary_file = file.path(workdir,"results",sample,"summary/bacillus_summary.tsv")

#if fastani
sample_report_fastani_file = file.path(workdir,"results",sample,"reffinder/fastani.summary.tsv")



sample_report_resfinder_file <- file.path(workdir,snakemake@input[["report_resfinder"]])
sample_report_plasmidfinder_file <- file.path(workdir,snakemake@input[["report_plasmidfinder"]])
sample_report_vfdb_file <- file.path(workdir,snakemake@input[["report_vfdb"]])
sample_report_mlst_file <- file.path(workdir,snakemake@input[["report_mlst"]])
sample_contig_stats_file  <- file.path(workdir,snakemake@input[["report_contigstats"]])
sample_report_refgenome_file <- file.path(workdir,snakemake@input[["report_mashgenome"]])
sample_report_refgenome_mashscreen_file <- file.path(workdir,snakemake@input[["report_mashscreengenome"]])
sample_report_refplasmid_file <- file.path(workdir,snakemake@input[["report_mashplasmid"]])
sample_plasmidblaster_file  <- file.path(workdir,snakemake@input[["report_plasmidblaster"]])

sample_platon_dir <- file.path(workdir,snakemake@input[["platon_dir"]])
platon_contig_classification_file <- file.path(workdir,snakemake@input[["platon_contig_classification"]])

# optional files for species
if(do.prokka) sample_report_prokka_file  <- file.path(workdir,snakemake@input[["report_prokka"]])

if(species == "Salmonella") sample_report_sistr_file  <- file.path(workdir,snakemake@input[["report_serotype"]])

if(species == "Ecoli") {
  sample_report_OH_file  <- file.path(workdir,snakemake@input[["report_OH"]])
  sample_report_ecoli_vf_file  <- file.path(workdir,snakemake@input[["report_ecoli_vf"]])
}

do_ani <- snakemake@params[["do_ani"]]
if(do_ani){
  sample_report_fastani_file  <- file.path(workdir,snakemake@input[["report_fastani"]])
}

if(species == "Bacillus") {
  sample_bacillus_summary.file <- file.path(workdir,snakemake@input[["bacillus_sample_summary"]])
}

