#!/usr/bin/env python3

# [GOAL] parse bakta summary by treating it as yaml object

import yaml
import os
import csv
import sys

## define data (for testing) >>>>>>>>>>>>
#bakta_summary_in = "/cephfs/abteilung4/Projects_NGS/Pipeline_testdata/BakCharak/bakcharak_2023-11-28_3.0.5_d90d158/results/salmonella6/bakta/salmonella6.txt"
##sample = "salmonella6"
#sample = os.path.basename(os.path.dirname(os.path.dirname(bakta_summary_in))) # TODO read as parameter
#baktadir = os.path.dirname(bakta_summary_in)
#bakta_summary_tsv = os.path.join(os.path.dirname(bakta_summary_in),sample + "_baktasummary.tsv")

#bakta_faa = os.path.join(os.path.dirname(bakta_summary_in),sample + ".faa")
#bakta_ffn = os.path.join(os.path.dirname(bakta_summary_in),sample + ".ffn")
#bakta_tsv = os.path.join(os.path.dirname(bakta_summary_in),sample + ".tsv")
# <<<<<<<<<<<<<<<


# snakemake input and output ---

bakta_summary_in = snakemake.input["summary"]
bakta_tsv = snakemake.input["report"]
bakta_ffn = snakemake.input["ffn"]
bakta_faa = snakemake.input["faa"]
sample = snakemake.params["sample"]
bakta_summary_tsv = snakemake.output["summary"]

baktadir = os.path.dirname(bakta_summary_in)

# test data
if not os.path.isfile(bakta_faa):
    print(f"file {bakta_faa} does NOT exist")
    sys.exit(1)

if not os.path.isfile(bakta_tsv):
    print(f"file {bakta_tsv} does NOT exist")
    sys.exit(1)


#print(f"Input {bakta_summary_in}")
#print(f"Output {bakta_summary_tsv}")


with open(bakta_summary_in,"r") as f:
    dict = yaml.load(f, Loader=yaml.FullLoader)

    # remove empty dic entries
    dict_tmp = {k: v for k, v in dict.items() if v is not None}
    keys2remove= {"DOI","URL"}
    dict_clean = {k: v for k, v in dict_tmp.items() if k not in keys2remove}
    #print(dict_clean)
    # Rename Keys
    dict_clean = {k.replace(' ', '_'): v for k, v in dict_clean.items()}
    # add more elements
    # prepend
    pre_dict = {'sample': sample }
    #print(pre_dict)
    pre_dict.update(dict_clean)
    
    # append
    extended_dict = {
    'bakta_dir': baktadir,
    'path_to_ffn': bakta_ffn,
    'path_to_faa': bakta_faa,
    'path_to_featuretable': bakta_tsv
    }
    #print(extended_dict)
    pre_dict.update(extended_dict)
    final_dictionary = pre_dict


# ----------------------

# better use pandas!
#with open(bakta_tsv,"r") as csvfile:
    #feature_table = csv.reader(csvfile, delimiter='\t') # , quotechar='|'
   
    
# export as tsv

# mandate formatting, column selection and column order
field_names = final_dictionary.keys()
#field_names = list(final_dictionary)
#TODO test if order can be changed, and names can be fixed (for bakta version conflicts)

with open(bakta_summary_tsv, 'w') as csvfile: 
    writer = csv.DictWriter(csvfile, fieldnames = field_names,delimiter ='\t' ) 
    writer.writeheader() 
    writer.writerow(final_dictionary) 


print(f"Output written to {bakta_summary_tsv}")
