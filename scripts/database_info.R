#!/usr/bin/env Rscript

# Goal: Get modification dates and locations of databases

library(dplyr)
library(yaml)

# manual
# OR
#resdb_path <- "/home/DenekeC/anaconda3/envs/bakcharak2/db/ncbi/sequences"
#plasmidfinder_path <- "/home/DenekeC/anaconda3/envs/bakcharak2/db/plasmidfinder/sequences"
#vfdb_path <- "/home/DenekeC/anaconda3/envs/bakcharak2/db/vfdb/sequences"
#CONDA_ENV="/home/DenekeC/anaconda3/envs/bakcharak2" 
#mlstreport.file <- "/cephfs/abteilung4/deneke/Projects/bakcharak_dev/Salmonella/bakcharak_amrfinder_test2/results/11-02165/mlst/report.tsv"
#amrfinder_path <- "/home/DenekeC/anaconda3/envs/bakcharak2/share/amrfinderplus/data/latest"
# platon_path = "/home/DenekeC/Snakefiles/bakcharak/databases/platon"
#outfile <- "log/database_versions.tsv"



if(exists("snakemake")){
  config.yaml <- snakemake@params[["config"]]
  #config.yaml <- snakemake@input[["config"]]
  config = config.yaml
  bakcharak_version <- config$version
  outfile <- snakemake@output[["versions"]]
} else{
  #config.yaml <- "/cephfs/abteilung4/Projects_NGS/Pipeline_testdata/BakCharak/bakcharak_2022-07-01_3.0.1_62d286b/config_bakcharak.yaml"
  config.yaml <- "/cephfs/abteilung4/Projects_NGS/Bacillus/cgmlst_scheme/bakcharak/config_bakcharak.yaml"
  config <- read_yaml(config.yaml)
  bakcharak_version <- config$version
  outfile <- "log/database_versions.tsv"
  
} 

# abricate databases ---

# get_all
# all lists with path and name and mincov and minid

# functions ---------------

test_abricate_db <- function(subelement, verbose=F){
  abricate_paramset <- c("mincov", "minid",  "name",   "path")
  if (verbose) message("subelement: ",subelement)
  if(length(subelement) == 1){
    if (verbose) message("Single element")
      return(F)
  }  
  if (all(abricate_paramset %in% ls(subelement))){
  #if (all(abricate_paramset %in% ls(config$params$vfdb))){
    if (verbose)message("IS abricate_db")
    return(T)
  } else{
    if (verbose)message("NOT abricate_db")
    return(F)
  } 
  
} 

get_abricate_version_info <- function(db_path){
  
  db_fasta <- file.path(db_path,"sequences")
  db_version.yaml <- file.path(db_path,"version.yaml")
  
  if (file.exists(db_version.yaml)){
    #message("version file exists")
    versions_info <- read_yaml(db_version.yaml)
    db_info = data.frame(
      db_name = versions_info$name,
      db_dir = db_path,
      db_version = versions_info$version,
      db_checksum = versions_info$checksum
    )
  } else if(file.exists(db_fasta)){
    warning("version file does NOT exist. Trying from fasta file ",db_fasta)
    db_info = data.frame(
      db_name = basename(dirname(db_fasta)),
      db_dir = db_path,
      db_version = as.character(as.Date(file.info(db_fasta)$mtime)),  #ALT: strsplit(as.character(file.info(db_fasta)$mtime)," ")[[1]][1] 
      db_checksum = tools::md5sum(db_fasta) # alternative digest::digest(file=db_fasta, algo = "md5", serialize = F)
    )
  } else{
    warning("No information available")
    db_info <- data.frame(
      db_name = basename(db_path),
      db_dir = db_path,
      db_version = NA,
      db_checksum = NA
    )
  } 
  
  return(db_info)
}

# test functions
# plasmidfinder ---
# db_path_1 <- "/home/DenekeC/Snakefiles/bakcharak/databases/genefinder/vfdb_bacillus_setB"
# db_path_2 <- file.path(config$params$plasmidfinder$path,config$params$plasmidfinder$name)
# get_abricate_version_info(db_path = db_path_1)
# get_abricate_version_info(db_path = db_path_2)

# end functions

abricate_versions <- do.call(rbind,lapply(config$params, function(param_elem){
  #test_abricate <- test_abricate_db(param_elem)
  if(test_abricate_db(param_elem)){
    db_path_tmp <- file.path(param_elem$path,param_elem$name)
    #message("db_path_tmp: ",db_path_tmp)
    get_abricate_version_info(db_path = db_path_tmp)
  } 
} ))



# mlst ---



if(!dir.exists(config$params$mlst$db)){
  warning("mlst path not valid")
  mlst_version <- data.frame(
    db_name = "pubmlst",
    db_dir = NA,
    db_version = NA,
    db_checksum = NA
  )
} else {
  mlst_files <- list.files(config$params$mlst$db,recursive = T, pattern = ".txt$", full.names = T)
  if(length(mlst_files) == 0){
    warning("no mlst scheme files found")
    mlst_version <- data.frame(
      db_name = "pubmlst",
      db_dir = config$params$mlst$db,
      db_version = NA,
      db_checksum = NA
    )
  } else{
    mlst_version <- data.frame(
      db_name = "pubmlst",
      db_dir = config$params$mlst$db,
      db_version = tail(sort(unique(as.character(as.Date(file.info(mlst_files)$mtime)))),1),
      db_checksum = digest::digest(unname(tools::md5sum(mlst_files)), algo = "md5")
    )
  } 
} 


# amrfinder ---

if(file.exists(file.path(config$params$amrfinder$path,"version.txt"))){
  amrfinder_version <- scan(file.path(config$params$amrfinder$path,"version.txt"),what=character(), sep = "\n", quiet = T)[1] 
} else{
  amrfinder_version <- NA
} 

if(file.exists(file.path(config$params$amrfinder$path,"AMR_CDS"))){
  amrfinder_checksum <- tools::md5sum(file.path(config$params$amrfinder$path,"AMR_CDS"))
} else{
  amrfinder_checksum <- NA
} 

amrfinderdb_info <- data.frame(
  db_name = "ncbi-amrfinder",
  db_dir = config$params$amrfinder$path,
  db_version = amrfinder_version,
  db_checksum = amrfinder_checksum
)

# platon ---

platondir <- config$params$platon$db

if(file.exists(file.path(platondir,"version.yaml"))){
  platonversion_list <- read_yaml(file.path(platondir,"version.yaml"))
  platon_info <- data.frame(
    db_name = "platon",
    db_dir = config$params$platon$db,
    db_version = paste(platonversion_list$version,platonversion_list$source,sep = "__"),
    db_checksum = platonversion_list$checksum
  )
  
} else if(file.exists(file.path(platondir,"version.info"))){
  platonversion_raw <- scan(file.path(platondir,"version.info"), what = character(), sep = "\n", quiet = T)
  platon_info <- data.frame(
    db_name = "platon",
    db_dir = config$params$platon$db,
    db_version = sub("Version ","",paste(platonversion_raw[1:2] ,collapse = "__")),
    db_checksum = platonversion_raw[3] 
  )
} else if(file.exists(file.path(platondir,"mps.tsv"))){
  warning("Version file missing")
  platon_info <- data.frame(
    db_name = "platon",
    db_dir = config$params$platon$db,
    db_version = as.character(as.Date(file.info(file.path(platondir,"mps.tsv"))$mtime)),
    db_checksum = tools::md5sum(file.path(platondir,"mps.tsv"))
  )  
} else{
  warning("Database  files missing")
  platon_info <- data.frame(
    db_name = "platon",
    db_dir = config$params$platon$db,
    db_version = NA,
    db_checksum = NA
  )   
} 






# ani ---



if (config$params$do_ani){
  
  ani_info_default <- data.frame(
    db_name = paste0("ANI:",basename(dirname(config$params$fastani$reference_list))),
    db_dir = config$params$fastani$reference_list,
    db_version = NA,
    db_checksum = NA
  )

  ani_reference_list <- config$params$fastani$reference_list
  if(!file.exists(ani_reference_list)){
    ani_info <- ani_info_default
  } else{
    ani_references <- scan(ani_reference_list, what = character(), sep = "\n", quiet = T)
    ani_references <- ani_references[file.exists(ani_references)] 
    if(length(ani_references) == 0){
      ani_info <- ani_info_default
    } else{
      ani_date <- tail(sort(unique(as.character(as.Date(file.info(ani_references)$mtime)))),1)
      
      ani_info <- data.frame(
        db_name = paste0("ANI_",basename(dirname(config$params$fastani$reference_list))),
        db_dir = config$params$fastani$reference_list,
        db_version = paste0(length(ani_references),"__",ani_date),
        db_checksum = digest::digest(unname(tools::md5sum(ani_references)), algo="md5")
      )
    } 
    
  } 
} # if do_ani

# mash?

# prokka/bakta?

# plasmid blaster?

# compile information ----

database_versions <- dplyr::bind_rows(
  abricate_versions,
  amrfinderdb_info,
  mlst_version,
  platon_info
  )

if(config$params$do_ani){
  database_versions <- dplyr::bind_rows(database_versions,ani_info)  
} 

# export

write.table(database_versions, file = outfile, sep="\t", quote = F, row.names = F)

message("Done parsing out database versions")
