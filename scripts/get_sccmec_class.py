#!/usr/bin/env python3

# [GOAL] Infer SCCmec class from abricate search agaist single gene database
# [Author] Carlus Deneke
# adapted from SCCmecFinder (https://bitbucket.org/genomicepidemiology/sccmecfinder)
# Notes: 
# * extra gene search with mec_database_20171117 (mec-class-C1) not included
# * no subtyping performed:
# if not (MyKmerFinder_hit == 'SCCmec_type_IV(2B)' or MyKmerFinder_hit == 'SCCmec_type_V(5C2&5)' or MyKmerFinder_hit == 'SCCmec_type_V(5C2)'): perform_subtyping = 'No'
# * this script is intended to be used in bakcharak in congunction with abricate gene search

Version="0.0.1"

import csv
import re
import os
from argparse import ArgumentParser
import sys

# argparser

parser = ArgumentParser(description='Extract SCCmec type from abricate gene search in S. aureus')
parser.add_argument("-i", "--input",type=os.path.abspath, dest="abricate_in", default="", help="Path to abricate input file", required=True)
parser.add_argument("-s", "--sample", type=str, dest="sample", default="", help="sample name", required=False)
parser.add_argument("-o","--outfile", type=os.path.abspath, dest="outfile", default="", help="Path to results file", required=True)
args = parser.parse_args()
print('Finding the SCCmec type based on:', args.abricate_in)


# test paths ---
# input file
#abricate_in = "/cephfs/abteilung4/deneke/Projects/bakcharak_dev/Staphyloccocus/sccmec/test/abricate_single_gene/22-ST00789-0.abricate.tsv"
#sample = "22-ST00789-0"
# output file
#outfile = "/cephfs/abteilung4/deneke/Projects/bakcharak_dev/Staphyloccocus/sccmec/test/abricate_single_gene/22-ST00789-0.sccmec_classes.tsv"

# checks

abricate_in = args.abricate_in
sample = args.sample
outfile = args.outfile
outdir = os.path.dirname(outfile)

print()

if not os.path.isfile(args.abricate_in):
    print(f"ERROR: Input path {args.abricate_in} does NOT exist.")
    sys.exit(1)
    
    
if not os.path.exists(outdir):
    print(f"WARNING: Outdir {outdir} does not yet exist. Creating outdir with {os.mkdir(outdir)}")
    os.mkdir(outdir) 


if os.path.exists(args.outfile):
    print(f"WARNING: Output path {args.outfile} already exists! Output will be overwritten")
    #sys.exit(1)

if args.sample == "":
    sample = os.path.basename(args.abricate_in)
    print(f"sample name not specified. Inferring from file name: sample = {sample}")

print()
#print("All checks completed")


# definitions ---------------------

# functions and defintions from https://bitbucket.org/genomicepidemiology/sccmecfinder

definition_SCCmec = {'SCCmec_type_I(1B)': set(['ccrA1', 'ccrB1', 'mecA', 'dmecR1', 'IS1272']), \
             'SCCmec_type_II(2A)': set(['ccrA2', 'ccrB2', 'mecA', 'mecR1', 'mecI']), \
             'SCCmec_type_III(3A)': set(['ccrA3', 'ccrB3', 'mecA', 'mecR1', 'mecI']), \
             'SCCmec_type_IV(2B)': set(['ccrA2', 'ccrB2', 'mecA', 'dmecR1', 'IS1272']), \
             'SCCmec_type_IV(2B&5)': set(['ccrA2', 'ccrB2', 'ccrC11','mecA', 'dmecR1', 'IS1272']), \
             'SCCmec_type_V(5C2)': set(['ccrC11', 'mec-class-C2', 'mecA']), \
             'SCCmec_type_V(5C2&5)': set(['ccrC11', 'ccrC12', 'mec-class-C2', 'mecA']), \
             'SCCmec_type_VI(4B)': set(['ccrA4', 'ccrB4', 'mecA', 'dmecR1', 'IS1272']), \
             'SCCmec_type_VII(5C1)': set(['ccrC11', 'mec-class-C1', 'mecA']), \
             'SCCmec_type_VIII(4A)': set(['ccrA4', 'ccrB4','mecA', 'mecR1', 'mecI']), \
             'SCCmec_type_IX(1C2)': set(['ccrA1', 'ccrB1' ,'mec-class-C2', 'mecA']), \
             'SCCmec_type_X(7C1)': set(['ccrA1', 'ccrB6', 'mec-class-C1', 'mecA']), \
             'SCCmec_type_XI(8E)': set(['ccrA1', 'ccrB3', 'mecC', 'mecR1', 'mecI']), \
             'SCCmec_type_XII(9C2)': set(['ccrC21', 'mec-class-C2', 'mecA']), \
             'SCCmec_type_XIII(9A)': set(['ccrC21', 'mecA', 'dmecR1', 'IS1272'])}

definition_SCCmec_classes = {'SCCmec_type_I(1B)': set(['ccr class 1', 'mec class B']), \
            'SCCmec_type_II(2A)': set(['ccr class 2', 'mec class A']), \
            'SCCmec_type_III(3A)': set(['ccr class 3', 'mec class A']), \
            'SCCmec_type_IV(2B)': set(['ccr class 2', 'mec class B']), \
            'SCCmec_type_IV(2B&5)': set(['ccr class 5', 'ccr class 2', 'mec class B']), \
            'SCCmec_type_V(5C2)': set(['ccr class 5', 'mec class C2']), \
            'SCCmec_type_V(5C2&5)': set(['ccr class 5&5', 'mec class C2']), \
            'SCCmec_type_VI(4B)': set(['ccr class 4', 'mec class B']), \
            'SCCmec_type_VII(5C1)': set(['ccr class 5', 'mec class C1']), \
            'SCCmec_type_VIII(4A)': set(['ccr class 4', 'mec class A']), \
            'SCCmec_type_IX(1C2)': set(['ccr class 1', 'mec class C2']), \
            'SCCmec_type_X(7C1)': set(['ccr class 7', 'mec class C1']), \
            'SCCmec_type_XI(8E)': set(['ccr class 8', 'mec class E']), \
            'SCCmec_type_XII(9C2)': set(['ccr class 9', 'mec class C2']), \
            'SCCmec_type_XIII(9A)': set(['ccr class 9', 'mec class A'])}

def performing_ccr_gene_complex_typing (ccrAB_genes, ccrC_genes, classes):
    if all([gene in ccrAB_genes for gene in ["ccrA1", "ccrB1"]]) == True: classes.append("ccr class 1")
    if all([gene in ccrAB_genes for gene in ["ccrA2", "ccrB2"]]) == True: classes.append("ccr class 2")
    if all([gene in ccrC_genes for gene in ["ccrC11", "ccrC12"]]) == True: classes.append("ccr class 5&5")
    elif all([gene in ccrC_genes for gene in ["ccrC11"]]) == True: classes.append("ccr class 5")
    if all([gene in ccrAB_genes for gene in ["ccrA3", "ccrB3"]]) == True: classes.append("ccr class 3")
    if all([gene in ccrAB_genes for gene in ["ccrA4", "ccrB4"]]) == True: classes.append("ccr class 4")
    if all([gene in ccrAB_genes for gene in ["ccrA5", "ccrB3"]]) == True: classes.append("ccr class 6")
    if all([gene in ccrAB_genes for gene in ["ccrA1", "ccrB6"]]) == True: classes.append("ccr class 7")
    if all([gene in ccrAB_genes for gene in ["ccrA1", "ccrB3"]]) == True: classes.append("ccr class 8")
    if all([gene in ccrC_genes for gene in ["ccrC21"]]) == True: classes.append("ccr class 9")
    return classes

def performing_mec_gene_complex_typing (mec_genes, classes):
    if all ( [gene in mec_genes for gene in ["mecA", "mecR1", "mecI"]]) == True: classes.append("mec class A")
    if all ( [gene in mec_genes for gene in ["mecA", "dmecR1", "IS1272"]]) == True: classes.append("mec class B")
    if all ( [gene in mec_genes for gene in ["mecA", "mec-class-C1"]]) == True: classes.append("mec class C1")
    if all ( [gene in mec_genes for gene in ["mecA", "mec-class-C2"]]) == True: classes.append("mec class C2")    
    if all ( [gene in mec_genes for gene in ['mecC', 'mecR1', 'mecI']]) == True: classes.append("mec class E") 
    return(classes)

def performing_SCCmectyping (classes):
    SCCmectyping = []
    if all([gene in classes for gene in ["ccr class 1", "mec class B"]]) == True: SCCmectyping.append("SCCmec_type_I(1B)")
    if all([gene in classes for gene in ["ccr class 2", "mec class A"]]) == True: SCCmectyping.append("SCCmec_type_II(2A)")
    if all([gene in classes for gene in ["ccr class 3", "mec class A"]]) == True: SCCmectyping.append("SCCmec_type_III(3A)")
    if all([gene in classes for gene in ["ccr class 2", "ccr class 5", "mec class B"]]) == True: SCCmectyping.append("SCCmec_type_IV(2B&5)")
    elif all([gene in classes for gene in ["ccr class 2", "mec class B"]]) == True: SCCmectyping.append("SCCmec_type_IV(2B)")
    if all([gene in classes for gene in ["ccr class 5", "mec class C2"]]) == True: SCCmectyping.append("SCCmec_type_V(5C2)")
    if all([gene in classes for gene in ["ccr class 5&5", "mec class C2"]]) == True: SCCmectyping.append("SCCmec_type_V(5C2&5)")    
    if all([gene in classes for gene in ["ccr class 4", "mec class B"]]) == True: SCCmectyping.append("SCCmec_type_VI(4B)")
    if all([gene in classes for gene in ["ccr class 5", "mec class C1"]]) == True: SCCmectyping.append("SCCmec_type_VII(5C1)")
    if all([gene in classes for gene in ["ccr class 4", "mec class A"]]) == True: SCCmectyping.append("SCCmec_type_VIII(4A)")
    if all([gene in classes for gene in ["ccr class 1", "mec class C2"]]) == True: SCCmectyping.append("SCCmec_type_IX(1C2)")
    if all([gene in classes for gene in ["ccr class 7", "mec class C1"]]) == True: SCCmectyping.append("SCCmec_type_X(7C1)")
    if all([gene in classes for gene in ["ccr class 8", "mec class E"]]) == True: SCCmectyping.append("SCCmec_type_XI(8E)")
    if all([gene in classes for gene in ["ccr class 9", "mec class C2"]]) == True: SCCmectyping.append("SCCmec_type_XII(9C2)")
    if all([gene in classes for gene in ["ccr class 9", "mec class A"]]) == True: SCCmectyping.append("SCCmec_type_XIII(9A)")
    return(SCCmectyping)

# new functions

# Define the file path and the pattern you want to match
#file_path = abricate_in
#pattern_to_match = r'ccr[AB]'  # Replace 'your_pattern' with your desired pattern

def extract_genes_from_table(file_path,pattern_to_match):

    # Create an empty list to store matching values
    matching_genes = []
    matching_genes_alleles = []

    # Open the TSV file and read it
    with open(file_path, 'r', newline='') as tsvfile:
        reader = csv.DictReader(tsvfile, delimiter='\t')
        # Assuming the header of the TSV file contains a "GENE" column
        for row in reader:
            gene_value = row.get("GENE")
            #if gene_value and pattern_to_match in gene_value:
            if gene_value and re.search(pattern_to_match, gene_value):
                # print(f'MATCH: {pattern_to_match} in {gene_value}') # verbose statement
                matching_genes_alleles.append(gene_value)
                # split gene name at first '(' to remove allele number
                gene_value_withoutallele = gene_value.split('(')[0]
                # remove -allele- substring
                gene_value_withoutallele = gene_value_withoutallele.replace('-allele-', '')
                matching_genes.append(gene_value_withoutallele)

    # Now, 'matching_genes' contains the values from the "Gene" column that match your pattern
    #print(matching_genes)
    #print(matching_genes_alleles)
    return(matching_genes,matching_genes_alleles)

def collect_warnings(SCCmectyping):

    if len(SCCmectyping) >= 2: #This will happen when the combination of gene complex can result into mulitple SCCmec element
        print("WARNING: More than one SCCmec type detected: " + str(len(SCCmectyping)) )
        print("This will happen when the combination of gene complex can result into mulitple SCCmec element")
        warning = "WARNING: More than one SCCmec type detected: " + str(len(SCCmectyping))
        return(warning)
    else:
        if SCCmectyping in ['SCCmec_type_III(3A)','SCCmec_type_IV(2B)', 'SCCmec_type_V(5C2&5)', 'SCCmec_type_V(5C2)']:
            print(f"WARNING: SCCmectyp {SCCmectyping} will not be subtyped")
            warning = f"WARNING: SCCmectype {SCCmectyping} will not be subtyped"
            return(warning)
        #else: 
            #print(f"No Subtyping possible for type(s) {SCCmectyping}")

    return("")




# main ---------------------

def main():

    # 0. initialize variables

    # lists
    (classes, SCCmectyping, ccrAB_genes, ccrC_genes, mec_genes) = ([], [], [],[], [])

    # 1. extract genes by group

    [ccrAB_genes,ccrAB_genealleles] = extract_genes_from_table(file_path = abricate_in,pattern_to_match = r'ccr[AB]')
    [ccrC_genes,ccrC_genealleles] = extract_genes_from_table(file_path = abricate_in,pattern_to_match = 'ccrC')
    [mec_genes,mec_genealleles] = extract_genes_from_table(file_path = abricate_in,pattern_to_match = 'mec')

    #print("Genes extracted")

    # 2. classify based on single genes (from https://bitbucket.org/genomicepidemiology/sccmecfinder)

    #print("performing_ccr_gene_complex_typing ...")
    performing_ccr_gene_complex_typing(ccrAB_genes, ccrC_genes,classes = classes)

    #print("performing_mec_gene_complex_typing")
    performing_mec_gene_complex_typing (mec_genes, classes)

    #print("performing_SCCmectyping")
    SCCmectyping = performing_SCCmectyping (classes)

    #print("Typing finished")
    print(f"\nSCCmectyping: {SCCmectyping}\n")

    # 2b. collect warnings
    warning = collect_warnings(SCCmectyping)
    #print(warning)
    

    # 3. return, print, write

    # Create a summary dictionary
    summary = {"sample":sample, 
               "SCCmectype":','.join(sorted(SCCmectyping)),
               "classes":','.join(sorted(classes)),
               "ccrC_genes":','.join(sorted(ccrAB_genealleles + ccrC_genealleles)),
               "mec_genes":','.join(sorted(mec_genealleles)),
               "comment": warning
               }

    print(summary)

    # print to tsv
    with open(outfile, 'w', newline='') as csv_file:
        hdr = summary.keys()
        csvDictR = csv.DictWriter(csv_file, hdr, delimiter='\t', lineterminator='\n')
        csvDictR.writeheader()
        csvDictR.writerow(summary)
        
    print(f"\nSummary written to {outfile}")


    # export as json   
    #import json
    #json_object = json.dumps(summary, indent = 4)  
    #print(json_object) 

    print("\nDone. Thank you for using this service 8-]")


if __name__ == '__main__':
    main()
