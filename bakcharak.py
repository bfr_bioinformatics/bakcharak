#!/usr/bin/env python3

# Python Standard Packages
import argparse
import subprocess
import os
import sys

# Other Packages
from yaml import dump as yaml_dump, safe_load as yaml_load


# %% Functions

def argument_parser(pipeline: str = "bakcharak",
                    repo_path: str = os.path.dirname(os.path.realpath(__file__)),
                    conda_prefix: str = os.environ['CONDA_PREFIX']) -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()
    # region # parse arguments
    # path arguments
    parser.add_argument('-l', '--sample_list', default = "/", type = os.path.abspath, help = '[REQUIRED] List of samples to analyze, as a two column tsv file with columns sample and assembly paths. Can be generated with provided script create_sampleSheet.sh')
    parser.add_argument('-d', '--working_directory', default = "/", type = os.path.abspath, help = '[REQUIRED] Working directory where results are saved')
    parser.add_argument('-s', '--snakefile', default = os.path.join(repo_path, "Snakefile"), type = os.path.abspath, help = f'Path to Snakefile of {pipeline}; default: {os.path.join(repo_path, "Snakefile")}'),
    parser.add_argument('-c', '--config', default = "/", help = 'Path to pre-defined config file (other parameters will be ignored)')
    # docker arguments
    parser.add_argument('--docker', default = "", help = 'Mapped volume path of the host system')
    # mlst arguments
    parser.add_argument('--mlst_scheme', default = "", help = 'Fix MLST scheme instead of auto-detecting scheme; default: ""')
    parser.add_argument('--mlst_db', default = os.path.join(conda_prefix, "db", "pubmlst"), type = os.path.abspath, help = f'Path to custom MLST database; default: {os.path.join(conda_prefix, "db", "pubmlst")}')
    parser.add_argument('--mlst_blastdb', default = os.path.join(conda_prefix, "db", "blast", "mlst.fa"), type = os.path.abspath, help = f'Path to custom MLST database for BLAST; default: {os.path.join(conda_prefix, "db", "blast", "mlst.fa")}')
    parser.add_argument('--cc', default = False, action = 'store_true', help = 'Infer MLST CC from ST')
    # abricate arguments
    parser.add_argument('--minid', default = 80, help = 'Minimum identity in ABRicate; default: 80')
    parser.add_argument('--mincov', default = 50, help = 'Minimum coverage in ABRicate; default: 50')
    parser.add_argument('--abricate_db', default = os.path.join(conda_prefix, "db"), type = os.path.abspath, help = f'Path to ABRicate and prokka databases; default: {os.path.join(conda_prefix, "db")}')
    parser.add_argument('--abricate_duplicationhandling', default = "max", help = 'How to treat genes that occur more than once. sum=sum values for each hit, max=value of best hit, first=value of first gene, all= add all values seperated by semicolon; default: "max"')
    parser.add_argument('--vfdb', default = "NA", type = os.path.abspath, help = 'Path to non-standard vfdb for use in ABRicate; path must contain file "sequences" in ABRicate format')
    # amrfinder arguments
    parser.add_argument('--amrfinder_db', default = os.path.join(conda_prefix, "share/amrfinderplus/data", "latest"), type = os.path.abspath, help = f'Path to AMRFinder; default: {os.path.join(conda_prefix, "share/amrfinderplus/data", "latest")}"')
    parser.add_argument('--phenomatch_db', default = os.path.join(repo_path, "databases/phenotypes/phenotype_amrfinder_matchtable_wPhenotypes.tsv"), type = os.path.abspath,
                        help = f'Path to phenomatch_db; default: {os.path.join(repo_path, "databases/phenotypes/phenotype_amrfinder_matchtable_wPhenotypes.tsv")}')
    parser.add_argument('--match_phenotype', default = False, action = 'store_true', help = 'Match resfinder phenotype to AMRFinder results. Requires a proper phenomatch_db; default: False')
    parser.add_argument('--amrfinder_minid', default = -1, help = 'Minimum identity in AMRFinder, -1 means use a curated threshold if it exists and 0.9 otherwise; default: -1')
    parser.add_argument('--amrfinder_mincov', default = 0.5, help = 'Minimum coverage in AMRFinder; default: 0.5')
    parser.add_argument('--amrfinder_duplicationhandling', default = "max", help = 'How to treat genes that occur more than once. #sum=sum values for each hit, max=value of best hit, first=value of first gene, all= add all values seperated by semicolon; default: "max"')
    # mash arguments
    parser.add_argument('--mash_plasmid_db', default = os.path.join(repo_path, "databases/mash/plasmids/plsdb.msh"), type = os.path.abspath, help = f'Path to mash db; default: {os.path.join(repo_path, "databases/mash/plasmids/plsdb.msh")}')
    parser.add_argument('--mash_genome_db', default = os.path.join(repo_path, "databases/mash/genomes/refseq.genomes.k21s1000.msh"), type = os.path.abspath, help = f'Path to mash db; default: {os.path.join(repo_path, "databases/mash/genomes/refseq.genomes.k21s1000.msh")}')
    parser.add_argument('--mash_genome_info', default = os.path.join(repo_path, "databases/mash/genomes/refseq.genomes.k21s1000.info"), type = os.path.abspath, help = f'Path to mash db; default: {os.path.join(repo_path, "databases/mash/genomes/refseq.genomes.k21s1000.info")}')
    parser.add_argument('--show_bestref', default = False, action = 'store_true', help = 'Display best mash reference genome in report')
    # plasmidblast arguments  # TODO: remove path to plasmidblast db or use path of platon
    parser.add_argument('--plasmidblast_db', default = os.path.join(repo_path, "databases/plasmidblast"), type = os.path.abspath, help = f'Path to location of plasmid blast database: plasmid_db; default: {os.path.join(repo_path, "databases/plasmidblast")}')
    # platon arguments
    parser.add_argument('--platon_db', default = os.path.join(repo_path, "databases/platon"), type = os.path.abspath, help = f'Path to location of platon database for plasmid prediction: plasmid_db; default: {os.path.join(repo_path, "databases/platon")}')
    # parser.add_argument('--platon_mode', default = "accuracy", help='{sensitivity,accuracy,specificity}, -m {sensitivity,accuracy,specificity} applied filter mode: sensitivity: RDS only (>= 95% sensitivity); specificity: RDS only (>=99.9% specificity); accuracy: RDS & characterization heuristics (highest accuracy); default: "accuracy"')
    parser.add_argument('--platon_mode', default = "accuracy", help = ' applied filter mode in platon: sensitivity: RDS only (>= 95 %% sensitivity); specificity: RDS only (>=99.9 %% specificity); accuracy: RDS  characterization heuristics (highest accuracy); default: "accuracy"')
    # fastani arguments
    parser.add_argument('--ani', default = False, action = 'store_true', help = 'Perform species identification with average nucleotide identity. Requires specification of ani_reference')
    parser.add_argument('--ani_reference', default = "NA", help = 'Path to ANI reference file. Each line in the file must contain the full path to a reference fasta file')
    # bakta arguments
    parser.add_argument('--bakta', default = False, action = 'store_true', help = 'Also run bakta')
    parser.add_argument('--bakta_genus', default = "Genus", help = 'Bakta genus option. See bakta --help for details; default: "Genus"')
    parser.add_argument('--bakta_db', default = os.path.join(repo_path, "databases/bakta"), type = os.path.abspath, help = f'Path to location of bakta database for annotation; default: {os.path.join(repo_path, "databases/bakta")}')
    parser.add_argument('--bakta_proteins', default = "/", type = os.path.abspath, help = 'Path to extra protein fasta file for use in bakta')
    parser.add_argument('--bakta_extra', default = "", help = 'Extra, non-standard commands for bakta, e.g. --complete or --compliant. See bakta --help for options. Use with care')
    # bakcharak arguments
    parser.add_argument('--species', default = "Unspecified Bacteria", type = str, help = 'Species or genus name. Special analyses are performed when choosing Salmonella, Ecoli, Bacillus, Staphylococcus. Check the available AMRFinder species with amrfinder -l; default: "Unspecified Bacteria"')
    parser.add_argument('--runname_position', default = 0, type = int, help = 'Position of runname in fasta path. Specify 0 to not export runname; default: 0')
    # parser.add_argument('--ephemeral', default = False, action = 'store_true', help = 'Remove most intermediate data. Analysis results are reduced to JSONs and reports')  # TODO: add mode
    # parser.add_argument('--json', default = False, action = 'store_true', help = 'Only keep annotated genome and JSONs (implies --ephemeral). High-Throughput mode')  # TODO: add mode
    # cpu arguments
    parser.add_argument('-t', '--threads', default = 0, type = int, help = f'Number of Threads/Cores to use. This overrides the "<{pipeline}>/profiles" settings')
    parser.add_argument('-p', '--threads_sample', default = 1, type = int, help = 'Number of Threads to use per sample in multi-threaded rules; default: 1', metavar = 'THREADS')
    parser.add_argument('-b', '--batch', default = "", type = str, help = 'Compute sample list in batches. Please state a fraction string, e.g. 1/3', metavar = 'FRACTION')
    parser.add_argument('-f', '--force', default = False, type = str, help = 'Snakemake force. Force recalculation of output (rule or file) specified here', metavar = 'RULE')
    parser.add_argument('--forceall', default = False, type = str, help = 'Snakemake force. Force recalculation of all steps', metavar = 'RULE')
    parser.add_argument('--profile', default = "none", type = str, help = f'(A) Full path or (B) directory name under "<{pipeline}>/profiles" of a Snakemake config.yaml with Snakemake parameters, e.g. available CPUs and RAM; default: "bfr.hpc"')
    parser.add_argument('-n', '--dryrun', default = False, action = 'store_true', help = 'Snakemake dryrun. Only calculate graph without executing anything')
    parser.add_argument('--unlock', default = False, action = 'store_true', help = 'Unlock a Snakemake execution folder if it had been interrupted')
    parser.add_argument('--logdir', default = "/", type = os.path.abspath, help = 'Path to directory where .snakemake output is to be saved')
    parser.add_argument('-V', '--version', default = False, action = 'store_true', help = 'Print program version')
    # endregion # parse arguments
    return parser


def select_amrfinder_organism(args):
    # init
    amr_command = ""

    # list of AMRFinder species: amrfinder -l --database path/2/amrfinder/latest
    # Acinetobacter_baumannii, Burkholderia_cepacia, Burkholderia_pseudomallei, Campylobacter, Citrobacter_freundii, Clostridioides_difficile, Enterobacter_asburiae, Enterobacter_cloacae,
    # Enterococcus_faecalis, Enterococcus_faecium, Escherichia, Klebsiella_oxytoca, Klebsiella_pneumoniae, Neisseria_gonorrhoeae, Neisseria_meningitidis, Pseudomonas_aeruginosa, Salmonella,
    # Serratia_marcescens, Staphylococcus_aureus, Staphylococcus_pseudintermedius, Streptococcus_agalactiae, Streptococcus_pneumoniae, Streptococcus_pyogenes, Vibrio_cholerae, Vibrio_parahaemolyticus, Vibrio_vulnificus

    available_species = ["Acinetobacter_baumannii",
                         "Campylobacter",
                         "Clostridioides_difficile",
                         "Enterococcus_faecalis",
                         "Enterococcus_faecium",
                         "Escherichia",
                         "Klebsiella_oxytoca",
                         "Klebsiella_pneumoniae",
                         "Salmonella",
                         "Staphylococcus_aureus",
                         "Staphylococcus_pseudintermedius",
                         "Vibrio_cholerae"]

    amr_genus_to_species_inference = {'Ecoli'         : 'Escherichia',
                                      'Shigella'      : 'Escherichia',
                                      'Vibrio'        : 'Vibrio_cholerae',
                                      'Klebsiella'    : 'Klebsiella_pneumoniae',
                                      'Clostridioides': 'Clostridioides_difficile',
                                      'Clostridium'   : 'Clostridioides_difficile',
                                      'Staphylococcus': 'Staphylococcus_aureus'
                                      }

    # matching species name
    if args.species in available_species:
        print(f"You chose a species input for AMRFinder: {args.species}")
        amr_species = args.species
        amr_command = f"-O {args.species}"

    # translated species name
    if args.species in amr_genus_to_species_inference.keys():
        print(f"WARNING: Inferring species {amr_genus_to_species_inference[args.species]} from argument {args.species} in AMRFinder point mutation search")
        print(f"You chose a species input for AMRFinder: {amr_genus_to_species_inference[args.species]}")
        amr_command = f"-O {amr_genus_to_species_inference[args.species]}"

    return amr_command


def select_vfdb(args):
    # default vfdb
    vfdb_type = "default"
    vfdb_name = "vfdb_all_setA"
    vfdb_path = os.path.join(args.repo_path, "databases/genefinder")

    # species-specific vfdb
    vfdb_name = "vfdb_aeromonas_setB" if args.species == "Aeromonas" else vfdb_name
    vfdb_name = "vfdb_bacillus_setB" if args.species == "Bacillus" else vfdb_name
    vfdb_name = "vfdb_clostridium_setB" if args.species == "Clostridium" else vfdb_name
    vfdb_name = "vfdb_klebsiella_setB" if args.species == "Klebsiella" else vfdb_name
    vfdb_name = "vfdb_staphylococcus_setB" if args.species == "Staphylococcus" else vfdb_name
    vfdb_name = "vfdb_vibrio_setB" if args.species == "Vibrio" else vfdb_name
    vfdb_name = "vfdb_yersinia_setB" if args.species == "Yersinia" else vfdb_name
    vfdb_type = "species-specific" if vfdb_name != "vfdb_all_setA" else vfdb_type
    # not used: "vfdb_escherichia_setB"

    # custom vfdb
    vfdb_type = "custom" if os.path.exists(os.path.join(args.vfdb, "sequences")) else vfdb_type
    vfdb_name = os.path.basename(args.vfdb) if os.path.exists(os.path.join(args.vfdb, "sequences")) else vfdb_name
    vfdb_path = os.path.dirname(args.vfdb) if os.path.exists(os.path.join(args.vfdb, "sequences")) else vfdb_path

    print(f"You chose a {vfdb_type} vfdb: {vfdb_name}")

    return vfdb_name, vfdb_path


def evaluate_arguments(args: argparse.Namespace) -> argparse.Namespace:
    # version
    args.version_fixed = open(os.path.join(args.repo_path, 'VERSION'), 'r').readline().strip()
    try:
        args.version_git = subprocess.check_output(["git", "describe", "--always"], stderr = subprocess.DEVNULL, cwd = args.repo_path).decode('ascii').strip()
    except:
        args.version_git = args.version_fixed

    if args.version:
        print(f"{args.pipeline} version\t{args.version_fixed}")
        if not args.version_fixed == args.version_git:
            print(f"commit    version\t{args.version_git.lstrip('v')}")
        sys.exit(0)
    else:
        print(f"You are using {args.pipeline} version {args.version_fixed} ({args.version_git})")

    # check required arguments
    required_paths_to_check = {
        "-d/--working_directory": args.working_directory,
        "-l/--sample_list"      : args.sample_list,
    }
    missing_required_paths = {key: path for key, path in required_paths_to_check.items() if path == '/'}
    if missing_required_paths:
        for key, path in missing_required_paths.items():
            print(f"ERROR: the following argument is required: {key}")
        sys.exit(1)

    # check dependency path - keep in sync with check_config()
    dependency_paths_to_check = {
        "-l/--sample_list"       : args.sample_list,
        "-c/--config"            : args.config,  # defaults to "/", hence, always exists
        "--mash_plasmid_db"      : args.mash_plasmid_db,
        "--mash_genome_db"       : args.mash_genome_db,
        "--mash_genome_info"     : args.mash_genome_info,
        "--platon_db"            : args.platon_db,
        "--amrfinder_db"         : args.amrfinder_db,
        "--mlst_db"              : args.mlst_db,
        "--mlst_blastdb"         : args.mlst_blastdb,
        "--plasmidblast_db:nsq"  : os.path.join(args.plasmidblast_db, "plsdb.nsq"),
        "--plasmidblast_db:table": os.path.join(args.plasmidblast_db, "plsdb_table.tsv"),
        "--ani_reference"        : args.ani_reference if args.ani else "/",
        "--bakta_db"             : args.bakta_db if args.bakta else "/"
    }
    missing_dependency_paths = {key: path for key, path in dependency_paths_to_check.items() if not os.path.exists(path)}
    if missing_dependency_paths:
        for key, path in missing_dependency_paths.items():
            print(f"ERROR: path for argument {key} does not exist: {path}")
            if key == "--amrfinder_db":
                print(f"Make sure amrfinder_db path exists. In doubt run amrfinder -u to get the latest release")
        sys.exit(1)

    # create working directory if it does not exist
    if not os.path.exists(args.working_directory):
        os.makedirs(args.working_directory)

    # set snakemake logdir
    args.logdir = args.working_directory if args.logdir == "/" else args.logdir
    print(f"Snakemake logs are saved in {args.logdir}/.snakemake/log")

    # find snakemake profile: first check relative paths, then absolute paths to make args.profile absolute
    if os.path.exists(os.path.join(args.repo_path, "profiles", args.profile)):
        smk_profile = os.path.join(args.repo_path, "profiles", args.profile)
    elif os.path.exists(os.path.realpath(args.profile)):
        smk_profile = os.path.realpath(args.profile)
    elif os.path.exists(os.path.join(args.repo_path, os.environ.get('SNAKEMAKE_PROFILE', '$'))):
        smk_profile = os.path.join(args.repo_path, os.environ.get('SNAKEMAKE_PROFILE'))
    elif os.path.exists(os.path.realpath(os.environ.get('SNAKEMAKE_PROFILE', '$'))):
        smk_profile = os.path.realpath(os.environ.get('SNAKEMAKE_PROFILE'))
    elif os.path.exists(os.path.join(args.repo_path, "profiles", "bfr.hpc")):
        smk_profile = os.path.join(args.repo_path, "profiles", "bfr.hpc")
    else:
        print(f"ERROR: path to Snakemake config.yaml {args.profile} does not exist")
        sys.exit(1)
    args.profile = smk_profile

    # other checks
    if args.mlst_scheme != "":
        print(f"Using fixed MLST scheme: {args.mlst_scheme}")
        # TODO: check if mlst scheme is valid
        args.mlst_scheme = f"--scheme {args.mlst_scheme}"

    # return modified args
    return args


def check_sample_list(sample_list):
    # Check whether sample list has header
    type1 = "sample\tfq1\tfq2\n"
    type2 = "sample\tassembly\n"
    type3 = "sample\tlong\tshort1\tshort2\n"

    with open(sample_list, 'r') as handle:
        header_line = handle.readline()

    if header_line != type2:
        print(f"ERROR: sample list {sample_list} does not have a proper header. Make sure that first line is tab-separated:\n{type2}")
        sys.exit(1)


def create_config(args: argparse.Namespace) -> dict:
    # species settings
    amrfinder_species = select_amrfinder_organism(args)
    vfdb_name, vfdb_path = select_vfdb(args)

    # region # create config dictionary
    config_dict = {
        'pipeline'   : args.pipeline,
        'version'    : args.version_fixed,
        'git_version': args.version_git,
        'workdir'    : args.working_directory,
        'samples'    : args.sample_list,
        'species'    : args.species,
        'smk_params' : {
            'log_directory': args.logdir,
            'profile'      : args.profile,
            'threads'      : args.threads_sample,
            'batch'        : args.batch,
            'docker'       : args.docker
        },
        'params'     : {
            'do_ani'            : args.ani,
            'do_bakta'          : args.bakta,
            'show_bestreference': args.show_bestref,
            # 'abricate_duplicationhandling': args.abricate_duplicationhandling, # inactive
            'runname_position'  : args.runname_position,
            'mash'              : {'plasmid_reference' : args.mash_plasmid_db,
                                   'genome_reference'  : args.mash_genome_db,
                                   'genome_info'       : args.mash_genome_info,
                                   'identity_threshold': 0.9,
                                   'hash_threshold'    : 300,
                                   'kmersize'          : 21,
                                   'sketchsize'        : 1000
                                   },
            'platon'            : {'db'     : args.platon_db,
                                   'mode'   : args.platon_mode,
                                   'options': "--verbose"
                                   },
            'amrfinder'         : {'path'            : args.amrfinder_db,
                                   'species'         : amrfinder_species,
                                   'minid'           : args.amrfinder_minid,
                                   'mincov'          : args.amrfinder_mincov,
                                   # 'do_match_phenotype': True, # inactive
                                   'phenomatch_db'   : args.phenomatch_db,
                                   'match_phenotype' : args.match_phenotype,
                                   'duplication_rule': args.amrfinder_duplicationhandling
                                   },
            'plasmidfinder'     : {'name'  : "plasmidfinder",
                                   'path'  : args.abricate_db,
                                   'minid' : args.minid,
                                   'mincov': args.mincov,
                                   'type'  : "abricate"
                                   },
            'vfdb'              : {'name'  : vfdb_name,
                                   'path'  : vfdb_path,
                                   'minid' : args.minid,
                                   'mincov': args.mincov,
                                   'type'  : "abricate"
                                   },
            'plasmidblast'      : {'reference'       : os.path.join(args.plasmidblast_db, "plsdb"),
                                   'plasmid_db_table': os.path.join(args.plasmidblast_db, "plsdb_table.tsv"),
                                   'max_target_seqs' : 500,
                                   'max_hsps'        : 500,
                                   'qcov_hsp_perc'   : 20
                                   },
            'mlst'              : {'db'     : args.mlst_db,
                                   'blastdb': args.mlst_blastdb,
                                   'cc'     : args.cc,
                                   'extra'  : args.mlst_scheme
                                   }
        }
    }
    # endregion # create config dictionary

    # region # create species config dictionary
    if args.species == "Bacillus":
        bacillus_dict = {
            'btyper_vf'               : {
                'name'  : "btyper_bacillus_vf",
                'path'  : os.path.join(args.repo_path, "databases/genefinder"),
                'minid' : args.minid,
                'mincov': args.mincov,
                'type'  : "abricate"
            },
            'panC_legacyclades'       : {
                'name'  : "panC_legacyclades",
                'path'  : os.path.join(args.repo_path, "databases/genefinder"),
                'minid' : args.minid,
                'mincov': args.mincov,
                'type'  : "abricate"
            },
            'panC_adjustedgroups'     : {
                'name'  : "panC_adjustedgroups",
                'path'  : os.path.join(args.repo_path, "databases/genefinder"),
                'minid' : args.minid,
                'mincov': args.mincov,
                'type'  : "abricate"
            },
            'mesopsychro'             : {
                'name'  : "mesopsychro",
                'path'  : os.path.join(args.repo_path, "databases/genefinder"),
                'minid' : 80,
                'mincov': 50,
                'type'  : "abricate"
            },
            'fastani_proposed_species': {
                'reference_list': os.path.join(args.repo_path, "databases/ani/Bacillus_btyper3/novelproposedSpecies.txt")
            }  #,
        }
        config_dict['params'].update(bacillus_dict)

        # bakta is a prerequisite for Bacillus
        if not args.bakta or args.bakta_proteins == "/":
            print("Species Bacillus requires --bakta together with --bakta_proteins pesticidal_proteins.faa")
            print(f"Forcing command --bakta together with --bakta_proteins {args.bakta_proteins}")
            pesticidal_proteins_faa = os.path.join(args.repo_path, "databases/protein_db/pesticidal_proteins/pesticidal_proteins.faa")
            args.bakta_proteins = pesticidal_proteins_faa
            args.bakta = True

    if args.species == "Salmonella":
        salmonella_dict = {
            'SPI'                  : {
                'name'  : "SPI",
                'path'  : os.path.join(args.repo_path, "databases/genefinder"),
                'minid' : 30,
                'mincov': 30,
                'type'  : "abricate"
            },
            'Salmonella_subspecies': {
                'mashdb': os.path.join(args.repo_path, "databases/mash/salmonella_subspecies/salmonella.msh"),
            }
        }
        config_dict['params'].update(salmonella_dict)

    if args.species == "Staphylococcus":
        staphylococcus_dict = {
            'saureus_toxin': {
                'name'  : "saureus_toxin",
                'path'  : os.path.join(args.repo_path, "databases/genefinder"),
                'minid' : args.minid,
                'mincov': args.mincov,
                'type'  : "abricate"
            },
            'sccmec'       : {
                'name'  : "sccmec",
                'path'  : os.path.join(args.repo_path, "databases/genefinder"),
                'minid' : 90,
                'mincov': 60,
                'type'  : "abricate"
            },
            'vfdb_extra'   : {
                #'name': "vfdb_saureus_setA",
                #'name': "vfdb_staphylococcus_setA",
                'name'  : "staphylococcal_enterotoxins",
                'path'  : os.path.join(args.repo_path, "databases/genefinder"),
                'minid' : args.minid,
                'mincov': args.mincov,
                'type'  : "abricate"
            },
        }
        config_dict['params'].update(staphylococcus_dict)

    if args.species == "Ecoli":
        ecoli_dict = {
            'OH'                     : {
                'name'  : "ecoh",
                'path'  : args.abricate_db,
                'minid' : args.minid,
                'mincov': args.mincov,
                'type'  : "abricate"
            },
            'ecoli_vf'               : {
                'name'  : "ecoli_vf",  # CGE VFs
                'path'  : args.abricate_db,
                'minid' : args.minid,
                'mincov': args.mincov,
                'type'  : "abricate"
            },
            'stx'                    : {
                'name'  : "stx",  # self-curated
                'path'  : os.path.join(args.repo_path, "databases/genefinder"),
                'minid' : args.minid,
                'mincov': 25,  # set to a fixed low value
                'type'  : "abricate"
            },
            'ecoli_pathotyping_rules': os.path.join(args.repo_path, "databases/pathotyping/ecoli_typing_rules.tsv")
        }
        config_dict['params'].update(ecoli_dict)
    # endregion # create species config dictionary

    # region # create conditional config dictionary
    if args.ani:
        ani_dict = {
            'fastani': {'reference_list': args.ani_reference}
        }
        if args.ani_reference == "NA":
            ani_dict['fastani']['reference_list'] = os.path.join(args.repo_path, "databases/ani", args.species, "referenceTypeStrains.txt")  # TODO: double-check
            print(f"Setting ani_reference to {args.ani_reference}")
        config_dict['params'].update(ani_dict)

    if args.bakta:
        bakta_genus = args.bakta_genus if args.bakta_genus != "Genus" else args.species.split()[0]
        bakta_dict = {
            'bakta': {'genus'   : bakta_genus,
                      'proteins': f"--proteins {args.bakta_proteins}" if not args.bakta_proteins == "/" else "",
                      # 'extra_proteins': args.bakta_proteins, # TODO: add function
                      'extra'   : args.bakta_extra,
                      'db'      : args.bakta_db,
                      'report'  : "False"
                      }  # NOT implemented
        }
        config_dict['params'].update(bakta_dict)
    # endregion # create conditional config dictionary

    # write config dictionary to yaml
    with open(args.config_file, 'w') as outfile:
        yaml_dump(data = config_dict, stream = outfile, default_flow_style = False, sort_keys = False)
    print(f"Config file saved to {args.config_file}")

    return config_dict


def check_config(config: dict) -> bool:
    # check required paths
    required_paths_to_check = {
        "config.workdir"                 : config.get('workdir', ''),
        "config.samples"                 : config.get('samples', ''),
        "config.smk_params.log_directory": config.get('smk_params', {}).get('log_directory', ''),
        "config.smk_params.profile"      : config.get('smk_params', {}).get('profile', '')
    }
    missing_required_paths = {key: path for key, path in required_paths_to_check.items() if not os.path.exists(path)}
    if missing_required_paths:
        print(f"WARNING: You are using an old config structure from v3.1.3 or earlier. Please create a new config_bakcharak.yaml") if not config.get('smk_params', None) else None
        for key, path in missing_required_paths.items():
            print(f"ERROR: the config-defined path does not exist: {key} -> {path}")
        sys.exit(1)

    # check dependency paths - keep in sync with evaluate_arguments()
    dependency_paths_to_check = {
        "config.params.mash.plasmid_reference"       : config.get('params', {}).get('mash', {}).get('plasmid_reference', ''),
        "config.params.mash.genome_reference"        : config.get('params', {}).get('mash', {}).get('genome_reference', ''),
        "config.params.mash.genome_info"             : config.get('params', {}).get('mash', {}).get('genome_info', ''),
        "config.params.platon.db"                    : config.get('params', {}).get('platon', {}).get('db', ''),
        "config.params.amrfinder.path"               : config.get('params', {}).get('amrfinder', {}).get('path', ''),
        "config.params.amrfinder.phenomatch_db"      : config.get('params', {}).get('amrfinder', {}).get('phenomatch_db', ''),
        "config.params.plasmidfinder.path"           : config.get('params', {}).get('plasmidfinder', {}).get('path', ''),
        "config.params.vfdb.path"                    : config.get('params', {}).get('vfdb', {}).get('path', ''),
        "config.params.plasmidblast.reference"       : config.get('params', {}).get('plasmidblast', {}).get('reference', '') + ".nsq",
        "config.params.plasmidblast.plasmid_db_table": config.get('params', {}).get('plasmidblast', {}).get('plasmid_db_table', ''),
        "config.params.mlst.db"                      : config.get('params', {}).get('mlst', {}).get('db', ''),
        "config.params.mlst.blastdb"                 : config.get('params', {}).get('mlst', {}).get('blastdb', ''),
        "config.params.bakta.db"                     : config.get('params', {}).get('bakta', {}).get('db', '') if config.get('params', {}).get('bakta', None) else "/",
    }
    missing_dependency_paths = {key: path for key, path in dependency_paths_to_check.items() if not os.path.exists(path)}
    if missing_dependency_paths:
        for key, path in missing_dependency_paths.items():
            print(f"ERROR: path for argument {key} does not exist: {path}")
            if key == "--amrfinder_db":
                print(f"Make sure amrfinder_db path exists. In doubt run amrfinder -u to get the latest release")
        sys.exit(1)

    return True


def build_snakemake_call(args: argparse.Namespace) -> str:
    # force mode
    force_mode = "--forceall" if args.forceall else ""
    force_mode = "--force" if args.force else force_mode

    # user-specified force target; force_targets are mutually exclusive
    force_target = args.forceall if args.forceall else ""
    force_target = args.force if args.force else force_target

    # all-rule force target, add new all rules here
    force = f"{force_mode} {force_target}"

    # other workflow options
    batch_rule = "summary_report"
    batch = f"--batch {batch_rule}={args.batch}" if args.batch else ""
    threads = "--cores" if args.profile == "none" else ""  # use all available cores, if no profile is specified
    threads = f"--cores {args.threads}" if args.threads > 0 else threads  # override cores by commandline arg if provided
    dryrun = "--quiet rules --dryrun" if args.dryrun else ""
    unlock = "--unlock" if args.unlock else ""

    # create command
    command = f"snakemake --profile {args.profile} --snakefile {args.snakefile} --configfile {args.config_file} " \
              f"--config samples={args.sample_list} " \
              f"{batch} {threads} {dryrun} {unlock} {force}"

    return command


# %% Main

def main():
    # create commandline parser
    parser = argument_parser()

    # parse arguments and create namespace object
    args = parser.parse_args()

    # append settings to args namespace
    args.pipeline = "bakcharak"
    args.repo_path = os.path.dirname(os.path.realpath(__file__))
    args.config_file = os.path.join(args.working_directory, f"config_{args.pipeline.lower()}.yaml") if args.config == "/" else args.config

    if args.config == "/":
        # evaluate arguments
        args = evaluate_arguments(args = args)

        # write a new config file
        config = create_config(args = args)
    else:
        # load config preset
        print(f"Config file preset used from {args.config_file}")
        with open(args.config_file, 'r') as file:
            config = yaml_load(file)

        # update args with config or fall back to defaults
        args.sample_list = config.get('samples')
        args.logdir = config.get('smk_params', {}).get('log_directory', args.logdir)
        args.profile = config.get('smk_params', {}).get('profile', args.profile) if args.profile == "none" else args.profile  # if --profile argument is not provided, i.e. profile is default, then use config profile
        args.threads_sample = config.get('threads_sample', args.threads) if args.threads == 1 else args.threads  # if --threads_sample argument is not provided, i.e. profile is default, then use config threads_sample

        # check config
        check_config(config = config)

    # other checks
    check_sample_list(sample_list = config.get('samples'))

    # run snakemake workflow
    smk_command = build_snakemake_call(args = args)
    print(smk_command)
    subprocess.call(args = smk_command, shell = True, cwd = args.logdir)  # start snakemake from logdir


# %% Main Call

if __name__ == '__main__':
    main()
