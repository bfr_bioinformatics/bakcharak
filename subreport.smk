# snakemake file with bakcharak subreports

import os
import pandas as pd
import shutil
# from snakemake.utils import R
shell.executable("bash")


# TODO: fix links to Details in summary report -> prepend absolute path

# definitions ---------------------------------------------------------------

# Set snakemake main workdir variable
workdir: config["workdir"]
#working_dir = config["workdir"]

#ref_dir = "/cephfs/abteilung4/NGS/Databases_autoqc/Staphylococcus/bakcharak" # TODO read in as parameter
ref_dir = config["refdir"]

#outdir = "/cephfs/abteilung4/Projects_NGS/BakCharac_projects/Staph_subreport"
#subsamplelist = "/cephfs/abteilung4/Projects_NGS/BakCharac_projects/Staph_subreport/subsamplelist.txt"  # TODO read in as parameter
subsamplelist = config["subsamplelist"]
#samples = pd.read_table(config["samples"], index_col="sample")
#samples.index = samples.index.astype('str', copy=False) # in case samples are integers, need to convert them to str

outdir = config["workdir"]
print("working_dir:" + config["workdir"])
print("refdir:" + ref_dir)
print("subsamplelist:" + subsamplelist)

# Rules ----------------------------------------------------------------


rule all:
    input:
        #"/cephfs/abteilung4/Projects_NGS/BakCharac_projects/Staph_subreport/results/reports/summary.html", # hard-coded
        #"{outdir}/results/summary/summary_all.tsv"
        #"/cephfs/abteilung4/Projects_NGS/BakCharac_projects/Staph_subreport/results/summary/summary_all.tsv"
        #outdir + "/test/summary/summary_plasmidfinder_short.tsv",
# mode2
        #outdir + "/results/summary/summary_plasmidfinder_short.tsv",
        #outdir + "/results/reports/summary.html", # htmp report
# mode1
        "results/summary/summary_plasmidfinder_short.tsv",
        "results/reports/summary.html", # htmp report



#rule copy_files:
    #input:
        ## database versions
        #database_versions = "logs/database_versions.tsv",
        ## software versions
        #software_versions = "logs/software_versions.tsv"
    #output: 
        #database_versions = outdir + "/logs/database_versions.tsv",
        ## software versions
        #software_versions = outdir + "/logs/software_versions.tsv"
    #message: "Copy log files to local folder"
    #shell:
        #"""
        #cp {input.database_versions} {output.database_versions}
        #cp {input.software_versions} {output.software_versions}
        #"""

#rule simulate_sub_reporting:
    #input: 
        ##{workdir}
        #subsamplelist = {subsamplelist}
    #output: 
        ##{outdir}
        ##"{outdir}/results/summary/summary_all.tsv"
        #summary_plasmidfinder = outdir + "/test/summary/summary_plasmidfinder_short.tsv"
    #shell:
        #"cp {input} {output.summary_plasmidfinder}"    


# mode 1: reference dir is ref_dir workdir is new target

rule subreporting:
    input: 
        #{workdir}
        subsamplelist = {subsamplelist}
    output: 
        #{outdir}
        #"{outdir}results/summary/summary_all.tsv"
        summary_plasmidfinder = "results/summary/summary_plasmidfinder_short.tsv",
        summary_resfinder = "results/summary/summary_amrfinder.tsv", # amrfinder
        summary_mobile_amr = "results/summary/summary_mobile_amrgenes.tsv",
        summary_vfdb = "results/summary/summary_vfdb_short.tsv",
        summary_mlst = "results/summary/summary_mlst_short.tsv",
        report_plasmidfinder = "results/summary/summary_plasmidfinder.tsv",
        report_resfinder = "results/summary/summary_amrfinder_genecoverage.tsv", # amrfinder
        report_vfdb = "results/summary/summary_vfdb.tsv",
        report_mlst = "results/summary/summary_mlst.tsv",
        summary_mash = "results/summary/summary_bestrefererence.tsv",
        # platon
        summary_platon = "results/summary/summary_platon.tsv",
        # if Salmonella
        summary_serotype = "results/summary/summary_sistr_short.tsv" if config["species"] == "Salmonella" else "dummyresults/summary/summary_sistr_short.tsv",
        report_serotype = "results/summary/summary_sistr.tsv" if config["species"] == "Salmonella" else "dummyresults/summary/summary_sistr.tsv",
        # if E.coli
        summary_OH = "results/summary/summary_OH_short.tsv" if config["species"] == "Ecoli" else "dummyresults/summary/summary_OH_short.tsv",
        summary_ecoli_vf = "results/summary/summary_ecoli_vf_short.tsv" if config["species"] == "Ecoli" else "dummyresults/summary/summary_ecoli_vf_short.tsv",
        report_OH = "results/summary/summary_OH.tsv" if config["species"] == "Ecoli" else "dummyresults/summary/summary_OH.tsv",
        report_ecoli_vf = "results/summary/summary_ecoli_vf.tsv" if config["species"] == "Ecoli" else "dummyresults/summary/summary_ecoli_vf.tsv",
        # if Bacillus
        report_btyper_vf = "results/summary/summary_btyper_vf.tsv" if config["species"] == "Bacillus" else "dummyresults/summary/summary_btyper_vf.tsv",
        summary_btyper_vf = "results/summary/summary_btyper_vf_short.tsv" if config["species"] == "Bacillus" else "dummyresults/summary/summary_btyper_vf_short.tsv",
        bacillus_summary = "results/summary/bacillus_summary.tsv" if config["species"] == "Bacillus" else "dummyresults/summary/bacillus_summary.tsv",
        # if Staphylococcus
        #report_saureus_setB = "results/summary/summary_vfdb_saureus_setB.tsv" if config["species"] == "Staphylococcus" else "dummyresults/summary_vfdb_saureus_setB.tsv",
        report_saureus_sccmec = "results/summary/summary_sccmec.tsv" if config["species"] == "Staphylococcus" else "dummyresults/summary_sccmec.tsv",
        report_saureus_toxin = "results/summary/summary_saureus_toxin.tsv" if config["species"] == "Staphylococcus" else "dummyresults/summary_saureus_toxin.tsv",
        # if CC
        summary_mlstcc = "results/summary/summary_mlstCC.tsv" if config["params"]["mlst"]["cc"] == True else "dummyresults/summary_mlstCC.tsv",
        # if fastani
        summary_fastani = "results/summary/summary_fastani.tsv" if config["params"]["do_ani"] == True else "dummyresults/summary_fastani.tsv",
    message: "Subreporting data"
    log: "logs/subreporting.log"
    params:
        force = "--force",
        outdir = {outdir},
        workdir = {ref_dir}, #workdir, # need to define
        subsamplelist = {subsamplelist},
    shell:
        """
        echo "{workflow.basedir}/scripts/sub_reportdata.R -d {params.workdir} -l {input.subsamplelist} -o {params.outdir} {params.force}"
        {workflow.basedir}/scripts/sub_reportdata.R -d {params.workdir} -l {input.subsamplelist} -o {params.outdir} {params.force}
        echo "touch {output.summary_serotype} {output.report_serotype} {output.summary_OH} {output.summary_ecoli_vf} {output.report_OH} {output.report_ecoli_vf} {output.report_btyper_vf} {output.summary_btyper_vf} {output.bacillus_summary} {output.report_saureus_sccmec} {output.report_saureus_toxin} {output.summary_mlstcc} {output.summary_fastani}"
        touch {output.summary_serotype} {output.report_serotype} {output.summary_OH} {output.summary_ecoli_vf} {output.report_OH} {output.report_ecoli_vf} {output.report_btyper_vf} {output.summary_btyper_vf} {output.bacillus_summary} {output.report_saureus_sccmec} {output.report_saureus_toxin} {output.summary_mlstcc} {output.summary_fastani}
        """

#/home/DenekeC/Snakefiles/bakcharak/scripts/sub_reportdata.R -d {params.workdir} -l {input.subsamplelist} -o {params.outdir} {params.force}

#rule remove_dummyresults:
    #input: "results/reports/summary.html"
    #output:
    #message: "Removing dummy results folder"
    #shell:
        #"rm -r dummyresults"
        

onsuccess:
    shell('rm -rf dummyresults')


#onstart:
#    shell('echo -e "running\t`date +%Y-%m-%d" "%H:%M`" > pipeline_status.txt')

#onsuccess:
#    shell('echo -e "success\t`date +%Y-%m-%d" "%H:%M`" > pipeline_status.txt')

#onerror:
#    shell('echo -e "error\t`date +%Y-%m-%d" "%H:%M`" > pipeline_status.txt'),
#    print("An error occurred")


#"dummyresults/summary/summary_sistr_short.tsv"
#"dummyresults/summary/summary_sistr.tsv"
#"dummyresults/summary/summary_OH_short.tsv"
#"dummyresults/summary/summary_ecoli_vf_short.tsv"
#"dummyresults/summary/summary_OH.tsv"
#"dummyresults/summary/summary_ecoli_vf.tsv"
#"dummyresults/summary/summary_btyper_vf.tsv"
#"dummyresults/summary/summary_btyper_vf_short.tsv"
#"dummyresults/summary/bacillus_summary.tsv"
#"dummyresults/summary_sccmec.tsv"
#"dummyresults/summary_saureus_toxin.tsv"

# version that changes workdir
# TODO need to remove workdir from file.path in summary_report.Rmd
rule summary_subreport:
    input:
        workdir = config["workdir"],
        summary_plasmidfinder = "results/summary/summary_plasmidfinder_short.tsv",
        summary_resfinder = "results/summary/summary_amrfinder.tsv", # amrfinder
        summary_mobile_amr = "results/summary/summary_mobile_amrgenes.tsv",
        summary_vfdb = "results/summary/summary_vfdb_short.tsv",
        summary_mlst = "results/summary/summary_mlst_short.tsv",
        report_plasmidfinder = "results/summary/summary_plasmidfinder.tsv",
        report_resfinder = "results/summary/summary_amrfinder_genecoverage.tsv", # amrfinder
        report_vfdb = "results/summary/summary_vfdb.tsv",
        report_mlst = "results/summary/summary_mlst.tsv",
        summary_mash = "results/summary/summary_bestrefererence.tsv",
        # platon
        summary_platon = "results/summary/summary_platon.tsv",
        # if Salmonella
        summary_serotype = "results/summary/summary_sistr_short.tsv" if config["species"] == "Salmonella" else [],
        report_serotype = "results/summary/summary_sistr.tsv" if config["species"] == "Salmonella" else [],
        # if E.coli
        summary_OH = "results/summary/summary_OH_short.tsv" if config["species"] == "Ecoli" else [],
        summary_ecoli_vf = "results/summary/summary_ecoli_vf_short.tsv" if config["species"] == "Ecoli" else [],
        report_OH = "results/summary/summary_OH.tsv" if config["species"] == "Ecoli" else [],
        report_ecoli_vf = "results/summary/summary_ecoli_vf.tsv" if config["species"] == "Ecoli" else [],
        # if Bacillus
        report_btyper_vf = "results/summary/summary_btyper_vf.tsv" if config["species"] == "Bacillus" else [],
        summary_btyper_vf = "results/summary/summary_btyper_vf_short.tsv" if config["species"] == "Bacillus" else [],
        bacillus_summary = "results/summary/bacillus_summary.tsv" if config["species"] == "Bacillus" else [],
        # if Staphylococcus
        #report_saureus_setB = "results/summary/summary_vfdb_saureus_setB.tsv" if config["species"] == "Staphylococcus" else [],
        report_saureus_sccmec = "results/summary/summary_sccmec.tsv" if config["species"] == "Staphylococcus" else [],
        report_saureus_toxin = "results/summary/summary_saureus_toxin.tsv" if config["species"] == "Staphylococcus" else [],
        # if CC
        summary_mlstcc = "results/summary/summary_mlstCC.tsv" if config["params"]["mlst"]["cc"] == True else [],
        #summary_mlstcc = "results/summary/summary_mlstCC.tsv" if config["params"]["mlst"]["cc"] == True else [],
        # if fastani
        summary_fastani = "results/summary/summary_fastani.tsv" if config["params"]["do_ani"] == True else [],
        # database versions
        database_versions = ref_dir + "/logs/database_versions.tsv",
        # software versions
        software_versions = ref_dir + "/logs/software_versions.tsv"
    output:
        html = "results/reports/summary.html", # with parameter
    message: "Creating Rmarkdown summary subreport"
    log: "logs/summary_report.log"
    conda: "../envs/rmarkdown_env.yaml"
    params:
        species = config["species"],
        samplelist = config["samples"],
        heatmaps = config["params"]["plots"],
        stats = "results/summary/summary_all.tsv",
        do_ani = config["params"]["do_ani"],
        do_cc = config["params"]["mlst"]["cc"],
        show_bestreference = config ["params"]["show_bestreference"],
        runname_position = config["params"]["runname_position"],
    script:
        "scripts/summary_report.Rmd"


# -------------------------------------------------------------------------------------------------------------------------
# mode 2: original bakcharak is working dir
#rule subreporting:
    #input: 
        ##{workdir}
        #subsamplelist = {subsamplelist}
    #output: 
        ##{outdir}
        ##"{outdir}/results/summary/summary_all.tsv"
        #summary_plasmidfinder = outdir + "/results/summary/summary_plasmidfinder_short.tsv",
        #summary_resfinder = outdir + "/results/summary/summary_amrfinder.tsv", # amrfinder
        #summary_mobile_amr = outdir + "/results/summary/summary_mobile_amrgenes.tsv",
        #summary_vfdb = outdir + "/results/summary/summary_vfdb_short.tsv",
        #summary_mlst = outdir + "/results/summary/summary_mlst_short.tsv",
        #report_plasmidfinder = outdir + "/results/summary/summary_plasmidfinder.tsv",
        #report_resfinder = outdir + "/results/summary/summary_amrfinder_genecoverage.tsv", # amrfinder
        #report_vfdb = outdir + "/results/summary/summary_vfdb.tsv",
        #report_mlst = outdir + "/results/summary/summary_mlst.tsv",
        #summary_mash = outdir + "/results/summary/summary_bestrefererence.tsv",
        ## platon
        #summary_platon = outdir + "/results/summary/summary_platon.tsv",
        ## if Salmonella
        #summary_serotype = outdir + "/results/summary/summary_sistr_short.tsv" if config["species"] == "Salmonella" else outdir + "/dummyresults/summary/summary_sistr_short.tsv",
        #report_serotype = outdir + "/results/summary/summary_sistr.tsv" if config["species"] == "Salmonella" else outdir + "/dummyresults/summary/summary_sistr.tsv",
        ## if E.coli
        #summary_OH = outdir + "/results/summary/summary_OH_short.tsv" if config["species"] == "Ecoli" else outdir + "/dummyresults/summary/summary_OH_short.tsv",
        #summary_ecoli_vf = outdir + "/results/summary/summary_ecoli_vf_short.tsv" if config["species"] == "Ecoli" else outdir + "/dummyresults/summary/summary_ecoli_vf_short.tsv",
        #report_OH = outdir + "/results/summary/summary_OH.tsv" if config["species"] == "Ecoli" else outdir + "/dummyresults/summary/summary_OH.tsv",
        #report_ecoli_vf = outdir + "/results/summary/summary_ecoli_vf.tsv" if config["species"] == "Ecoli" else outdir + "/dummyresults/summary/summary_ecoli_vf.tsv",
        ## if Bacillus
        #report_btyper_vf = outdir + "/results/summary/summary_btyper_vf.tsv" if config["species"] == "Bacillus" else outdir + "/dummyresults/summary/summary_btyper_vf.tsv",
        #summary_btyper_vf = outdir + "/results/summary/summary_btyper_vf_short.tsv" if config["species"] == "Bacillus" else outdir + "/dummyresults/summary/summary_btyper_vf_short.tsv",
        #bacillus_summary = outdir + "/results/summary/bacillus_summary.tsv" if config["species"] == "Bacillus" else outdir + "/dummyresults/summary/bacillus_summary.tsv",
        ## if Staphylococcus
        #report_saureus_setB = outdir + "/results/summary/summary_vfdb_saureus_setB.tsv" if config["species"] == "Staphylococcus" else outdir + "/dummyresults/.dummy",
        #report_saureus_sccmec = outdir + "/results/summary/summary_sccmec.tsv" if config["species"] == "Staphylococcus" else outdir + "/dummyresults/.dummy",
        #report_saureus_toxin = outdir + "/results/summary/summary_saureus_toxin.tsv" if config["species"] == "Staphylococcus" else outdir + "/dummyresults/.dummy",
        ## if CC
        #summary_mlstcc = outdir + "/results/summary/summary_mlstCC.tsv" if config["params"]["mlst"]["cc"] == True else [],
        ## if fastani
        #summary_fastani = outdir + "/results/summary/summary_fastani.tsv" if config["params"]["do_ani"] == True else [],
    #message: "Subreporting data"
    ##log: "logs/subreporting.log"
    #params:
        #force = "--force",
        #outdir = {working_dir},
        #workdir = {ref_dir}, #workdir, # need to define
        #subsamplelist = {subsamplelist}
    #shell:
        #"""
        #echo "scripts/sub_reportdata.R -d {params.workdir} -l {input.subsamplelist} -o {params.outdir} {params.force}"
        #/home/DenekeC/Snakefiles/bakcharak/scripts/sub_reportdata.R -d {params.workdir} -l {input.subsamplelist} -o {params.outdir} {params.force}
        #"""
# scripts/sub_reportdata.R -d /cephfs/abteilung4/NGS/Databases_autoqc/Staphylococcus/bakcharak -l /cephfs/abteilung4/Projects_NGS/BakCharac_projects/Staph_subreport/subsamplelist.txt -o /cephfs/abteilung4/Projects_NGS/BakCharac_projects/Staph_subreport 


# mode 2
#rule summary_subreport:
    #input:
        ##workdir = [], # empty
        #workdir = outdir, # empty
        #summary_plasmidfinder = outdir + "/results/summary/summary_plasmidfinder_short.tsv",
        #summary_resfinder = outdir + "/results/summary/summary_amrfinder.tsv", # amrfinder
        #summary_mobile_amr = outdir + "/results/summary/summary_mobile_amrgenes.tsv",
        #summary_vfdb = outdir + "/results/summary/summary_vfdb_short.tsv",
        #summary_mlst = outdir + "/results/summary/summary_mlst_short.tsv",
        #report_plasmidfinder = outdir + "/results/summary/summary_plasmidfinder.tsv",
        #report_resfinder = outdir + "/results/summary/summary_amrfinder_genecoverage.tsv", # amrfinder
        #report_vfdb = outdir + "/results/summary/summary_vfdb.tsv",
        #report_mlst = outdir + "/results/summary/summary_mlst.tsv",
        #summary_mash = outdir + "/results/summary/summary_bestrefererence.tsv",
        ## platon
        #summary_platon = outdir + "/results/summary/summary_platon.tsv",
        ## if Salmonella
        #summary_serotype = outdir + "/results/summary/summary_sistr_short.tsv" if config["species"] == "Salmonella" else [],
        #report_serotype = outdir + "/results/summary/summary_sistr.tsv" if config["species"] == "Salmonella" else [],
        ## if E.coli
        #summary_OH = outdir + "/results/summary/summary_OH_short.tsv" if config["species"] == "Ecoli" else [],
        #summary_ecoli_vf = outdir + "/results/summary/summary_ecoli_vf_short.tsv" if config["species"] == "Ecoli" else [],
        #report_OH = outdir + "/results/summary/summary_OH.tsv" if config["species"] == "Ecoli" else [],
        #report_ecoli_vf = outdir + "/results/summary/summary_ecoli_vf.tsv" if config["species"] == "Ecoli" else [],
        ## if Bacillus
        #report_btyper_vf = outdir + "/results/summary/summary_btyper_vf.tsv" if config["species"] == "Bacillus" else [],
        #summary_btyper_vf = outdir + "/results/summary/summary_btyper_vf_short.tsv" if config["species"] == "Bacillus" else [],
        #bacillus_summary = outdir + "/results/summary/bacillus_summary.tsv" if config["species"] == "Bacillus" else [],
        ## if Staphylococcus
        #report_saureus_setB = outdir + "/results/summary/summary_vfdb_saureus_setB.tsv" if config["species"] == "Staphylococcus" else [],
        #report_saureus_sccmec = outdir + "/results/summary/summary_sccmec.tsv" if config["species"] == "Staphylococcus" else [],
        #report_saureus_toxin = outdir + "/results/summary/summary_saureus_toxin.tsv" if config["species"] == "Staphylococcus" else [],
        ## if CC
        #summary_mlstcc = outdir + "/results/summary/summary_mlstCC.tsv" if config["params"]["mlst"]["cc"] == True else [],
        ##summary_mlstcc = "results/summary/summary_mlstCC.tsv" if config["params"]["mlst"]["cc"] == True else [],
        ## if fastani
        #summary_fastani = outdir + "/results/summary/summary_fastani.tsv" if config["params"]["do_ani"] == True else [],
        ## database versions
        #database_versions = outdir + "/logs/database_versions.tsv",
        ## software versions
        #software_versions = outdir + "/logs/software_versions.tsv"
        ##database_versions = "logs/database_versions.tsv",
        ### software versions
        ##software_versions = "logs/software_versions.tsv"
    #output:
        ##html = "results/reports/summary.html", # original
        ##html = "/cephfs/abteilung4/Projects_NGS/BakCharac_projects/Staph_subreport/results/reports/summary.html", # head-coded
        ##html = outdir + "/results/reports/summary.html", # with parameter
        #html = outdir + "/results/reports/summary.html", # with parameter
    #message: "Creating Rmarkdown summary subreport"
    #log: outdir + "logs/summary_report.log"
    #conda: "../envs/rmarkdown_env.yaml"
    #params:
        #species = config["species"],
        #heatmaps = config["params"]["plots"],
        ##stats = outdir + "/results/summary/summary_all.tsv",
        #do_ani = config["params"]["do_ani"],
        #do_cc = config["params"]["mlst"]["cc"],
    #script:
        #"scripts/summary_report.Rmd"


# ------------
# other
#rule summary_subreport:
    #input:
        #workdir = config["workdir"],
        #summary_plasmidfinder = outdir + "/results/summary/summary_plasmidfinder_short.tsv",
        #summary_resfinder = outdir + "/results/summary/summary_amrfinder.tsv", # amrfinder
        #summary_mobile_amr = outdir + "/results/summary/summary_mobile_amrgenes.tsv",
        #summary_vfdb = outdir + "/results/summary/summary_vfdb_short.tsv",
        #summary_mlst = outdir + "/results/summary/summary_mlst_short.tsv",
        #report_plasmidfinder = outdir + "/results/summary/summary_plasmidfinder.tsv",
        #report_resfinder = outdir + "/results/summary/summary_amrfinder_genecoverage.tsv", # amrfinder
        #report_vfdb = outdir + "/results/summary/summary_vfdb.tsv",
        #report_mlst = outdir + "/results/summary/summary_mlst.tsv",
        #summary_mash = outdir + "/results/summary/summary_bestrefererence.tsv",
        ## platon
        #summary_platon = outdir + "/results/summary/summary_platon.tsv",
        ## if Salmonella
        #summary_serotype = outdir + "/results/summary/summary_sistr_short.tsv" if config["species"] == "Salmonella" else [],
        #report_serotype = outdir + "/results/summary/summary_sistr.tsv" if config["species"] == "Salmonella" else [],
        ## if E.coli
        #summary_OH = outdir + "/results/summary/summary_OH_short.tsv" if config["species"] == "Ecoli" else [],
        #summary_ecoli_vf = outdir + "/results/summary/summary_ecoli_vf_short.tsv" if config["species"] == "Ecoli" else [],
        #report_OH = outdir + "/results/summary/summary_OH.tsv" if config["species"] == "Ecoli" else [],
        #report_ecoli_vf = outdir + "/results/summary/summary_ecoli_vf.tsv" if config["species"] == "Ecoli" else [],
        ## if Bacillus
        #report_btyper_vf = outdir + "/results/summary/summary_btyper_vf.tsv" if config["species"] == "Bacillus" else [],
        #summary_btyper_vf = outdir + "/results/summary/summary_btyper_vf_short.tsv" if config["species"] == "Bacillus" else [],
        #bacillus_summary = outdir + "/results/summary/bacillus_summary.tsv" if config["species"] == "Bacillus" else [],
        ## if Staphylococcus
        #report_saureus_setB = outdir + "/results/summary/summary_vfdb_saureus_setB.tsv" if config["species"] == "Staphylococcus" else [],
        #report_saureus_sccmec = outdir + "/results/summary/summary_sccmec.tsv" if config["species"] == "Staphylococcus" else [],
        #report_saureus_toxin = outdir + "/results/summary/summary_saureus_toxin.tsv" if config["species"] == "Staphylococcus" else [],
        ## if CC
        #summary_mlstcc = outdir + "/results/summary/summary_mlstCC.tsv" if config["params"]["mlst"]["cc"] == True else [],
        ## if fastani
        #summary_fastani = outdir + "/results/summary/summary_fastani.tsv" if config["params"]["do_ani"] == True else [],
        ## database versions
        #database_versions = outdir + "/logs/database_versions.tsv",
        ## software versions
        #software_versions = outdir + "/logs/software_versions.tsv"
        ##database_versions = "logs/database_versions.tsv",
        ### software versions
        ##software_versions = "logs/software_versions.tsv"
    #output:
        ##html = "results/reports/summary.html", # original
        ##html = "/cephfs/abteilung4/Projects_NGS/BakCharac_projects/Staph_subreport/results/reports/summary.html", # head-coded
        ##html = outdir + "/results/reports/summary.html", # with parameter
        #html = outdir + "/results/reports/summary.html", # with parameter
    #message: "Creating Rmarkdown summary subreport"
    #log: outdir + "logs/summary_report.log"
    #conda: "../envs/rmarkdown_env.yaml"
    #params:
        #species = config["species"],
        #heatmaps = config["params"]["plots"],
        ##stats = outdir + "/results/summary/summary_all.tsv",
        #do_ani = config["params"]["do_ani"],
        #do_cc = config["params"]["mlst"]["cc"],
    #script:
        #"scripts/summary_report.Rmd"

