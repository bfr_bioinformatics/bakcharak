#!/usr/bin/env python3

import argparse
import subprocess
import os
import sys

version = 2.0




def main():
    
   
    #find out path of abricate
#    conda_path = os.environ['CONDA_PREFIX']
#    db_dir = os.path.join(conda_path,"db")
    
    # find out repo path
#    repo_path = os.path.dirname(os.path.realpath(__file__))

    
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--subsample_list', help='List of samples to subset, as a text file with the sample name in the first column', required=True, type=os.path.abspath)
    parser.add_argument('-o', '--outdir', help='Directory where results are saved', required=True, type=os.path.abspath)
    parser.add_argument('-b', '--bakcharakdir', help='Directory where bakcharak results are saved', required=True, type=os.path.abspath)


    parser.add_argument('-t', '--threads', help='Number of Threads to use. Note that samples can only be processed sequentially due to the required database access. However the allele calling can be executed in parallel for the different loci, default = 10', default=10, required=False)
    parser.add_argument('-n', '--dryrun', help='Snakemake dryrun. Only calculate graph without executing anything', default=False, action='store_true',  required=False)

    #parser.add_argument('--forceall', help='Snakemake force. Force recalculation of all steps', default=False, action='store_true',  required=False)
    parser.add_argument('--unlock', help='unlock snakemake', default=False, action='store_true',  required=False)
    parser.add_argument('--logdir', help='Path to directory whete .snakemake output is to be saved', default='NA', required=False)



    #parser.add_argument('-c', '--config', help='Path to pre-defined config file (Other parameters will be ignored)', default="", required=True)
    
    args = parser.parse_args()
    
    # define configfile
    configfile = os.path.join(args.bakcharakdir, "config_bakcharak.yaml")
    
    # define snakefile
    snakefile = os.path.join(os.path.dirname(os.path.realpath(__file__)),"subreport.smk")
    
    

    if args.unlock:
        unlock = "--unlock"    
    else:
        unlock = ""   


    if args.dryrun:
        dryrun = "-n"
    else:
        dryrun = ""

    #if args.forceall:
        #forceall = "--forceall"
    #else:
        #forceall = ""

    forceall = "--forceall"

    # check existence
    if not os.path.exists(configfile):
        print("ERROR: configfile " + configfile + " could NOT be found")
        sys.exit(1)

    if not os.path.exists(args.subsample_list):
        print("ERROR: subsample_list " + args.subsample_list + " could NOT be found")
        sys.exit(1)
        
        
    # run snakemake
    call = "snakemake --keep-going -p --configfile {configfile} --config workdir={outdir} refdir={refdir} subsamplelist={subsamplelist} --snakefile {snakefile} {forceall} {unlock} {dryrun} --cores {threads}".format(snakefile=snakefile, configfile=configfile, forceall=forceall, unlock = unlock, dryrun=dryrun, threads=args.threads,subsamplelist=args.subsample_list, outdir=args.outdir, refdir=args.bakcharakdir)
    
#    --config workdir=/cephfs/abteilung4/Projects_NGS/BakCharac_projects/Staph_subreport_new2 refdir=/cephfs/abteilung4/NGS/Databases_autoqc/Staphylococcus/bakcharak subsamplelist=/cephfs/abteilung4/Projects_NGS/BakCharac_projects/Staph_subreport/subsamplelist.txt
    print(call)
    subprocess.call(call, shell=True)



if __name__ == '__main__':
    main()
